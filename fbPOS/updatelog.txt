=======================
version 2.2.0516.1:

1、打印页面，安卓禁止返回键返回，Fix Bug：#2678 #2676；
2、添加Order打印：1、加入product到cart list，点Order打印，打印厨单，自动修改所有cart list商品状态Ordered为Preparing；2、继续添加商品，点Order打印，pop window选择打印类型，new order打印刚刚添加的商品，同时厨单下面出现Add Order提示，Reprint打印当前cart list的所有商品，同时厨单下面出现Reprint提示，两种类型都自动修改所有cart list商品状态Ordered为Preparing；3、cart list中没有Ordered状态的商品，点Order打印，此时相当于Reprint打印当前cart list的所有商品，同时厨单下面出现Reprint提示；
3、禁止现有跳转页面的动画效果，减少页面跳转顿挫，修改提示 payed 为 paid；
4、Fix Order History和Product List 搜索框不为空的时候下拉刷新结果不正确的bug；
5、订单未支付显示Invoice打印，此时打印Invoice不显示payment信息，若订单已支付则显示Receipt打印，此时打印Receipt会显示payment信息。


=======================
version 2.2.0516.2:
1、Fix hold之后，在serving list中删除刚刚hold的订单，订单被删除，这时候加product，提示Invoice not found. 的bug。原因：删除的订单ID没有及时清除；
2、查看order history之后可以选择是否需要打印；
3、Fix 按invoice打印也会修改cart list商品状态Ordered为Preparing的bug；



=======================
version 2.2.0519.1:
1、支付成功检查是否有未served商品，有则不跳到serving list；



=======================
version 2.2.0601.2:
1、update offline upload data function
2、add decimal point setting
3、add device prefix setting
4、fix canot show correct options bug
5、edit cart option UI change to block
6、add cart list slide button
7、fix payment pop UI
8、cashier current login name
9、multi option
10、speed up pop up edit cart

=======================
version 2.2.0602.1:
1、Enhance decimal point setting to 0,1,2
2、Add share table num
3、multi option
4、speed up pop up edit cart

=======================
version 2.2.0603.2:
1、fix offline addon
2、fix donwload loading UI

=======================
version 2.2.0605.1:
1、download animation
2、fix decimalPoint edit
3、pick up list

=======================
version 2.2.0606.1:
1、edit slide UI

=======================
version 2.2.0607.1:
1、add check out param "pay_method" for offline mode(offline not ready for param "payments")

=======================
version 2.2.0607.2:
1、change download waiting UI to circle
2、fix pick up invoice pass param 'direct sale' when checkout

=======================
version 2.2.0609.2:
1、scan for member coin

=======================
version 2.2.0609.4:
1、Octopus api

=======================
version 2.2.06014.4:
1、New Octopus
2、Sub category

=======================
version 2.2.06019.1:
1、Log out clear cart
2、fix sometime add to cart remind 'this cart have paid'
3、select Cashier in setting, pass cashierID when pay
4、Octopus give shopID&warehouseID&invoiceID in 'fare', add Octopus transactionDate in invoice
5、can switch sandbox&production mode in login page
6、choose print or not in setting page

=======================
version 2.2.06020.1:
1、Change Login name in setting and staff in invoice from login to name
2、Octopus "add value by .." sentence correct 
3、change split payment chooose octopus function
4、fix bug [3094]


