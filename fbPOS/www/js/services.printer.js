angular.module('services.printer', ['ionic', 'ngCordova', 'pascalprecht.translate', 'services.localstorage', 'services.helper'])

    /**************************************************
    // Helper services
    **************************************************/
    .factory('$printer', function ($ionicLoading, $rootScope, $localStorage, $q, $filter, $helper, $translate, $timeout) {

        // initialize $helper config
        var printer = {};

        /**************************************************
        // initialize epson printer connection
        **************************************************/
        printer.epsonInit = function () {

            $rootScope.epsonPrinterReady = false;
            $rootScope.epsonCanvasReady = false;
            $rootScope.epsonIsPrinting = false;
            $rootScope.epsonDevice = new epson.ePOSDevice();
            $rootScope.epsonDevice.connect($localStorage.get('settings').epson_ip_address, $localStorage.get('settings').epson_port, function (code) {
                if (code.indexOf('OK') !== -1) {
                    $helper.toast($translate.instant(code), 'short', 'bottom');
                    $rootScope.epsonDevice.createDevice($localStorage.get('settings').epson_device_id, $rootScope.epsonDevice.DEVICE_TYPE_PRINTER, { crypto: false, buffer: false },
                        function (data, code) {
                            if (code.indexOf('OK') !== -1) {
                                console.log('epson init success!');
                                $rootScope.epsonPrinter = data;
                                $rootScope.epsonPrinterReady = true;
                                printer.epsonAttachDeviceEvent();
                                printer.epsonAttachPrinterEvent();
                            } else {
                                console.log('epson init error!');
                                console.log(code);
                                $helper.toast(code, 'long', 'bottom');
                            }
                        });
                }
                if (code.indexOf('ERROR') !== -1) {
                    console.log(code);
                    $rootScope.showPrintPage = false;
                    $helper.toast($translate.instant(code), 'short', 'bottom');
                }
            }, { eposprint: true });
        };

        /**************************************************
        // attach epson device event
        **************************************************/
        printer.epsonAttachDeviceEvent = function () {

            //       $rootScope.epsonDevice.onreconnecting = function() {
            //         console.log('epsonPrinterDev:onreconnecting');
            //       };
            //       $rootScope.epsonDevice.onreconnect = function() {
            //         console.log('epsonPrinterDev:onreconnect');
            //       };
            $rootScope.epsonDevice.ondisconnect = function () {
                console.log('epsonPrinterDev:ondisconnect');
                printer.epsonInit();
            };

        };

        /**************************************************
        // attach epson printer event
        **************************************************/
        printer.epsonAttachPrinterEvent = function () {

            // Set a response receipt callback function
            $rootScope.epsonPrinter.onreceive = function (res) {
                var message = ('\
          Print' + (res.success ? 'Success' : 'Failure') + '\n\
          Code:' + res.code + '\n\
          Battery:' + res.battery + '\n\
          ' + printer.epsonGetStatusText(res.status) + '\
        ');
                console.log(message);
                $rootScope.epsonIsPrinting = false;
                console.log('finishing: ' + (performance.now() - $rootScope.t0) + 'ms');
                if (!res.success) {

                    console.log('final not success');
                }
                if (res.success)
                    console.log('final success!!');
                //$helper.toast(code, 'long', 'bottom');
                $rootScope.showPrintPage = false;
                $helper.hideLoading();
                $rootScope.showOctopusLoading = false;

            };

            $rootScope.epsonPrinter.onerror = function (err) {
                console.log(err);
                if (err.status != 0)
                    $helper.toast(err.status, 'short', 'bottom');
                $rootScope.showPrintPage = false;
                $rootScope.epsonPrinter = null;
            };

            // Set a status change callback funciton
            //       $rootScope.epsonPrinter.onstatuschange = function(status) { console.log('Printer:onstatuschange'); };
            //       $rootScope.epsonPrinter.ononline = function() { console.log('Printer:ononline'); };
            //       $rootScope.epsonPrinter.onoffline = function() { console.log('Printer:onoffline'); };
            //       $rootScope.epsonPrinter.onpoweroff = function() { console.log('Printer:onpoweroff'); };
            //       $rootScope.epsonPrinter.oncoverok = function() { console.log('Printer:oncoverok'); };
            //       $rootScope.epsonPrinter.oncoveropen = function() { console.log('Printer:oncoveropen'); };
            //       $rootScope.epsonPrinter.onpaperok = function() { console.log('Printer:onpaperok'); };
            //       $rootScope.epsonPrinter.onpapernearend = function() { console.log('Printer:onpapernearend'); };
            //       $rootScope.epsonPrinter.onpaperend = function() { console.log('Printer:onpaperend'); };
            //       $rootScope.epsonPrinter.ondrawerclosed = function() { console.log('Printer:ondrawerclosed'); };
            //       $rootScope.epsonPrinter.ondraweropen = function() { console.log('Printer:ondraweropen'); };
            //       $rootScope.epsonPrinter.onbatterystatuschange = function() { console.log('Printer:onbatterystatuschange'); };
            //       $rootScope.epsonPrinter.onbatteryok = function() { console.log('Printer:onbatteryok'); };
            //       $rootScope.epsonPrinter.onbatterylow = function() { console.log('Printer:onbatterylow'); };

            $rootScope.epsonPrinter.startMonitor();

        };

        /**************************************************
        // resolve espon status to human readable message
        **************************************************/
        printer.epsonGetStatusText = function () {
            var s = 'Status: \n';
            if (status & $rootScope.epsonPrinter.ASB_NO_RESPONSE) {
                s += ' No printer response\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_PRINT_SUCCESS) {
                s += ' Print complete\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_DRAWER_KICK) {
                s += ' Status of the drawer kick number 3 connector pin = "H"\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_OFF_LINE) {
                s += ' Offline status\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_COVER_OPEN) {
                s += ' Cover is open\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_PAPER_FEED) {
                s += ' Paper feed switch is feeding paper\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_WAIT_ON_LINE) {
                s += ' Waiting for online recovery\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_PANEL_SWITCH) {
                s += ' Panel switch is ON\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_MECHANICAL_ERR) {
                s += ' Mechanical error generated\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_AUTOCUTTER_ERR) {
                s += ' Auto cutter error generated\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_UNRECOVER_ERR) {
                s += ' Unrecoverable error generated\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_AUTORECOVER_ERR) {
                s += ' Auto recovery error generated\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_RECEIPT_NEAR_END) {
                s += ' No paper in the roll paper near end detector\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_RECEIPT_END) {
                s += ' No paper in the roll paper end detector\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_BUZZER) {
                s += ' Sounding the buzzer (certain model)\n';
            }
            if (status & $rootScope.epsonPrinter.ASB_SPOOLER_IS_STOPPED) {
                s += ' Stop the spooler\n';
            }
            return s;
        };

        /**************************************************
        // print test page for epson printer
        **************************************************/
        printer.esponOpenDrawer = function () {

            $rootScope.epsonPrinter.addPulse(printer.DRAWER_1, printer.PULSE_100);
            $rootScope.epsonPrinter.send();

        };


        /**************************************************
        // prepare canvas for printing
        **************************************************/
        printer.epsonPrepareCanvas = function (css, html) {
            $rootScope.printCalcNum = 50;
            $rootScope.epsonCanvasReady = false;
            if (document.getElementById('printer-css') && document.getElementById('printer-content') && document.getElementById('printer-canvas')) {
                $rootScope.showPrintPage = true;
                document.getElementById('printer-css').innerHTML = css;
                document.getElementById('printer-content').innerHTML = html;
                document.getElementById('printer-canvas').innerHTML = '';
                html2canvas(document.getElementById('printer-content'), {
                    width: 562,
                    onrendered: function (canvas) {
                        console.log('html2canvas: ' + (performance.now() - $rootScope.t0) + 'ms');
                        document.getElementById('printer-canvas').appendChild(canvas);
                        $rootScope.epsonCanvasReady = true;
                    }
                });
            } else {
                $rootScope.showPrintPage = false;
            }

        };

        printer.epsonPrepareTwoCanvas = function (css, html1, html2) {
            $rootScope.epsonCanvasReady = false;
            document.getElementById('printer-css').innerHTML = css;
            document.getElementById('printer-content').innerHTML = html1;
            document.getElementById('printer-content2').innerHTML = html2;
            document.getElementById('printer-canvas').innerHTML = '';

            html2canvas(document.getElementById('printer-content'), {
                width: 562,
                onrendered: function (foodCanvas) {
                    console.log('html2canvas: ' + (performance.now() - $rootScope.t0) + 'ms');
                    document.getElementById('printer-canvas').appendChild(foodCanvas);
                    html2canvas(document.getElementById('printer-content2'), {
                        width: 562,
                        onrendered: function (drinkCanvas) {
                            $rootScope.epsonCanvasReady = true;
                            document.getElementById('printer-canvas').appendChild(drinkCanvas);
                        }
                    });

                }
            });

        };

        printer.epsonPrepareThreeCanvas = function (css, html1, html2, html3) {
            $rootScope.epsonCanvasReady = false;
            document.getElementById('printer-css').innerHTML = css;
            document.getElementById('printer-content').innerHTML = html1;
            document.getElementById('printer-content2').innerHTML = html2;
            document.getElementById('printer-content3').innerHTML = html3;
            document.getElementById('printer-canvas').innerHTML = '';

            html2canvas(document.getElementById('printer-content'), {
                width: 562,
                onrendered: function (foodCanvas) {
                    console.log('html2canvas: ' + (performance.now() - $rootScope.t0) + 'ms');
                    document.getElementById('printer-canvas').appendChild(foodCanvas);
                    html2canvas(document.getElementById('printer-content2'), {
                        width: 562,
                        onrendered: function (drinkCanvas) {
                            document.getElementById('printer-canvas').appendChild(drinkCanvas);
                            html2canvas(document.getElementById('printer-content3'), {
                                width: 562,
                                onrendered: function (food2Canvas) {
                                    $rootScope.epsonCanvasReady = true;
                                    document.getElementById('printer-canvas').appendChild(food2Canvas);
                                }
                            });
                        }
                    });

                }
            });

        };

        // 2017-6-6 - TK: handle multiple printing page
        printer.epsonAppendCanvas = function (html) {
            console.log(html);

            var defer = $q.defer();

            var div = document.createElement('div');
            console.log(div);
            div.className = 'printer-content';
            div.innerHTML = html;
            document.getElementById('global-print-page').appendChild(div);
            console.log(document);
            html2canvas(div, {
                width: 562,
                onrendered: function (canvas) {
                    document.getElementById('printer-canvas').appendChild(canvas);
                    div.parentNode.removeChild(div);
                    defer.resolve();
                }
            });

            return defer.promise;

        };

        // 2017-6-6 - TK: handle multiple printing page
        printer.epsonPrepareMultipleCanvas = function (css, htmls) {
            $helper.showLoading(15000);
            $rootScope.printCalcNum = 50;
            $rootScope.epsonCanvasReady = false;
            $rootScope.showPrintPage = true;
            if (document.getElementById('printer-css') && document.getElementById('printer-canvas')) {                
                document.getElementById('printer-css').innerHTML = css;
                document.getElementById('printer-canvas').innerHTML = '';
                $rootScope.epsonPageCount = htmls.length;
                $rootScope.epsonPageReadyCount = 0;
                for (var i = 0; i < htmls.length; i++) {
                    printer.epsonAppendCanvas(htmls[i]).then(function (res) {
                        console.log('done');
                        $rootScope.epsonPageReadyCount++;
                        if ($rootScope.epsonPageReadyCount == $rootScope.epsonPageCount) {
                            $rootScope.epsonCanvasReady = true;
                        }
                    }).catch(function (err) {
                        // handle html2canvas error
                    });
                }
            } else {
                $rootScope.showPrintPage = false;
            }

        };

        /**************************************************
        // print test page for epson printer
        **************************************************/
        printer.epsonPrintCanvas = function () {

            console.log('into epsonPrintCanvas......');
            console.log($rootScope.epsonPrinter);

            if (!$rootScope.epsonPrinter) {
                $rootScope.showPrintPage = false;
                console.log('not connect printer');
                return;

            }

            if ($rootScope.epsonIsPrinting) {
                console.log('dont duplicate print!!');
                // $rootScope.showPrintPage = false;
                // $helper.toast($translate.instant('PRINTING_IN_PROCESS'), 'short', 'bottom');
                // $rootScope.epsonIsPrinting = false;
                return;
            }

            if (!$rootScope.epsonCanvasReady) {
                console.log('canvas not ready');
                // $helper.toast($translate.instant('CANVAS_NOT_READY'), 'long', 'bottom');
                $timeout(function () {
                    if ($rootScope.printCalcNum < 1) {
                        return;
                    }
                    printer.epsonPrintCanvas();
                    $rootScope.printCalcNum--;
                }, 100);
                $timeout(function () {
                    // $rootScope.showPrintPage = false;
                    console.log('showPrintPage already false!!!');
                }, 1000);

                // $rootScope.watchFunc = $rootScope.$watch('epsonCanvasReady', function(newValue, oldValue) {
                //     console.log('into epson watch');
                //     if (newValue == true) {
                //         printer.epsonPrintCanvas();
                //     }
                // });
                return;
            }

            $rootScope.epsonIsPrinting = true;
            $rootScope.watchFunc = function () { };
            for (var i = 0; i < document.getElementById('printer-canvas').getElementsByTagName('canvas').length; i++) {
                var canvas = document.getElementById('printer-canvas').getElementsByTagName('canvas')[i];
                if (canvas.getContext) {
                    var context = canvas.getContext('2d');
                    $rootScope.epsonPrinter.addTextAlign($rootScope.epsonPrinter.ALIGN_CENTER);
                    $rootScope.epsonPrinter.brightness = 0.3;
                    $rootScope.epsonPrinter.halftone = $rootScope.epsonPrinter.HALFTONE_DITHER;
                    $rootScope.epsonPrinter.addImage(canvas.getContext('2d'), 0, 0, canvas.width, canvas.height, $rootScope.epsonPrinter.COLOR_1, $rootScope.epsonPrinter.MODE_MONO);
                    // add auto cut between each canvas, send out command on last canvas
                    if (i == document.getElementById('printer-canvas').getElementsByTagName('canvas').length - 1) {
                        $rootScope.epsonPrinter.addCut($rootScope.epsonPrinter.CUT_FEED);
                        console.log('before: ' + (performance.now() - $rootScope.t0) + 'ms');
                        $rootScope.epsonPrinter.send();
                        console.log('already sent data to printer !!!');
                    } else {
                        $rootScope.epsonPrinter.addCut($rootScope.epsonPrinter.CUT_FEED);
                    }

                }
            }
            // $rootScope.showPrintPage = false;

        };

        printer.immediatePrint = function (css, html) {
            $rootScope.epsonCanvasReady = false;
            document.getElementById('printer-css').innerHTML = css;
            document.getElementById('printer-content').innerHTML = html;
            document.getElementById('printer-canvas').innerHTML = '';
            html2canvas(document.getElementById('printer-content'), {
                width: 562,
                onrendered: function (canvas) {
                    console.log('html2canvas: ' + (performance.now() - $rootScope.t0) + 'ms');
                    document.getElementById('printer-canvas').appendChild(canvas);
                    $rootScope.epsonCanvasReady = true;
                    printer.epsonPrintCanvas();
                }
            });
        };

        /**************************************************
        // print test page for epson printer
        **************************************************/
        printer.epsonTest = function () {

            $rootScope.epsonPrinter.addText('------------------------------\n');
            $rootScope.epsonPrinter.addText('CASH                    200.00\n');
            $rootScope.epsonPrinter.addText('CHANGE                   25.19\n');
            $rootScope.epsonPrinter.addText('------------------------------\n');
            $rootScope.epsonPrinter.addCut(printer.CUT_FEED);
            $rootScope.epsonPrinter.send()
        };

        /**************************************************
        // Return service
        **************************************************/
        return printer;

    });
