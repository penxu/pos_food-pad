angular.module('services.offline', ['ionic', 'ngCordova', 'services.helper'])
    //0615.1 phi
    /**************************************************
    // offline services
    **************************************************/
    .factory('$offline', function ($cordovaSQLite, $ionicLoading, $q, $http, $localStorage, SERVER, $helper, $ionicModal, $cordovaFileTransfer, $rootScope, $cordovaZip, $translate, $timeout) {

        var secondPath = 'product-media/';
        var pdfPath = '~/Documents/invoice.pdf';
        // initialize $offline config
        var offline = {
            db: null,
            serialize: function (obj, prefix) {
                var str = [];
                for (var p in obj) {
                    if (obj.hasOwnProperty(p)) {
                        var k = prefix ? prefix + '[' + p + ']' : p,
                            v = obj[p];
                        str.push(typeof v == 'object' ? this.serialize(v, k) : encodeURIComponent(k) + '=' + encodeURIComponent(v));
                    }
                }
                return str.join('&');
            }
        };


        var checkUndefined = function (params) {
            Object.keys(params).map(function (objectKey, index) {
                var value = params[objectKey];
                if (typeof value == "undefined" || value == null) {
                    params[objectKey] = '';
                }
            });
        };


        var emptyTozero = function (str) {
            return str != '' ? str : 0;
        };

        var encodePreSku = function (json_array) {
            var sku = '';
            var spec_array = JSON.parse(json_array);
            for (var i = 0; i < spec_array.length; i++) {
                sku = sku + 'X' + spec_array[i].dictionary.toString(16);
                sku = sku + 'Y' + spec_array[i].option.toString(16);
                sku = sku + '-';
            }
            return sku;
        };

        /**************************************************
        // open database
        **************************************************/
        offline.openDb = function () {
            offline.db = window.sqlitePlugin.openDatabase({
                name: $localStorage.get('activate').prefix + $localStorage.get('activate').path + '.db',
                location: 'default'
            });

        };

        /**************************************************
        // close database
        **************************************************/
        offline.closeDb = function (params) {

            if (offline.db != null) {
                offline.db.close(
                    function () {
                        offline.db = null;
                        console.log('database closed success');
                    },
                    function () {
                        console.log('database closed fail');
                    }
                );
            }
        };

        /**************************************************
        // get offline data
        **************************************************/
        offline.getOfflineData = function (params) {
            console.log('show loading');

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            //console.log('ready to post');
            $http.post(SERVER.http + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'api/get-offline-data', offline.serialize(params)).success(function (data) {

                console.log('api return offline data');
                try {
                    var res = angular.fromJson(data);
                    console.log('already get offline data~');
                    if (res.status == 'Y') {
                        offline.db.sqlBatch(res.data, function () {
                            defer.resolve();
                        }, function (err) {
                            defer.reject(err);
                        });
                    } else {
                        defer.reject(res.msg);
                    }
                } catch (err) {
                    defer.reject(err);
                }
            }).error(function (err) {
                console.log('request error');
                defer.reject(err);
            });


            return defer.promise;
        };

        /**************************************************
        // get offline data
        **************************************************/
        offline.getOfflinePhoto = function (params) {
            console.log('into photo function');
            // $helper.showLoading(180000);
            var defer = $q.defer();
            // var url = 'http://sandbox.gardengallery.posify.me//data/product-image/1';
            var url = SERVER.http + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + '/data/product-image/' + $localStorage.get('user').shop_id;
            console.log('link : ' + url);
            var targetPath = $helper.getRootPath() + secondPath + 'photo.zip';
            var unzipPath = $helper.getRootPath() + secondPath;
            var trustHosts = true;
            var options = {};
            $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
                .then(function (result) {

                    console.log('download photo finish');
                    console.log('zip file path:' + targetPath);
                    console.log('unzip path:' + unzipPath);
                    $cordovaZip
                        .unzip(
                        targetPath, // https://github.com/MobileChromeApps/zip/blob/master/tests/tests.js#L32
                        unzipPath // https://github.com/MobileChromeApps/zip/blob/master/tests/tests.js#L45
                        ).then(function () {
                            // $ionicLoading.hide();
                            //console.log('success unzip');
                            //$helper.clearCache(targetPath);
                            // $helper.toast('Download data success!', 'short', 'bottom');
                            defer.resolve();
                            // if ($rootScope.sandboxCheckbox) {
                            //     $localStorage.set('sandbox_update_time', $rootScope.sandboxUpdateTime);
                            // } else {
                            //     $localStorage.set('production_update_time', $rootScope.productionUpdateTime);
                            // }
                        }, function (status) {
                            // console.log('~~into photo errors~~');
                            defer.reject(status);
                            // $helper.toast('unzip status: ' + status, 'short', 'top');
                        }, function (progressEvent) {

                        });
                    // Success!
                }, function (err) {
                    // Error
                    $ionicLoading.hide();
                });
            return defer.promise;
        };

        /**************************************************
        // offline api function
        **************************************************/
        offline.getCategoryNestedData = function (category, locale) {
            var data = {
                id: category.ID,
                type: category.TYPE,
                parent_id: category.PARENT_ID,
                name: category['NAME_' + locale],
                name_en_us: category.NAME_EN_US,
                name_zh_hk: category.NAME_ZH_HK,
                name_zh_cn: category.NAME_ZH_CN,
                sorting: category.SORTING,
                show_home: category.SHOW_HOME,
                product_count: category.PRODUCT_COUNT,
                enabled: category.ENABLED,
                theme: category.THEME,
                children: []
            };
            var all = {
                id: category.ID,
                type: category.TYPE,
                parent_id: category.PARENT_ID,
                name: 'All',
                name_en_us: 'All',
                name_zh_hk: '全部',
                name_zh_cn: '全部',
                sorting: category.SORTING,
                show_home: category.SHOW_HOME,
                product_count: category.PRODUCT_COUNT,
                enabled: category.ENABLED,
                theme: category.THEME,
                children: []
            };
            data.children.push(all);
            offline.db.executeSql('SELECT * FROM "PRODUCT_CATEGORY" WHERE "ENABLED" = 1 AND "PARENT_ID" = ? ORDER BY "SORTING" DESC', [category.ID], function (res) {
                for (var i = 0; i < res.rows.length; i++) {
                    data.children.push(offline.getCategoryNestedData(res.rows.item(i), locale));
                }
            }, function (err) {
                console.log(err);
            });

            return data;
        };

        offline.getCategory = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            //console.log('into tks func');
            offline.db.executeSql('SELECT * FROM "PRODUCT_CATEGORY" WHERE "ENABLED" = 1 AND "PARENT_ID" = 0 ORDER BY "SORTING" DESC', [], function (res) {
                var ret = {
                    status: 'Y',
                    msg: '',
                    data: []
                };
                //console.log('before loop tks func');
                for (var i = 0; i < res.rows.length; i++) {
                    ret.data.push(offline.getCategoryNestedData(res.rows.item(i), params.locale));
                }
                //console.log('finish loop tks func');
                defer.resolve(ret);
            }, function (err) {
                //console.log('error tks func');
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getMemberList = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            //console.log('into phi func');
            var additional_sql = '';
            if (typeof params.keyword != "undefined" && params.keyword != null) {
                additional_sql = additional_sql + ' where FIRST_NAME like "%' + params.keyword + '%"';
                additional_sql = additional_sql + ' or LAST_NAME like "%' + params.keyword + '%"';
                additional_sql = additional_sql + ' or MOBILE like "%' + params.keyword + '%"';
                additional_sql = additional_sql + ' or EMAIL like "%' + params.keyword + '%"';
            }

            var offset = 0;
            var limit = 20;

            if (typeof params.limit_from != "undefined") {
                offset = params.limit_from;
            }
            if (typeof params.limit != "undefined") {
                limit = params.limit;
            }

            offline.db.executeSql(
                'SELECT * FROM "MEMBER_PROFILE"' + additional_sql, [

                ],
                function (res) {
                    var ret = {
                        status: 'Y',
                        msg: '',

                        member: {
                            count: res.rows.length,
                            list: []
                        }

                    };
                    //console.log(res);
                    for (var i = offset; i < offset + limit && i < res.rows.length; i++) {
                        record = res.rows.item(i);
                        checkUndefined(record);
                        var member_info = {
                            user_id: record.USER_ID,
                            member_id: record.ID,
                            first_name: record.FIRST_NAME,
                            last_name: record.LAST_NAME,
                            //grade : record.
                            //VIP_LEVEL :
                            //discount:
                            gender: record.GENDER,
                            country_code: record.COUNTRY_CODE,
                            mobile: record.MOBILE,
                            email: record.EMAIL,
                            birthday: record.BIRTHDAY,
                            nationality_id: record.NATIONALITY,
                            num_family: record.NUM_FAMILY,
                            occupation: record.OCCUPATION,
                            prefer_currency: record.PREFER_CURRENCY,

                        };
                        ret.member.list.push(member_info);
                    }
                    defer.resolve(ret);
                },
                function (err) {
                    //console.log('reject phi func');
                    defer.reject(err);
                }
            );
            //console.log('finish loop');
            return defer.promise;

        };

        offline.getInvoiceList = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var status_sql = "";
            if (typeof params.status != "undefined" && params.status != []) {
                status_sql = status_sql + " where (";
                for (var ii = 0; ii < params.status.length; ii++) {
                    if (ii != 0) {
                        status_sql = status_sql + " OR ";
                    }
                    status_sql = status_sql + "INVOICE.STATUS = " + params.status[ii];

                }
                status_sql = status_sql + ")";

            } else {
                status_sql = " where INVOICE.STATUS = 0";
            }
            if (typeof params.keyword != "undefined" && params.keyword != null) {
                status_sql = status_sql + " AND (";
                status_sql = status_sql + "INVOICE.INVOICE_NO LIKE '%" + params.keyword + "%' OR ";
                status_sql = status_sql + "INVOICE.BILLING_LAST_NAME LIKE '%" + params.keyword + "%' OR ";
                status_sql = status_sql + "INVOICE.BILLING_FIRST_NAME LIKE '%" + params.keyword + "%')";

            }
            status_sql = status_sql + " and NOT(INVOICE.FB_TABLE = 0 AND IITEM.ITEM_TOTAL = 0) order by INVOICE.ID desc";

            var limit_from = 0;
            var limit = 20;
            if (typeof params.limit_from != "undefined" && params.limit_from != null) {
                limit_from = params.limit_from;
            }
            if (typeof params.limit != "undefined" && params.limit != null) {
                limit = params.limit;
            }

            offline.db.executeSql(
                'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL, count(case when SERVED = 1 AND QTY > 0 then 1 else null end) as SERVED_QTY, count(case when QTY > 0 then 1 else null end) as CC_QTY from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID' + status_sql, [],
                function (res) {
                    var ret = {
                        status: 'Y',
                        msg: '',
                        data: {
                            count: res.rows.length,
                            list: []
                        }
                    };

                    var invoice_charge_index = 0;
                    for (var i = limit_from; i < limit + limit_from && i < res.rows.length; i++) {
                        var record = res.rows.item(i);
                        checkUndefined(record);
                        var serve_status = emptyTozero(record.SERVED_QTY) + '/' + emptyTozero(record.CC_QTY);
                        var inv_info = {
                            id: record.ID,
                            table_num: emptyTozero(record.FB_TABLE),
                            invoice_no: record.INVOICE_NO,
                            item_count: emptyTozero(record.ITEM_COUNT),
                            currency: record.CURRENCY,
                            gender: emptyTozero(record.GENDER),
                            billing_last_name: record.BILLING_LAST_NAME,
                            price: emptyTozero(record.ITEM_TOTAL),
                            status_num: emptyTozero(record.STATUS),
                            invoice_date: record.INVOICE_DATE,
                            bill_to: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                            member_mobile: record.BILLING_COUNTRY_CODE + ' ' + record.BILLING_MOBILE,
                            delivery_type: record.DELIVERY_TYPE,
                            check_out_warehouse: emptyTozero(record.CHECK_OUT_WAREHOUSE_ID),
                            pick_up_warehouse_id: emptyTozero(record.PICK_UP_WAREHOUSE_ID),
                            expected_delivery_date: record.EXPECTED_DELIVERY_DATE,
                            name: 'Offline Visitor',
                            user_id: emptyTozero(record.USER_ID),
                            member_id: 0,
                            vip_level: 0,
                            member_profile: [],
                            served_qty: serve_status,
                            sync: emptyTozero(record.CARRY_UP),
                        };

                        ret.data.list.push(inv_info);

                        if (inv_info.bill_to != ' ') {
                            inv_info.name = inv_info.bill_to;
                        }


                        var iid = record.ID;
                        offline.db.executeSql(
                            'SELECT * from INVOICE_CHARGES where INVOICE_ID=?', [iid],
                            function (inv_charge) {

                                var baseDiscount = 0;
                                var baseCharge = 0;
                                var baseTotal = ret.data.list[invoice_charge_index].price;

                                for (var j = 0; j < inv_charge.rows.length; j++) {
                                    var invoice_charge = inv_charge.rows.item(j);
                                    checkUndefined(invoice_charge);
                                    if (invoice_charge.VALUE_TYPE == 'percent') {
                                        if (invoice_charge.SIGN == '+') {
                                            var totalFlag = baseTotal;
                                            baseTotal *= 1 + Number(invoice_charge.VALUE) / 100;
                                            baseCharge += baseTotal - totalFlag;
                                        } else {
                                            var totalFlag = baseTotal;
                                            baseTotal *= 1 - Number(invoice_charge.VALUE) / 100;
                                            baseDiscount += totalFlag - baseTotal;
                                        }
                                    }

                                }

                                for (var j = 0; j < inv_charge.rows.length; j++) {
                                    var invoice_charge = inv_charge.rows.item(j);
                                    checkUndefined(invoice_charge);
                                    if (invoice_charge.VALUE_TYPE != 'percent') {
                                        if (invoice_charge.SIGN == '+') {
                                            var totalFlag = baseTotal;
                                            baseTotal += Number(invoice_charge.VALUE);
                                            baseCharge += baseTotal - totalFlag;
                                        } else {
                                            var totalFlag = baseTotal;
                                            baseTotal -= Number(invoice_charge.VALUE);
                                            baseDiscount += totalFlag - baseTotal;
                                        }
                                    }

                                }

                                ret.data.list[invoice_charge_index++].price = baseTotal;

                                if (limit == ret.data.list.length || res.rows.length == ret.data.list.length) {

                                    defer.resolve(ret);
                                }

                            },
                            function (err) {
                                defer.reject(err);
                            });

                    }
                    if (0 == ret.data.list.length) {
                        //console.log(ret);
                        defer.resolve(ret);
                    }

                },
                function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        };

        offline.getInvoiceDetail = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            if (typeof params.fb_table != "undefined" && params.fb_table != null) {
                offline.db.executeSql('UPDATE "INVOICE" set "FB_TABLE" = ? where ID = ?', [params.fb_table, params.invoice_id], function (update_res) {

                    offline.db.executeSql(
                        'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL  from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID where INVOICE.ID = ?', [params.invoice_id],
                        function (res) {

                            var record = res.rows.item(0);
                            checkUndefined(record);
                            var ret = {
                                status: 'Y',
                                msg: '',
                                data: {
                                    id: record.ID,
                                    table_num: record.FB_TABLE,
                                    invoice_no: record.INVOICE_NO,
                                    ticket_num: record.INVOICE_NO.slice(-4),
                                    currency: record.CURRENCY,
                                    gender: record.GENDER,
                                    billing_area_id: emptyTozero(record.BILLING_AREA_ID),
                                    billing_region_id: emptyTozero(record.BILLING_REGION_ID),
                                    billing_district_id: emptyTozero(record.BILLING_DISTRICT_ID),
                                    billing_country_id: emptyTozero(record.BILLING_COUNTRY_ID),
                                    billing_address_1: record.BILLING_ADDRESS_1,
                                    billing_address_2: record.BILLING_ADDRESS_2,
                                    billing_address_3: record.BILLING_ADDRESS_3,
                                    billing_address_4: record.BILLING_ADDRESS_4,
                                    billing_first_name: record.BILLING_FIRST_NAME,
                                    billing_last_name: record.BILLING_LAST_NAME,
                                    billing_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                    billing_email: record.BILLING_EMAIL,
                                    billing_mobile: record.BILLING_COUNTRY_CODE + ' ' + record.BILLING_MOBILE,

                                    pick_up_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                    pick_up_first_name: record.BILLING_FIRST_NAME,
                                    pick_up_last_name: record.BILLING_LAST_NAME,
                                    pick_up_country_code: record.BILLING_COUNTRY_CODE,
                                    pick_up_mobile: record.BILLING_MOBILE,
                                    pick_up_email: record.BILLING_EMAIL,

                                    shipping_area_id: emptyTozero(record.SHIPPING_AREA_ID),
                                    shipping_region_id: emptyTozero(record.SHIPPING_REGION_ID),
                                    shipping_district_id: emptyTozero(record.SHIPPING_DISTRICT_ID),
                                    shipping_country_id: emptyTozero(record.SHIPPING_COUNTRY_ID),
                                    shipping_address_1: record.BILLING_ADDRESS_1,
                                    shipping_address_2: record.BILLING_ADDRESS_2,
                                    shipping_address_3: record.BILLING_ADDRESS_3,
                                    shipping_address_4: record.BILLING_ADDRESS_4,
                                    shipping_first_name: record.SHIPPING_FIRST_NAME,
                                    shipping_last_name: record.SHIPPING_LAST_NAME,
                                    shipping_contact: record.SHIPPING_FIRST_NAME + ' ' + record.SHIPPING_LAST_NAME,
                                    shipping_email: record.SHIPPING_EMAIL,
                                    shipping_mobile: record.SHIPPING_COUNTRY_CODE + ' ' + record.SHIPPING_MOBILE,

                                    currency: record.CURRENCY,
                                    item_count: record.ITEM_COUNT,
                                    item_total: emptyTozero(record.ITEM_TOTAL),
                                    pay_method: record.PAY_METHOD,
                                    status_num: emptyTozero(record.STATUS),

                                    deposit: emptyTozero(record.PAYED_AMOUNT),
                                    payment_type: record.PAYMENT_TYPE,
                                    payed_amount: emptyTozero(record.PAYED_AMOUNT),

                                    delivery_type: record.DELIVERY_TYPE,
                                    remark: record.REMARK,
                                    products: [],
                                    department_list: [],

                                    invoice_date: record.INVOICE_DATE,
                                    check_out_warehouse: emptyTozero(record.CHECK_OUT_WAREHOUSE_ID),
                                    pick_up_warehouse_id: emptyTozero(record.PICK_UP_WAREHOUSE_ID),
                                    pick_up_site: record.PICK_UP_SITE,
                                    expected_delivery_date: record.EXPECTED_DELIVERY_DATE,
                                    invoice_charges: [],

                                    delivery_total: emptyTozero(record.DELIVERY_TOTAL),
                                    refund_total: emptyTozero(record.REFUND_TOTAL),
                                    refund: emptyTozero(record.REFUND_TOTAL),
                                    grand_total: emptyTozero(record.ITEM_TOTAL),

                                    customer_name: 'Visitor',
                                    customer_info: {
                                        account_no: 'visitor',
                                        user_id: 0,
                                        customer_name: '',
                                        customer_first_name: '',
                                        customer_last_name: '',
                                        customer_email: '',
                                        customer_country_code: '',
                                        customer_mobile: '',
                                        vip_level: ''
                                    }
                                }
                            };

                            offline.db.executeSql(
                                'SELECT *,INVOICE_ITEM.ID as ITEM_ID, INVOICE_ITEM.ADDONS as ITEM_ADDONS from INVOICE_ITEM left join (select PRODUCT.DEPARTMENT_ID, PRODUCT.ID, PRODUCT.NAME_EN_US, PRODUCT.NAME_ZH_HK, PRODUCT.NAME_ZH_CN, PRODUCT.HIGHLIGHT_EN_US,PRODUCT.HIGHLIGHT_ZH_HK, PRODUCT.HIGHLIGHT_ZH_CN, PRODUCT.CODE, PRODUCT.WEIGHT, PRODUCT.VOLUME, PRODUCT.REMARK, PRODUCT.SPEC_FOR_OFFLINE, PM.ID as PHOTO_ID from PRODUCT LEFT JOIN (SELECT MIN(ID) as ID,PRODUCT_ID FROM PRODUCT_MEDIA group by PRODUCT_ID) as PM on PRODUCT.ID = PM.PRODUCT_ID) as FP on INVOICE_ITEM.PRODUCT_ID = FP.ID where INVOICE_ID=?', [record.ID],
                                function (inv_item) {

                                    ret.data.item_count = inv_item.rows.length;

                                    for (var i = 0; i < inv_item.rows.length; i++) {

                                        //ret.data.item_count = inv_item.rows.length;
                                        //                     var product;
                                        //                     var invoice_item = inv_item.rows.item(0);
                                        //                     offline.db.executeSql(
                                        //                     'SELECT * from PRODUCT where ID=?', [inv_item.rows.item(i).PRODUCT_ID], function(inv_item_pro) {
                                        //
                                        //                         product = inv_item_pro.rows.item(0);
                                        //
                                        //
                                        //                     }, function(err) {
                                        //                       defer.reject(err);
                                        //                     });
                                        var invoice_item = inv_item.rows.item(i);
                                        checkUndefined(invoice_item);
                                        var photo_url = $helper.getRootPath() + secondPath + invoice_item.PHOTO_ID;

                                        var product_info = {
                                            id: invoice_item.PRODUCT_ID,
                                            item_id: invoice_item.ITEM_ID,
                                            name: invoice_item['NAME_' + params.locale],
                                            highlight: invoice_item['HIGHLIGHT_' + params.locale],
                                            photo: photo_url,
                                            qty: emptyTozero(invoice_item.QTY),
                                            buy_qty: emptyTozero(invoice_item.QTY),
                                            sku_no: invoice_item.SKU_NO,
                                            code: invoice_item.CODE,
                                            options: [],
                                            currency: invoice_item.CURRENCY,
                                            o_price: emptyTozero(invoice_item.UNIT_PRICE),
                                            unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                            actual_unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                            sub_total: emptyTozero(invoice_item.UNIT_PRICE * invoice_item.QTY),
                                            //buy_sub_total
                                            //actual sub total
                                            weight: invoice_item.WEIGHT,
                                            volumn: invoice_item.VOLUMN,
                                            remark: invoice_item.REMARK,
                                            served: invoice_item.SERVED,
                                            specifications: JSON.parse(invoice_item.SPEC_FOR_OFFLINE),
                                            addons: JSON.parse(invoice_item.ITEM_ADDONS),
                                            department_id: invoice_item.DEPARTMENT_ID,
                                            department_name: '',
                                            department_name_EN_US: '',
                                            department_name_ZH_HK: '',
                                            department_name_ZH_CN: '',
                                        };

                                        var spec_array = JSON.parse(invoice_item.SPEC_FOR_OFFLINE);

                                        var oids = [];

                                        var attribute = invoice_item.SKU_NO.split("-");
                                        var len = attribute.length;

                                        for (var j = 0; j < len - 1; j++) {
                                            var tokens = attribute[j].split("Y");
                                            var dict_id = parseInt(tokens[0].split("X")[1], 16);
                                            var opt_id = parseInt(tokens[1], 16);
                                            oids.push(opt_id);
                                        }

                                        for (var sa = 0; sa < spec_array.length; sa++) {
                                            var spec = spec_array[sa];
                                            var dict_opt = {
                                                title: spec.title,
                                                options: []
                                            };


                                            for (var oi = 0; oi < spec.options.length; oi++) {

                                                if (oids.indexOf(spec.options[oi]['id']) != -1) {

                                                    dict_opt.options.push(spec.options[oi]['name']);
                                                }
                                            }

                                            product_info.options.push(dict_opt);

                                        }

                                        ret.data.products.push(product_info);


                                    }

                                    var baseDiscount = 0;
                                    var baseCharge = 0;
                                    var baseTotal = record.ITEM_TOTAL;

                                    offline.db.executeSql(
                                        'SELECT * from INVOICE_CHARGES where INVOICE_ID=?', [record.ID],
                                        function (inv_charge) {

                                            for (var i = 0; i < inv_charge.rows.length; i++) {
                                                var invoice_charge = inv_charge.rows.item(i);
                                                checkUndefined(invoice_charge);

                                                var charge_info = {
                                                    title_EN_US: invoice_charge.NAME_EN_US,
                                                    title_ZH_HK: invoice_charge.NAME_ZH_HK,
                                                    title_ZH_CN: invoice_charge.NAME_ZH_CN,
                                                    sign: invoice_charge.SIGN,
                                                    type: invoice_charge.TYPE,
                                                    value_type: invoice_charge.VALUE_TYPE,
                                                    value: invoice_charge.VALUE,

                                                };
                                                ret.data.invoice_charges.push(charge_info);

                                            }

                                            for (var i = 0; i < inv_charge.rows.length; i++) {
                                                var invoice_charge = inv_charge.rows.item(i);
                                                checkUndefined(invoice_charge);
                                                if (invoice_charge.VALUE_TYPE == 'percent') {
                                                    if (invoice_charge.SIGN == '+') {
                                                        var totalFlag = baseTotal;
                                                        baseTotal *= 1 + Number(invoice_charge.VALUE) / 100;
                                                        baseCharge += baseTotal - totalFlag;
                                                    } else {
                                                        var totalFlag = baseTotal;
                                                        baseTotal *= 1 - Number(invoice_charge.VALUE) / 100;
                                                        baseDiscount += totalFlag - baseTotal;
                                                    }
                                                }

                                            }

                                            for (var i = 0; i < inv_charge.rows.length; i++) {
                                                var invoice_charge = inv_charge.rows.item(i);
                                                checkUndefined(invoice_charge);
                                                if (invoice_charge.VALUE_TYPE != 'percent') {
                                                    if (invoice_charge.SIGN == '+') {
                                                        var totalFlag = baseTotal;
                                                        baseTotal += Number(invoice_charge.VALUE);
                                                        baseCharge += baseTotal - totalFlag;
                                                    } else {
                                                        var totalFlag = baseTotal;
                                                        baseTotal -= Number(invoice_charge.VALUE);
                                                        baseDiscount += totalFlag - baseTotal;
                                                    }
                                                }

                                            }

                                            ret.data.grand_total = baseTotal;
                                            ret.data.discount_total = 0;
                                            ret.data.service_total = 0;

                                            // get department
                                            var cur_products = ret.data.products;
                                            var pro_count = 0;
                                            if (cur_products.length != 0) {
                                                for (var pro_dep = 0; pro_dep < cur_products.length; pro_dep++) {
                                                    var product = cur_products[pro_dep];
                                                    offline.db.executeSql(
                                                        'SELECT * from `OPTION` where ID=?', [product.department_id],
                                                        function (opt_record) {
                                                            var option = opt_record.rows.item(0);
                                                            checkUndefined(option);

                                                            ret.data.products[pro_count].department_name = option['NAME_' + params.locale];
                                                            ret.data.products[pro_count].department_name_EN_US = option.NAME_EN_US;
                                                            ret.data.products[pro_count].department_name_ZH_HK = option.NAME_ZH_HK;
                                                            ret.data.products[pro_count].department_name_ZH_CN = option.NAME_ZH_CN;
                                                            if (ret.data.department_list.indexOf(option.ID) == -1) {
                                                                ret.data.department_list.push(option.ID);
                                                            }
                                                            pro_count++;
                                                            if (pro_count == cur_products.length) {
                                                                defer.resolve(ret);
                                                            }
                                                        },
                                                        function (err) {
                                                            defer.reject(err);
                                                        });


                                                }
                                            } else {
                                                defer.resolve(ret);
                                            }

                                            // get department
                                            // if no get department                                defer.resolve(ret);

                                        },
                                        function (err) {
                                            defer.reject(err);
                                        });


                                    //defer.resolve(ret);

                                },
                                function (err) {
                                    defer.reject(err);
                                });

                        },
                        function (err) {
                            defer.reject(err);
                        });

                }, function (err) {

                    console.log(err);
                    defer.reject(err);
                });
            } else {
                offline.db.executeSql(
                    'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL  from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID where INVOICE.ID = ?', [params.invoice_id],
                    function (res) {

                        var record = res.rows.item(0);
                        checkUndefined(record);
                        var ret = {
                            status: 'Y',
                            msg: '',
                            data: {
                                id: record.ID,
                                table_num: record.FB_TABLE,
                                invoice_no: record.INVOICE_NO,
                                ticket_num: record.INVOICE_NO.slice(-4),
                                currency: record.CURRENCY,
                                gender: record.GENDER,
                                billing_area_id: emptyTozero(record.BILLING_AREA_ID),
                                billing_region_id: emptyTozero(record.BILLING_REGION_ID),
                                billing_district_id: emptyTozero(record.BILLING_DISTRICT_ID),
                                billing_country_id: emptyTozero(record.BILLING_COUNTRY_ID),
                                billing_address_1: record.BILLING_ADDRESS_1,
                                billing_address_2: record.BILLING_ADDRESS_2,
                                billing_address_3: record.BILLING_ADDRESS_3,
                                billing_address_4: record.BILLING_ADDRESS_4,
                                billing_first_name: record.BILLING_FIRST_NAME,
                                billing_last_name: record.BILLING_LAST_NAME,
                                billing_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                billing_email: record.BILLING_EMAIL,
                                billing_mobile: record.BILLING_COUNTRY_CODE + ' ' + record.BILLING_MOBILE,

                                pick_up_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                pick_up_first_name: record.BILLING_FIRST_NAME,
                                pick_up_last_name: record.BILLING_LAST_NAME,
                                pick_up_country_code: record.BILLING_COUNTRY_CODE,
                                pick_up_mobile: record.BILLING_MOBILE,
                                pick_up_email: record.BILLING_EMAIL,

                                shipping_area_id: emptyTozero(record.SHIPPING_AREA_ID),
                                shipping_region_id: emptyTozero(record.SHIPPING_REGION_ID),
                                shipping_district_id: emptyTozero(record.SHIPPING_DISTRICT_ID),
                                shipping_country_id: emptyTozero(record.SHIPPING_COUNTRY_ID),
                                shipping_address_1: record.BILLING_ADDRESS_1,
                                shipping_address_2: record.BILLING_ADDRESS_2,
                                shipping_address_3: record.BILLING_ADDRESS_3,
                                shipping_address_4: record.BILLING_ADDRESS_4,
                                shipping_first_name: record.SHIPPING_FIRST_NAME,
                                shipping_last_name: record.SHIPPING_LAST_NAME,
                                shipping_contact: record.SHIPPING_FIRST_NAME + ' ' + record.SHIPPING_LAST_NAME,
                                shipping_email: record.SHIPPING_EMAIL,
                                shipping_mobile: record.SHIPPING_COUNTRY_CODE + ' ' + record.SHIPPING_MOBILE,

                                currency: record.CURRENCY,
                                item_count: record.ITEM_COUNT,
                                item_total: emptyTozero(record.ITEM_TOTAL),
                                pay_method: record.PAY_METHOD,
                                status_num: emptyTozero(record.STATUS),

                                deposit: emptyTozero(record.PAYED_AMOUNT),
                                payment_type: record.PAYMENT_TYPE,
                                payed_amount: emptyTozero(record.PAYED_AMOUNT),

                                delivery_type: record.DELIVERY_TYPE,
                                remark: record.REMARK,
                                products: [],

                                invoice_date: record.INVOICE_DATE,
                                check_out_warehouse: emptyTozero(record.CHECK_OUT_WAREHOUSE_ID),
                                pick_up_warehouse_id: emptyTozero(record.PICK_UP_WAREHOUSE_ID),
                                pick_up_site: record.PICK_UP_SITE,
                                expected_delivery_date: record.EXPECTED_DELIVERY_DATE,
                                invoice_charges: [],
                                department_list: [],

                                delivery_total: emptyTozero(record.DELIVERY_TOTAL),
                                refund_total: emptyTozero(record.REFUND_TOTAL),
                                refund: emptyTozero(record.REFUND_TOTAL),
                                grand_total: emptyTozero(record.ITEM_TOTAL),

                                customer_name: 'Visitor',
                                customer_info: {
                                    account_no: 'visitor',
                                    user_id: 0,
                                    customer_name: '',
                                    customer_first_name: '',
                                    customer_last_name: '',
                                    customer_email: '',
                                    customer_country_code: '',
                                    customer_mobile: '',
                                    vip_level: ''
                                }
                            }
                        };

                        offline.db.executeSql(
                            'SELECT *,INVOICE_ITEM.ID as ITEM_ID, INVOICE_ITEM.ADDONS as ITEM_ADDONS from INVOICE_ITEM left join (select PRODUCT.DEPARTMENT_ID, PRODUCT.ID, PRODUCT.NAME_EN_US, PRODUCT.NAME_ZH_HK, PRODUCT.NAME_ZH_CN, PRODUCT.HIGHLIGHT_EN_US,PRODUCT.HIGHLIGHT_ZH_HK, PRODUCT.HIGHLIGHT_ZH_CN, PRODUCT.CODE, PRODUCT.WEIGHT, PRODUCT.VOLUME, PRODUCT.REMARK, PRODUCT.SPEC_FOR_OFFLINE, PRODUCT.DEPARTMENT_ID, PM.ID as PHOTO_ID from PRODUCT LEFT JOIN (SELECT MIN(ID) as ID,PRODUCT_ID FROM PRODUCT_MEDIA group by PRODUCT_ID) as PM on PRODUCT.ID = PM.PRODUCT_ID) as FP on INVOICE_ITEM.PRODUCT_ID = FP.ID where INVOICE_ID=?', [record.ID],
                            function (inv_item) {

                                ret.data.item_count = inv_item.rows.length;

                                for (var i = 0; i < inv_item.rows.length; i++) {

                                    //ret.data.item_count = inv_item.rows.length;
                                    //                     var product;
                                    //                     var invoice_item = inv_item.rows.item(0);
                                    //                     offline.db.executeSql(
                                    //                     'SELECT * from PRODUCT where ID=?', [inv_item.rows.item(i).PRODUCT_ID], function(inv_item_pro) {
                                    //
                                    //                         product = inv_item_pro.rows.item(0);
                                    //
                                    //
                                    //                     }, function(err) {
                                    //                       defer.reject(err);
                                    //                     });
                                    var invoice_item = inv_item.rows.item(i);
                                    checkUndefined(invoice_item);
                                    var photo_url = $helper.getRootPath() + secondPath + invoice_item.PHOTO_ID;

                                    var product_info = {
                                        id: invoice_item.PRODUCT_ID,
                                        item_id: invoice_item.ITEM_ID,
                                        name: invoice_item['NAME_' + params.locale],
                                        highlight: invoice_item['HIGHLIGHT_' + params.locale],
                                        photo: photo_url,
                                        qty: emptyTozero(invoice_item.QTY),
                                        buy_qty: emptyTozero(invoice_item.QTY),
                                        sku_no: invoice_item.SKU_NO,
                                        code: invoice_item.CODE,
                                        options: [],
                                        currency: invoice_item.CURRENCY,
                                        o_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        actual_unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        sub_total: emptyTozero(invoice_item.UNIT_PRICE * invoice_item.QTY),
                                        //buy_sub_total
                                        //actual sub total
                                        weight: invoice_item.WEIGHT,
                                        volumn: invoice_item.VOLUMN,
                                        remark: invoice_item.REMARK,
                                        served: invoice_item.SERVED,
                                        specifications: JSON.parse(invoice_item.SPEC_FOR_OFFLINE),
                                        addons: JSON.parse(invoice_item.ITEM_ADDONS),
                                        department_id: invoice_item.DEPARTMENT_ID,
                                        department_name: '',
                                        department_name_EN_US: '',
                                        department_name_ZH_HK: '',
                                        department_name_ZH_CN: '',

                                    };

                                    var spec_array = JSON.parse(invoice_item.SPEC_FOR_OFFLINE);

                                    var oids = [];

                                    var attribute = invoice_item.SKU_NO.split("-");
                                    var len = attribute.length;

                                    for (var j = 0; j < len - 1; j++) {
                                        var tokens = attribute[j].split("Y");
                                        var dict_id = parseInt(tokens[0].split("X")[1], 16);
                                        var opt_id = parseInt(tokens[1], 16);
                                        oids.push(opt_id);
                                    }

                                    for (var sa = 0; sa < spec_array.length; sa++) {
                                        var spec = spec_array[sa];
                                        var dict_opt = {
                                            title: spec.title,
                                            options: []
                                        };


                                        for (var oi = 0; oi < spec.options.length; oi++) {

                                            if (oids.indexOf(spec.options[oi]['id']) != -1) {

                                                dict_opt.options.push(spec.options[oi]['name']);
                                            }
                                        }

                                        product_info.options.push(dict_opt);

                                    }

                                    ret.data.products.push(product_info);


                                }



                                var baseDiscount = 0;
                                var baseCharge = 0;
                                var baseTotal = record.ITEM_TOTAL;

                                offline.db.executeSql(
                                    'SELECT * from INVOICE_CHARGES where INVOICE_ID=?', [record.ID],
                                    function (inv_charge) {

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);

                                            var charge_info = {
                                                title_EN_US: invoice_charge.NAME_EN_US,
                                                title_ZH_HK: invoice_charge.NAME_ZH_HK,
                                                title_ZH_CN: invoice_charge.NAME_ZH_CN,
                                                sign: invoice_charge.SIGN,
                                                type: invoice_charge.TYPE,
                                                value_type: invoice_charge.VALUE_TYPE,
                                                value: invoice_charge.VALUE,

                                            };
                                            ret.data.invoice_charges.push(charge_info);

                                        }

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);
                                            if (invoice_charge.VALUE_TYPE == 'percent') {
                                                if (invoice_charge.SIGN == '+') {
                                                    var totalFlag = baseTotal;
                                                    baseTotal *= 1 + Number(invoice_charge.VALUE) / 100;
                                                    baseCharge += baseTotal - totalFlag;
                                                } else {
                                                    var totalFlag = baseTotal;
                                                    baseTotal *= 1 - Number(invoice_charge.VALUE) / 100;
                                                    baseDiscount += totalFlag - baseTotal;
                                                }
                                            }

                                        }

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);
                                            if (invoice_charge.VALUE_TYPE != 'percent') {
                                                if (invoice_charge.SIGN == '+') {
                                                    var totalFlag = baseTotal;
                                                    baseTotal += Number(invoice_charge.VALUE);
                                                    baseCharge += baseTotal - totalFlag;
                                                } else {
                                                    var totalFlag = baseTotal;
                                                    baseTotal -= Number(invoice_charge.VALUE);
                                                    baseDiscount += totalFlag - baseTotal;
                                                }
                                            }

                                        }

                                        ret.data.grand_total = baseTotal;
                                        ret.data.discount_total = 0;
                                        ret.data.service_total = 0;
                                        // get department
                                        var cur_products = ret.data.products;
                                        var pro_count = 0;
                                        if (cur_products.length != 0) {
                                            for (var pro_dep = 0; pro_dep < cur_products.length; pro_dep++) {
                                                var product = cur_products[pro_dep];
                                                offline.db.executeSql(
                                                    'SELECT * from `OPTION` where ID=?', [product.department_id],
                                                    function (opt_record) {
                                                        var option = opt_record.rows.item(0);
                                                        checkUndefined(option);

                                                        ret.data.products[pro_count].department_name = option['NAME_' + params.locale];
                                                        ret.data.products[pro_count].department_name_EN_US = option.NAME_EN_US;
                                                        ret.data.products[pro_count].department_name_ZH_HK = option.NAME_ZH_HK;
                                                        ret.data.products[pro_count].department_name_ZH_CN = option.NAME_ZH_CN;
                                                        if (ret.data.department_list.indexOf(option.ID) == -1) {
                                                            ret.data.department_list.push(option.ID);
                                                        }
                                                        pro_count++;
                                                        if (pro_count == cur_products.length) {
                                                            defer.resolve(ret);
                                                        }
                                                    },
                                                    function (err) {
                                                        defer.reject(err);
                                                    });


                                            }
                                        } else {
                                            defer.resolve(ret);
                                        }

                                        // get department
                                        // if no get department                                defer.resolve(ret);

                                    },
                                    function (err) {
                                        defer.reject(err);
                                    });
                                //defer.resolve(ret);
                            },
                            function (err) {
                                defer.reject(err);
                            });

                    },
                    function (err) {
                        defer.reject(err);
                    });
            }
            return defer.promise;

        };

        offline.newCart = function (params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var beautiful_time = '' + time_now.getFullYear() + '-' + (time_now.getMonth() + 1) + '-' + time_now.getDate() + ' ' + time_now.getHours() + ':' + time_now.getMinutes() + ':' + time_now.getSeconds();

            if (typeof params.user_id != "undefined" && params.user_id != null) {
                offline.db.executeSql(
                    'SELECT * FROM MEMBER_PROFILE where USER_ID = ?', [params.user_id],
                    function (member_res_original) {

                        if (member_res_original.rows.length != 0) {
                            ret = {
                                status: 'Y',
                                msg: ''
                            }
                            member_res = member_res_original.rows.item(0);
                            checkUndefined(member_res);
                            offline.db.executeSql(
                                'INSERT INTO INVOICE (CREATE_DATE, UPDATE_DATE, INVOICE_DATE, STATUS, BILLING_FIRST_NAME, BILLING_LAST_NAME, GENDER, CHECK_OUT_WAREHOUSE_ID, USER_ID, BILLING_EMAIL, BILLING_COUNTRY_CODE, BILLING_MOBILE, ITEM_TOTAL, FB_TABLE, PAYED_AMOUNT,CARRY_UP) VALUES (?,?,?, ?, ?, ?,?,?,?, ?, ?, ?,0,0,0,0)', [timestamp, timestamp, beautiful_time, 0, member_res.FIRST_NAME, member_res.LAST_NAME, member_res.GENDER, params.check_out_warehouse_id, params.user_id, member_res.EMAIL, member_res.COUNTRY, member_res.MOBILE],
                                function (tx, res) {
                                    var ret = {
                                        status: 'Y',
                                        msg: '',
                                        invoice_id: tx.insertId,
                                        billing_last_name: member_res.LAST_NAME,
                                        gender: member_res.GENDER,
                                    };


                                    defer.resolve(ret);

                                },
                                function (err) {
                                    defer.reject(err);
                                });
                        } else {
                            ret = {
                                status: 'E',
                                msg: 'no such member'
                            }
                            defer.resolve(ret);
                        }


                    },
                    function (err) {
                        defer.reject(err);
                    });

            } else {
                offline.db.executeSql(
                    'INSERT INTO INVOICE (CREATE_DATE, UPDATE_DATE, INVOICE_DATE, STATUS, BILLING_LAST_NAME, GENDER, CHECK_OUT_WAREHOUSE_ID, FB_TABLE, ITEM_TOTAL, PAYED_AMOUNT, CARRY_UP) VALUES (?, ?,?,?,?, ?, ?,0,0,0,0)', [timestamp, timestamp, beautiful_time, 0, params.billing_last_name, params.gender, params.check_out_warehouse_id],
                    function (tx, res) {
                        var ret = {
                            status: 'Y',
                            msg: '',
                            invoice_id: tx.insertId,
                            billing_last_name: params.billing_last_name,
                            gender: params.gender,
                        };


                        defer.resolve(ret);

                    },
                    function (err) {
                        defer.reject(err);
                    });
            }



            return defer.promise;
        };

        offline.setCart = function (params) {
            var defer = $q.defer();
            switch (params.action) {
                case 'read':
                    offline.getInvoiceDetail({

                        invoice_id: params.invoice_id,
                        locale: params.locale,
                        fb_table: params.table_num

                    }).then(function (ret) {

                        defer.resolve(ret);

                    });
                    break;
                case 'add':
                    offline.addToCart({

                        params: params



                    }).then(function (ret_add) {

                        offline.getInvoiceDetail({

                            invoice_id: params.invoice_id,
                            locale: params.locale

                        }).then(function (ret) {
                            ret.item_id = ret_add.item_id;
                            defer.resolve(ret);

                        });

                    });


                    break;
                case 'update':
                    offline.updateCart({

                        params: params



                    }).then(function (ret_update) {

                        offline.getInvoiceDetail({

                            invoice_id: params.invoice_id,
                            locale: params.locale

                        }).then(function (ret) {
                            ret.item_id = ret_update.item_id;
                            defer.resolve(ret);

                        });

                    });

                    break;
                case 'remove':
                    offline.removeFromCart({

                        params: params



                    }).then(function (ret) {

                        offline.getInvoiceDetail({

                            invoice_id: params.invoice_id,
                            locale: params.locale

                        }).then(function (ret) {

                            defer.resolve(ret);

                        });

                    });
                    break;
                case 'empty':
                    offline.EmptyCart({

                        params: params



                    }).then(function (ret) {

                        offline.getInvoiceDetail({

                            invoice_id: params.invoice_id,
                            locale: params.locale

                        }).then(function (ret) {

                            defer.resolve(ret);

                        });

                    });
                    break;
                case 'delete':
                    offline.deleteCart({

                        params: params



                    }).then(function (ret) {

                        defer.resolve(ret);


                    });

                    break;
                case 'address':

                    offline.addressCart({

                        params: params



                    }).then(function (ret) {

                        offline.getInvoiceDetail({

                            invoice_id: params.invoice_id,
                            locale: params.locale

                        }).then(function (ret) {

                            defer.resolve(ret);

                        });

                    });


                    break;
                case 'serve':

                    offline.serveCart({
                        params: params
                    }).then(function (ret) {
                        console.log(ret);
                        defer.resolve(ret);

                    });


                    break;
            }
            return defer.promise;
        };
        offline.serveCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;
            if (typeof params.served_item == "undefined") {
                var ret = {
                    status: 'E',
                    payed: 'E',
                    msg: 'Data Empty'
                };
                defer.resolve(ret);

            }

            var served_items = JSON.parse(params.served_item);
            var count = 0;
            for (var i = 0; i < served_items.length; i++) {

                offline.db.executeSql('UPDATE "INVOICE_ITEM" set "SERVED" = ?, "UPDATE_DATE" = ? where ID = ?', [served_items[i].served, timestamp, served_items[i].item_id], function (update_res) {
                    count++;
                    if (served_items.length == count) {
                        var ret = {
                            status: 'Y',
                            payed: 'E',
                            msg: 'SUCCESS'
                        }

                        offline.db.executeSql(
                            'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL, count(case when SERVED = 1 AND QTY > 0 then 1 else null end) as SERVED_QTY, count(case when QTY > 0 then 1 else null end) as CC_QTY  from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID where INVOICE.ID = ? and INVOICE.STATUS = ?', [params.invoice_id, 2],
                            function (res) {
                                if (res.rows.length != 0) {
                                    var record = res.rows.item(0);
                                    checkUndefined(record);
                                    if (emptyTozero(record.SERVED_QTY) == emptyTozero(record.CC_QTY)) {
                                        offline.confirmPayment({

                                            invoice_id: params.invoice_id,
                                            all_served: 'y',

                                        }).then(function (ret_c) {
                                            ret.payed = 'Y';
                                            defer.resolve(ret);

                                        });
                                    } else {
                                        defer.resolve(ret);
                                    }

                                } else {
                                    defer.resolve(ret);
                                }



                            },
                            function (err) {

                                console.log(err);
                                defer.reject(err);
                            });




                    }

                }, function (err) {

                    console.log(err);
                    defer.reject(err);
                });

            }
            return defer.promise;

        };
        offline.addToCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;
            var sku = '';
            if (typeof params.spec != "undefined") {

                sku = encodePreSku(params.spec);
            }
            sku = sku + 'P' + params.product_id.toString(16);
            if (typeof params.sku_no != "undefined") {
                sku = params.sku_no;
            }

            var add_on_json = '[]';
            var add_on_price = 0;

            if (typeof params.addons != "undefined") {
                add_on_json = params.addons;
                var addons_array = JSON.parse(add_on_json);
                for (var r = 0; r < addons_array.length; r++) {
                    add_on_price = parseFloat(add_on_price) + parseFloat(addons_array[r].unit_price) * parseFloat(addons_array[r].qty);
                }


            }


            offline.db.executeSql('SELECT * FROM "PRODUCT_SKU_INFO" WHERE "SKU_NO" = ?', [sku], function (find_sku_res) {


                if (find_sku_res.rows.length != 0) {
                    var sku_inf = find_sku_res.rows.item(0);
                    offline.db.executeSql('SELECT * FROM "INVOICE_ITEM" WHERE "INVOICE_ID" = ? AND "PRODUCT_ID" = ? and "SKU_NO" = ?', [params.invoice_id, params.product_id, sku], function (res) {

                        var ret = {
                            status: 'Y',
                            msg: ''
                        }

                        offline.db.executeSql('INSERT INTO "INVOICE_ITEM" (CREATE_DATE, UPDATE_DATE, INVOICE_ID, PRODUCT_ID, UNIT_PRICE, QTY, SKU_NO, STATUS, ADDONS) VALUES(?,?,?,?,?,?,?,?,?)', [timestamp, timestamp, params.invoice_id, params.product_id, (parseFloat(sku_inf.PRICE) + parseFloat(add_on_price)), params.qty, sku, 0, add_on_json], function (tx, add_res) {
                            //console.log(add_res);
                            //console.log('added');
                            //update stock
                            var ret = {
                                status: 'Y',
                                msg: '',
                                item_id: tx.insertId
                            }

                            defer.resolve(ret);
                        }, function (err) {

                            defer.reject(err);
                        });
                    }, function (err) {
                        defer.reject(err);
                    });
                } else {
                    var ret = {
                        status: 'E',
                        msg: 'sku not found'
                    }
                    defer.resolve(ret);
                }

            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        offline.updateCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;
            var sku = '';
            if (typeof params.spec != "undefined") {

                sku = encodePreSku(params.spec);
            }
            sku = sku + 'P' + params.product_id.toString(16);
            if (typeof params.sku_no != "undefined") {
                sku = params.sku_no;
            }
            var add_on_json = '[]';
            var add_on_price = 0;
            if (typeof params.addons != "undefined") {

                add_on_json = params.addons;
                var addons_array = JSON.parse(add_on_json);
                for (var r = 0; r < addons_array.length; r++) {
                    add_on_price = parseFloat(add_on_price) + parseFloat(addons_array[r].unit_price) * parseFloat(addons_array[r].qty);
                }


            }
            offline.db.executeSql('SELECT * FROM "PRODUCT_SKU_INFO" WHERE "SKU_NO" = ?', [sku], function (find_sku_res) {


                if (find_sku_res.rows.length != 0) {
                    var sku_inf = find_sku_res.rows.item(0);

                    var exec_sql = 'SELECT * FROM "INVOICE_ITEM" WHERE "INVOICE_ID" = ' + params.invoice_id + ' AND "PRODUCT_ID" = ' + params.product_id + ' and "SKU_NO" = ' + sku + '';

                    if (typeof params.item_id != "undefined") {
                        exec_sql = 'SELECT * FROM "INVOICE_ITEM" WHERE "ID" = ' + params.item_id;
                    }

                    offline.db.executeSql(exec_sql, [], function (res) {

                        var ret = {
                            status: 'Y',
                            msg: ''
                        }

                        if (res.rows.length != 0) {
                            var record = res.rows.item(0);
                            checkUndefined(record);
                            offline.db.executeSql('UPDATE "INVOICE_ITEM" set "QTY" = ?, "ADDONS" = ?, "UNIT_PRICE" = ? where ID = ?', [params.qty, add_on_json, (parseFloat(sku_inf.PRICE) + parseFloat(add_on_price)), record.ID], function (update_res) {
                                defer.resolve(ret);
                            }, function (err) {
                                defer.reject(err);
                            });
                            defer.resolve(ret);
                        } else {
                            //console.log('enter else');
                            offline.db.executeSql('INSERT INTO "INVOICE_ITEM" (CREATE_DATE, UPDATE_DATE, INVOICE_ID, PRODUCT_ID, UNIT_PRICE, QTY, SKU_NO, STATUS, ADDONS) VALUES(?,?,?,?,?,?,?,?,?)', [timestamp, timestamp, params.invoice_id, params.product_id, (parseFloat(sku_inf.PRICE) + parseFloat(add_on_price)), params.qty, sku, 0, add_on_json], function (tx, add_res) {
                                //console.log(add_res);
                                //console.log('added');
                                //update stock
                                var ret = {
                                    status: 'Y',
                                    msg: '',
                                    item_id: tx.insertId
                                }

                                defer.resolve(ret);

                            }, function (err) {
                                defer.reject(err);
                            });
                            defer.resolve(ret);
                        }


                    }, function (err) {
                        defer.reject(err);
                    });
                } else {
                    var ret = {
                        status: 'E',
                        msg: 'sku not found'
                    }
                    defer.resolve(ret);
                }

            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        offline.EmptyCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;

            var exec_sql = 'SELECT * FROM "INVOICE_ITEM" WHERE "INVOICE_ID" = ' + params.invoice_id;

            offline.db.executeSql(exec_sql, [], function (res) {

                var ret = {
                    status: 'Y',
                    msg: ''
                }

                if (res.rows.length != 0) {

                    for (var i = 0; i < res.rows.length; i++) {
                        var record = res.rows.item(i);
                        checkUndefined(record);
                        if (record.UPDATE_DATE == record.CREATE_DATE) {
                            offline.db.executeSql('DELETE FROM "INVOICE_ITEM" where ID = ?', [record.ID], function (update_res) {

                                defer.resolve(ret);

                            }, function (err) {

                                defer.reject(err);
                            });
                        }
                        else {
                            offline.db.executeSql('UPDATE "INVOICE_ITEM" SET QTY = 0 where ID = ?', [record.ID], function (update_res) {

                                defer.resolve(ret);

                            }, function (err) {

                                defer.reject(err);
                            });
                        }
                    }


                } else {
                    ret.status = 'E';
                    defer.resolve(ret);
                }


            }, function (err) {
                console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        };


        offline.removeFromCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;
            var sku = '';
            if (typeof params.spec != "undefined") {

                sku = encodePreSku(params.spec);
            }
            sku = sku + 'P' + params.product_id.toString(16);
            if (typeof params.sku_no != "undefined") {
                sku = params.sku_no;
            }



            offline.db.executeSql('SELECT * FROM "PRODUCT_SKU_INFO" WHERE "SKU_NO" = ?', [sku], function (find_sku_res) {


                if (find_sku_res.rows.length != 0) {
                    var sku_inf = find_sku_res.rows.item(0);

                    var exec_sql = 'SELECT * FROM "INVOICE_ITEM" WHERE "INVOICE_ID" = ' + params.invoice_id + ' AND "PRODUCT_ID" = ' + params.product_id + ' and "SKU_NO" = ' + sku + '';

                    if (typeof params.item_id != "undefined") {
                        exec_sql = 'SELECT * FROM "INVOICE_ITEM" WHERE "ID" = ' + params.item_id;
                    }


                    offline.db.executeSql(exec_sql, [], function (res) {

                        var ret = {
                            status: 'Y',
                            msg: ''
                        }

                        if (res.rows.length != 0) {
                            var record = res.rows.item(0);
                            checkUndefined(record);

                            if (record.UPDATE_DATE == record.CREATE_DATE) {
                                offline.db.executeSql('DELETE FROM "INVOICE_ITEM" where ID = ?', [record.ID], function (update_res) {

                                    defer.resolve(ret);

                                }, function (err) {

                                    defer.reject(err);
                                });
                            }
                            else {
                                offline.db.executeSql('UPDATE "INVOICE_ITEM" SET QTY = 0 where ID = ?', [record.ID], function (update_res) {

                                    defer.resolve(ret);

                                }, function (err) {

                                    defer.reject(err);
                                });
                            }

                        } else {
                            ret.status = 'E';
                            defer.resolve(ret);
                        }
                    }, function (err) {
                        defer.reject(err);
                    });
                } else {
                    var ret = {
                        status: 'E',
                        msg: 'sku not found'
                    }
                    defer.resolve(ret);
                }

            }, function (err) {
                //console.log(err);
                defer.reject(err);
            });

            return defer.promise;
        };

        offline.deleteCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;

            offline.db.executeSql('SELECT * FROM "INVOICE_ITEM" WHERE "UPDATE_DATE" != "CREATE_DATE"', [], function (cc_res) {

                if (cc_res.rows.length != 0) {
                    var ret = {
                        status: 'E',
                        msg: 'ITEM SERVED, CANT DELETE CART'
                    }
                    defer.resolve(ret);
                }
                else {
                    offline.db.executeSql('DELETE FROM "INVOICE_ITEM" WHERE "INVOICE_ID" = ?', [params.invoice_id], function (res) {

                        offline.db.executeSql('DELETE FROM "INVOICE" WHERE "ID" = ?', [params.invoice_id], function (res) {

                            var ret = {
                                status: 'Y',
                                msg: ''
                            }


                            offline.db.executeSql('DELETE FROM "INVOICE_CHARGES" where INVOICE_ID = ?', [params.invoice_id], function (res) {

                                defer.resolve(ret);
                            }, function (err) {
                                //console.log(err);
                                defer.reject(err);
                            });

                        }, function (err) {
                            //console.log(err);
                            defer.reject(err);
                        });

                    }, function (err) {
                        //console.log(err);
                        defer.reject(err);
                    });
                }


            }, function (err) {
                console.log(err);
            });

            return defer.promise;
        };

        offline.addressCart = function (o_params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            var params = o_params.params;
            checkUndefined(params);
            //console.log('in address');
            //console.log(params);
            var b_fn = params.billing_first_name;
            var b_ln = params.billing_last_name;
            var b_country_code = params.billing_country_code;
            var b_mobile = params.billing_mobile;
            var b_email = params.billing_email;
            var pick_warehouse_id = params.pick_up_warehouse_id;
            if (params.delivery_type == 'pick up') {
                b_fn = params.pick_up_first_name;
                b_ln = params.pick_up_last_name;
                b_country_code = params.pick_up_country_code;
                b_mobile = params.pick_up_mobile;
                b_email = params.pick_up_email;
            }
            offline.db.executeSql('UPDATE "INVOICE" set BILLING_FIRST_NAME = ?, BILLING_LAST_NAME = ?, BILLING_EMAIL = ?, BILLING_COUNTRY_CODE = ?, BILLING_MOBILE = ?, REMARK = ?, DELIVERY_TYPE = ?, PAYMENT_TYPE = ?, PAYED_AMOUNT = ?, PICK_UP_WAREHOUSE_ID = ?, PICK_UP_SITE = ? where ID = ?', [b_fn, b_ln, b_email, b_country_code, b_mobile, params.remark, params.delivery_type, params.payment_type, params.payed_amount, pick_warehouse_id, params.pick_up_site, params.invoice_id], function (res) {
                //console.log('good sql');
                var ret = {
                    status: 'Y',
                    msg: ''
                }
                // defer.resolve(ret);

                offline.db.executeSql('DELETE FROM "INVOICE_CHARGES" where INVOICE_ID = ?', [params.invoice_id], function (res) {

                    var ret = {
                        status: 'Y',
                        msg: '',
                    }

                    if (typeof params.invoice_charges != "undefined") {
                        var charge_array = JSON.parse(params.invoice_charges);
                        for (var c = 0; c < charge_array.length; c++) {
                            offline.db.executeSql('INSERT INTO "INVOICE_CHARGES" (CREATE_DATE, UPDATE_DATE, INVOICE_ID, NAME_EN_US, NAME_ZH_HK, NAME_ZH_CN, SIGN, TYPE,VALUE_TYPE,VALUE) VALUES(?,?,?,?,?,?,?,?,?,?)', [timestamp, timestamp, params.invoice_id, charge_array[c].title_EN_US, charge_array[c].title_ZH_HK, charge_array[c].title_ZH_CN, charge_array[c].sign, charge_array[c].type, charge_array[c].value_type, charge_array[c].value], function (charge_res) {

                                if (c == charge_array.length) {
                                    defer.resolve(ret);
                                }


                            }, function (err) {
                                //console.log('error');
                                //console.log(err);
                                defer.reject(err);
                            });
                        }
                        if (charge_array.length == 0) {
                            defer.resolve(ret);
                        }
                    } else {
                        defer.resolve(ret);
                    }


                    //console.log(ret);
                    //defer.resolve(ret);
                }, function (err) {
                    //console.log(err);
                    defer.reject(err);
                });



            }, function (err) {
                //console.log(err);
                defer.reject(err);
            });
            return defer.promise;

        }

        offline.checkOut = function (params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var time_now = new Date();
            var timestamp = time_now.getTime();
            checkUndefined(params);
            console.log('in checkout');
            var b_fn = params.billing_first_name;
            var b_ln = params.billing_last_name;
            var b_country_code = params.billing_country_code;
            var b_mobile = params.billing_mobile;
            var b_email = params.billing_email;
            var pick_warehouse_id = params.pick_up_warehouse_id;
            if (params.delivery_type == 'pick up') {
                b_fn = params.pick_up_first_name;
                b_ln = params.pick_up_last_name;
                b_country_code = params.pick_up_country_code;
                b_mobile = params.pick_up_mobile;
                b_email = params.pick_up_email;
            }
            var yyyy = time_now.getFullYear().toString();
            yyyy = yyyy.slice(-2);

            var mm = '00' + (time_now.getMonth() + 1).toString(); // getMonth() is zero-based
            mm = mm.slice(-2);
            var dd = '00' + time_now.getDate().toString();
            dd = dd.slice(-2);
            var apid = '0000' + params.invoice_id;
            apid = apid.slice(-4);

            var invoice_no = 'OFFLINE-' + yyyy + mm + dd + apid;

            var all_served = 'n';
            if (typeof params.all_served != "undefined" && params.all_served == 'y') {
                all_served = 'y';
            }
            var qr = new QRious({ value: invoice_no });
            var invoice_no_qr = qr.toDataURL();
            console.log('before sql');
            offline.db.executeSql('UPDATE "INVOICE" set BILLING_FIRST_NAME = ?, BILLING_LAST_NAME = ?, BILLING_EMAIL = ?, BILLING_COUNTRY_CODE = ?, BILLING_MOBILE = ?, REMARK = ?, DELIVERY_TYPE = ?, PAYMENT_TYPE = ?, PAYED_AMOUNT = ?, PAY_METHOD = ?, INVOICE_NO = ?, PICK_UP_WAREHOUSE_ID = ?, PICK_UP_SITE = ? where ID = ?', [b_fn, b_ln, b_email, b_country_code, b_mobile, params.remark, params.delivery_type, params.payment_type, params.payed_amount, params.pay_method, invoice_no, pick_warehouse_id, params.pick_up_site, params.invoice_id],
                function (res) {

                    var ret = {
                        status: 'Y',
                        msg: '',
                        invoice_id: params.invoice_id,
                        invoice_no: invoice_no,
                        ticket_num: apid,
                        pdf: pdfPath,
                        data: []
                    }

                    offline.db.executeSql('DELETE FROM "INVOICE_CHARGES" where INVOICE_ID = ?', [params.invoice_id], function (res) {

                        // var ret = {
                        //     status : 'Y',
                        //     msg : '',
                        //     invoice_id : params.invoice_id,
                        //     invoice_no : invoice_no,
                        //     pdf : $helper.getRootPath() + '/invoice.pdf'
                        // }

                        if (typeof params.invoice_charges != "undefined") {
                            var charge_array = JSON.parse(params.invoice_charges);
                            for (var c = 0; c < charge_array.length; c++) {
                                offline.db.executeSql('INSERT INTO "INVOICE_CHARGES" (CREATE_DATE, UPDATE_DATE, INVOICE_ID, NAME_EN_US, NAME_ZH_HK, NAME_ZH_CN, SIGN, TYPE,VALUE_TYPE,VALUE) VALUES(?,?,?,?,?,?,?,?,?,?)', [timestamp, timestamp, params.invoice_id, charge_array[c].title_EN_US, charge_array[c].title_ZH_HK, charge_array[c].title_ZH_CN, charge_array[c].sign, charge_array[c].type, charge_array[c].value_type, charge_array[c].value], function (charge_res) {
                                    //console.log(c);
                                    if (c == charge_array.length) {
                                        offline.getInvoiceDetail(params).then(function (detail_ret) {
                                            //console.log(JSON.stringify(detail_ret));
                                            ret.data = detail_ret.data;

                                            var products = [];
                                            var i = 1;

                                            angular.forEach(detail_ret.data.products, function (p) {

                                                var dis = '';
                                                if ((p.o_price - p.unit_price) / p.o_price == 0) {
                                                    dis = '- ';
                                                } else {
                                                    dis = (p.o_price - p.unit_price) / p.o_price + '%';
                                                }
                                                products.push({
                                                    ITEM_NO: i,
                                                    PRODUCT_CODE: p.code,
                                                    PRODUCT_NAME: p.name,
                                                    QTY: p.buy_qty,
                                                    UNIT_PRICE: p.unit_price,
                                                    DISCOUNT: dis,
                                                    SUB_TOTAL: '$' + p.sub_total.toFixed(2)
                                                });
                                                i++;
                                            });

                                            var charges = [];
                                            angular.forEach(detail_ret.data.invoice_charges, function (c) {
                                                var charge_value = '';
                                                if (c.sign == '+') {
                                                    if (c.value_type == 'value') {
                                                        charge_value = '$' + c.value.toFixed(2);
                                                    } else {
                                                        charge_value = c.value.toFixed(2) + '%';
                                                    }
                                                } else {
                                                    if (c.value_type == 'value') {
                                                        charge_value = '($' + c.value.toFixed(2) + ')';
                                                    } else {
                                                        charge_value = '(' + c.value.toFixed(2) + '%)';
                                                    }
                                                }
                                                charges.push({
                                                    CHARGE_NAME: c.title_EN_US,
                                                    CHARGE_VALUE: charge_value,
                                                    CHARGE_VALUE_TYPE: c.value_type,
                                                    CHARGE_SIGN: c.sign
                                                });
                                            });
                                            if (typeof params.confirm != "undefined" && params.confirm == 'y') {
                                                offline.confirmPayment({

                                                    invoice_id: params.invoice_id,
                                                    all_served: all_served,

                                                }).then(function (ret_c) {

                                                    defer.resolve(ret);

                                                });
                                            } else {
                                                defer.resolve(ret);
                                            }
                                            //defer.resolve(ret);

                                        });
                                    }
                                }, function (err) {
                                    //console.log('error');
                                    //console.log(err);
                                    defer.reject(err);
                                });
                            }
                            if (charge_array.length == 0) {

                                offline.getInvoiceDetail(params).then(function (detail_ret) {
                                    //console.log(JSON.stringify(detail_ret));
                                    ret.data = detail_ret.data;

                                    var products = [];
                                    var i = 1;

                                    angular.forEach(detail_ret.data.products, function (p) {

                                        var dis = '';
                                        if ((p.o_price - p.unit_price) / p.o_price == 0) {
                                            dis = '- ';
                                        } else {
                                            dis = (p.o_price - p.unit_price) / p.o_price + '%';
                                        }
                                        products.push({
                                            ITEM_NO: i,
                                            PRODUCT_CODE: p.code,
                                            PRODUCT_NAME: p.name,
                                            QTY: p.buy_qty,
                                            UNIT_PRICE: p.unit_price,
                                            DISCOUNT: dis,
                                            SUB_TOTAL: '$' + p.sub_total.toFixed(2)
                                        });
                                        i++;
                                    });

                                    var charges = [];
                                    angular.forEach(detail_ret.data.invoice_charges, function (c) {
                                        var charge_value = '';
                                        if (c.sign == '+') {
                                            if (c.value_type == 'value') {
                                                charge_value = '$' + c.value.toFixed(2);
                                            } else {
                                                charge_value = c.value.toFixed(2) + '%';
                                            }
                                        } else {
                                            if (c.value_type == 'value') {
                                                charge_value = '($' + c.value.toFixed(2) + ')';
                                            } else {
                                                charge_value = '(' + c.value.toFixed(2) + '%)';
                                            }
                                        }
                                        charges.push({
                                            CHARGE_NAME: c.title_EN_US,
                                            CHARGE_VALUE: charge_value,
                                            CHARGE_VALUE_TYPE: c.value_type,
                                            CHARGE_SIGN: c.sign
                                        });
                                    });
                                    if (typeof params.confirm != "undefined" && params.confirm == 'y') {
                                        offline.confirmPayment({

                                            invoice_id: params.invoice_id,
                                            all_served: all_served,

                                        }).then(function (ret_c) {

                                            defer.resolve(ret);

                                        });
                                    } else {
                                        defer.resolve(ret);
                                    }


                                });
                            }

                        } else {
                            offline.getInvoiceDetail(params).then(function (detail_ret) {
                                //console.log(JSON.stringify(detail_ret));
                                ret.data = detail_ret.data;

                                var products = [];
                                var i = 1;

                                angular.forEach(detail_ret.data.products, function (p) {

                                    var dis = '';
                                    if ((p.o_price - p.unit_price) / p.o_price == 0) {
                                        dis = '- ';
                                    } else {
                                        dis = (p.o_price - p.unit_price) / p.o_price + '%';
                                    }
                                    products.push({
                                        ITEM_NO: i,
                                        PRODUCT_CODE: p.code,
                                        PRODUCT_NAME: p.name,
                                        QTY: p.buy_qty,
                                        UNIT_PRICE: p.unit_price,
                                        DISCOUNT: dis,
                                        SUB_TOTAL: '$' + p.sub_total.toFixed(2)
                                    });
                                    i++;
                                });

                                var charges = [];
                                angular.forEach(detail_ret.data.invoice_charges, function (c) {
                                    var charge_value = '';
                                    if (c.sign == '+') {
                                        if (c.value_type == 'value') {
                                            charge_value = '$' + c.value.toFixed(2);
                                        } else {
                                            charge_value = c.value.toFixed(2) + '%';
                                        }
                                    } else {
                                        if (c.value_type == 'value') {
                                            charge_value = '($' + c.value.toFixed(2) + ')';
                                        } else {
                                            charge_value = '(' + c.value.toFixed(2) + '%)';
                                        }
                                    }
                                    charges.push({
                                        CHARGE_NAME: c.title_EN_US,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: c.value_type,
                                        CHARGE_SIGN: c.sign
                                    });
                                });

                                if (typeof params.confirm != "undefined" && params.confirm == 'y') {
                                    offline.confirmPayment({

                                        invoice_id: params.invoice_id,
                                        all_served: all_served,

                                    }).then(function (ret_c) {

                                        defer.resolve(ret);

                                    });
                                } else {
                                    defer.resolve(ret);
                                }


                            });
                        }
                    }, function (err) {
                        //console.log(err);
                        defer.reject(err);
                    });
                },
                function (err) {
                    //console.log(err);
                    defer.reject(err);
                });
            return defer.promise;
        }

        offline.confirmPayment = function (params) {
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();

            checkUndefined(params);

            offline.db.executeSql(
                'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL, count(case when SERVED = 1 AND QTY > 0 then 1 else null end) as SERVED_QTY, count(case when QTY > 0 then 1 else null end) as CC_QTY  from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID where INVOICE.ID = ?', [params.invoice_id],
                function (res) {
                    var ret = {
                        status: 'Y',
                        msg: '',
                        data: {
                            count: res.rows.length,
                            list: []
                        }
                    };
                    console.log('before length');
                    if (res.rows.length > 0) {
                        console.log('after length');
                        var record = res.rows.item(0);
                        checkUndefined(record);
                        var serve_qty = emptyTozero(record.SERVED_QTY);
                        var item_qty = emptyTozero(record.CC_QTY);
                        var table_num = emptyTozero(record.FB_TABLE);

                        var delivery_type = "";
                        if (record.DELIVERY_TYPE == "") {
                            delivery_type = delivery_type + ", DELIVERY_TYPE = 'direct sales'";
                        }

                        if (typeof params.all_served != "undefined" && params.all_served == 'y') {
                            serve_qty = item_qty;
                        }

                        if (serve_qty != item_qty) {

                            if (record.INVOICE_NO == "") {
                                var ret = {
                                    status: 'E',
                                    msg: 'Please Print First'
                                };
                                defer.resolve(ret);

                            } else {
                                console.log('before sql');
                                offline.db.executeSql('UPDATE "INVOICE" set STATUS = ?' + delivery_type + ' where ID = ?', [2, params.invoice_id], function (res) {

                                    var ret = {
                                        status: 'Y',
                                        msg: '',
                                        invoice_id: params.invoice_id,
                                    }

                                    console.log(ret);
                                    defer.resolve(ret);
                                }, function (err) {
                                    //console.log(err);
                                    defer.reject(err);
                                });
                            }

                        } else {
                            offline.db.executeSql('UPDATE "INVOICE" set STATUS = ? where ID = ?', [5, params.invoice_id], function (res) {

                                var ret = {
                                    status: 'Y',
                                    msg: '',
                                    invoice_id: params.invoice_id,
                                }

                                //console.log(ret);
                                defer.resolve(ret);
                            }, function (err) {
                                //console.log(err);
                                defer.reject(err);
                            });
                        }
                    } else {
                        ret = {
                            status: 'E',
                            msg: 'Invoice Not Found',
                        };
                    }

                },
                function (err) {
                    defer.reject(err);
                });

            /*
            offline.db.executeSql('UPDATE "INVOICE" set STATUS = ? where ID = ?', [5, params.invoice_id], function(res) {

                var ret = {
                    status: 'Y',
                    msg: '',
                    invoice_id: params.invoice_id,
                }

                //console.log(ret);
                defer.resolve(ret);
            }, function(err) {
                //console.log(err);
                defer.reject(err);
            });
            */



            return defer.promise;
        }

        offline.getProductList = function (params) {

            // 2017-6-1 - TK: sample code for synchronize offline api
            if (!$rootScope.stopSyncCommand) {
                offline.syncCommand({ method: 'offline.getProductList', params: params });
            }

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var cat_param = "";
            if (params.category_ids && Array.isArray(params.category_ids)) {

                cat_param = cat_param + " AND (";
                for (var n = 0; n < params.category_ids.length; n++) {
                    if (n != 0) {
                        cat_param = cat_param + ' OR '
                    }
                    cat_param = cat_param + "CATEGORY_IDS LIKE '%," + params.category_ids[n] + ",%'";

                }
                cat_param = cat_param + ')';
            }
            var search = "";
            //console.log(params.keyword);
            if (typeof params.keyword != "undefined" && params.keyword != null) {
                search = search + " AND (";
                search = search + "NAME_EN_US LIKE '%" + params.keyword + "%' OR ";
                search = search + "NAME_ZH_HK LIKE '%" + params.keyword + "%' OR ";
                search = search + "NAME_ZH_CN LIKE '%" + params.keyword + "%')";

            }
            var limit_from = 0;
            var limit = 20;
            if (typeof params.limit_from != "undefined" && params.limit_from != null) {
                limit_from = params.limit_from;
            }
            if (typeof params.limit != "undefined" && params.limit != null) {
                limit = params.limit;
            }
            //console.log('!!!!!!!!');
            //console.log(params);
            //console.log(cat_param);
            //console.log(search);

            //console.log('SELECT * from PRODUCT LEFT JOIN (SELECT MIN(ID) as ID,PRODUCT_ID FROM PRODUCT_MEDIA group by PRODUCT_ID) as PM on PRODUCT.ID = PM.PRODUCT_ID where PRODUCT.ENABLE_FOR_POS = 1 ' + cat_param + search + ' ORDER BY PRODUCT.SORTING DESC, PRODUCT.ID DESC LIMIT ? OFFSET ? ');
            offline.db.executeSql(
                'SELECT PRODUCT.*, PRODUCT_MEDIA.ID AS MEDIA_ID FROM PRODUCT LEFT OUTER JOIN PRODUCT_MEDIA ON PRODUCT.ID = PRODUCT_MEDIA.PRODUCT_ID WHERE PRODUCT.ENABLE_FOR_POS = 1 ' + cat_param + search + ' GROUP BY PRODUCT.ID ORDER BY PRODUCT.SORTING DESC, PRODUCT.ID DESC', [],
                //         offline.db.executeSql(
                //             'SELECT * from PRODUCT LEFT JOIN (SELECT MIN(ID) as ID,PRODUCT_ID FROM PRODUCT_MEDIA group by PRODUCT_ID) as PM on PRODUCT.ID = PM.PRODUCT_ID where PRODUCT.ENABLE_FOR_POS = 1 ' + cat_param + search + ' ORDER BY PRODUCT.SORTING DESC, PRODUCT.ID DESC', [],
                function (res) {
                    //console.log(res);
                    var ret = {
                        status: 'Y',
                        msg: '',
                        data: {
                            advertisements: {
                                count: 0,
                                list: []
                            },
                            products: {
                                count: res.rows.length,
                                list: []
                            }

                        }
                    };
                    //console.log('cao~~~'+res.rows.length);
                    for (var i = limit_from; i < limit + limit_from && i < res.rows.length; i++) {

                        var record = res.rows.item(i);
                        var photo_url = $helper.getRootPath() + secondPath + record.MEDIA_ID;

                        checkUndefined(record);
                        var product_info = {
                            id: record.ID,
                            code: record.CODE,
                            gift: record.GIFT,
                            category_ids: record.CATEGORY_IDS.substring(1, record.CATEGORY_IDS.length - 1).split(","),
                            name: record['NAME_' + params.locale],
                            highlight: record['HIGHLIGHT_' + params.locale],
                            photo: photo_url,
                            currency: record.CURRENCY,
                            price: record.OFF_PRICE,
                            //specifications : offline.getProductSpec(record.ID),
                            //original_price : offline.getProductPrice(record.ID, record.CURRENCY),
                            qty: record.QTY,
                            presale_qty: record.PRESALE_QTY,
                            sorting: record.SORTING,
                            enabled: record.ENABLED,
                            enable_for_pos: record.ENABLE_FOR_POS,
                            show_price: record.SHOP_PRICE,
                            specifications: JSON.parse(record.SPEC_FOR_OFFLINE),
                            addons: JSON.parse(record.ADDONS),
                        };
                        //SERVER.http + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain +

                        ret.data.products.list.push(product_info);





                    }
                    defer.resolve(ret);
                    //console.log('return la');
                },
                function (err) {
                    var ret = {
                        status: 'E',
                        msg: 'Connection time out',
                    };
                    defer.reject(ret);
                });

            return defer.promise;

        };


        offline.getProductDetail = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var cat_param = "";

            offline.db.executeSql(
                'SELECT * from PRODUCT LEFT JOIN (SELECT ID, PRODUCT_ID FROM PRODUCT_MEDIA) as PM on PRODUCT.ID = PM.PRODUCT_ID where PRODUCT.ID = ? ', [params.product_id],
                function (res) {

                    var record = res.rows.item(0);
                    checkUndefined(record);
                    var ret = {
                        status: 'Y',
                        msg: '',
                        data: {
                            id: record.PRODUCT_ID,
                            code: record.CODE,
                            gift: record.GIFT,
                            //shop_name
                            category_ids: record.CATEGORY_IDS.substring(1, record.CATEGORY_IDS.length - 1).split(","),
                            name: record['NAME_' + params.locale],
                            name_en_us: record.NAME_EN_US,
                            name_zh_hk: record.NAME_ZH_HK,
                            name_zh_cn: record.NAME_ZH_CN,

                            highlight: record['HIGHLIGHT_' + params.locale],
                            highlight_en_us: record.HIGHLIGHT_EN_US,
                            highlight_zh_hk: record.HIGHLIGHT_ZH_HK,
                            highlight_zh_cn: record.HIGHLIGHT_ZH_CN,

                            content: record['CONTENT_' + params.locale],
                            content_en_us: record.CONTENT_EN_US,
                            content_zh_hk: record.CONTENT_ZH_HK,
                            content_zh_cn: record.CONTENT_ZH_CN,
                            photos: [],
                            currency: record.CURRENCY,
                            price: record.OFF_PRICE,
                            //specifications : offline.getProductSpec(record.ID),
                            //original_price : offline.getProductPrice(record.ID, record.CURRENCY),
                            qty: record.QTY,
                            sorting: record.SORTING,
                            enabled: record.ENABLED,
                            enable_for_pos: record.ENABLE_FOR_POS,
                            show_price: record.SHOP_PRICE,
                            remark: record.REMARK,
                            specifications: JSON.parse(record.SPEC_FOR_OFFLINE),
                            addons: JSON.parse(record.ADDONS),
                            department_id: record.DEPARTMENT_ID,

                        }
                    };
                    for (var i = 0; i < res.rows.length; i++) {

                        var record = res.rows.item(i);
                        var photo_url = $helper.getRootPath() + secondPath + record.ID;

                        ret.data.photos.push(photo_url);

                    }

                    defer.resolve(ret);
                    //console.log('return la');
                },
                function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        };

        offline.getSKUInfo = function (params) {

            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            var cat_param = "";

            var sku = '';
            if (typeof params.spec != "undefined") {

                //            var spec_array = JSON.parse(params.spec);
                //             for (var i=0;i<spec_array.length;i++)
                //             {
                //                 sku = sku + 'X' + spec_array[i].dictionary.toString(16);
                //                 sku = sku + 'Y' + spec_array[i].option.toString(16);
                //                 sku = sku + '-';
                //             }

                sku = encodePreSku(params.spec);

            }
            sku = sku + 'P' + params.product_id.toString(16);
            if (typeof params.sku_no != "undefined") {
                sku = params.sku_no;
            }
            //console.log(sku);

            offline.db.executeSql(
                'SELECT * from ((select * from PRODUCT_SKU_INFO where SKU_NO = ?) as F_SKU left join (select ID, NAME_EN_US, NAME_ZH_HK, NAME_ZH_CN from PRODUCT) as SHORT_P on F_SKU.PRODUCT_ID = SHORT_P.ID) as PSI left join (select * from PRODUCT_STOCK where PRODUCT_STOCK.WAREHOUSE_ID = ?) as F_PS on F_PS.SKU_NO = PSI.SKU_NO', [sku, params.warehouse_id],
                function (res) {
                    var record = res.rows.item(0);
                    checkUndefined(record);

                    var ret = {
                        status: 'Y',
                        msg: '',
                        product_id: record.PRODUCT_ID,
                        product_name: record['NAME_' + params.locale],
                        remarks: '------',
                        reserved_amount: 0,
                        local_qty: emptyTozero(record.QTY),
                        local_pending_in: emptyTozero(record.PENDING_IN),
                        local_pending_out: emptyTozero(record.PENDING_OUT),
                        local_warehouse_id: emptyTozero(record.WAREHOUSE_ID),
                        local_warehouse_name: 'Offline',
                        local_warehouse_location: '',
                        global_qty: emptyTozero(record.QTY),
                        price: record.PRICE,
                        original_price: record.ORIGINAL_PRICE,
                        data: []
                    };
                    //console.log(ret);
                    defer.resolve(ret);
                    //console.log('return la');
                },
                function (err) {
                    //console.log(err);
                    //console.log('error la');
                    defer.reject(err);
                });

            return defer.promise;

        };

        //get invoice ready to upload //0601.2 phi
        offline.getCanUploadInvoice = function (params) {
            console.log('into get upload');
            var defer = $q.defer();
            if (offline.db == null) offline.openDb();
            offline.db.executeSql(
                'SELECT * from INVOICE left join (select INVOICE_ID, count(*) as ITEM_COUNT , sum(UNIT_PRICE*QTY) as ITEM_TOTAL  from INVOICE_ITEM group by INVOICE_ID) as IITEM on IITEM.INVOICE_ID = INVOICE.ID where INVOICE.STATUS = ? and INVOICE.CARRY_UP = ?', [5, 0],
                function (res) {
                    console.log('success sql in upload');
                    var up_ret = {
                        list: [],
                        status: 'Y',
                    };
                    var list_c = 0;
                    var ids = [];
                    var idc = 0;
                    var iic = 0;
                    var icc = 0;
                    for (var longloop = 0; longloop < res.rows.length; longloop++) {
                        console.log(longloop);
                        var record = res.rows.item(longloop);
                        checkUndefined(record);
                        ids[idc++] = record.ID;
                        var ret = {
                            status: 'Y',
                            msg: '',
                            data: {
                                id: record.ID,
                                table_num: record.FB_TABLE,
                                invoice_no: record.INVOICE_NO,
                                currency: record.CURRENCY,
                                gender: record.GENDER,
                                billing_area_id: emptyTozero(record.BILLING_AREA_ID),
                                billing_region_id: emptyTozero(record.BILLING_REGION_ID),
                                billing_district_id: emptyTozero(record.BILLING_DISTRICT_ID),
                                billing_country_id: emptyTozero(record.BILLING_COUNTRY_ID),
                                billing_address_1: record.BILLING_ADDRESS_1,
                                billing_address_2: record.BILLING_ADDRESS_2,
                                billing_address_3: record.BILLING_ADDRESS_3,
                                billing_address_4: record.BILLING_ADDRESS_4,
                                billing_first_name: record.BILLING_FIRST_NAME,
                                billing_last_name: record.BILLING_LAST_NAME,
                                billing_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                billing_email: record.BILLING_EMAIL,
                                billing_mobile: record.BILLING_COUNTRY_CODE + ' ' + record.BILLING_MOBILE,

                                pick_up_contact: record.BILLING_FIRST_NAME + ' ' + record.BILLING_LAST_NAME,
                                pick_up_first_name: record.BILLING_FIRST_NAME,
                                pick_up_last_name: record.BILLING_LAST_NAME,
                                pick_up_country_code: record.BILLING_COUNTRY_CODE,
                                pick_up_mobile: record.BILLING_MOBILE,
                                pick_up_email: record.BILLING_EMAIL,

                                shipping_area_id: emptyTozero(record.SHIPPING_AREA_ID),
                                shipping_region_id: emptyTozero(record.SHIPPING_REGION_ID),
                                shipping_district_id: emptyTozero(record.SHIPPING_DISTRICT_ID),
                                shipping_country_id: emptyTozero(record.SHIPPING_COUNTRY_ID),
                                shipping_address_1: record.BILLING_ADDRESS_1,
                                shipping_address_2: record.BILLING_ADDRESS_2,
                                shipping_address_3: record.BILLING_ADDRESS_3,
                                shipping_address_4: record.BILLING_ADDRESS_4,
                                shipping_first_name: record.SHIPPING_FIRST_NAME,
                                shipping_last_name: record.SHIPPING_LAST_NAME,
                                shipping_contact: record.SHIPPING_FIRST_NAME + ' ' + record.SHIPPING_LAST_NAME,
                                shipping_email: record.SHIPPING_EMAIL,
                                shipping_mobile: record.SHIPPING_COUNTRY_CODE + ' ' + record.SHIPPING_MOBILE,

                                currency: record.CURRENCY,
                                item_count: record.ITEM_COUNT,
                                item_total: emptyTozero(record.ITEM_TOTAL),
                                pay_method: record.PAY_METHOD,
                                status_num: emptyTozero(record.STATUS),

                                deposit: emptyTozero(record.PAYED_AMOUNT),
                                payment_type: record.PAYMENT_TYPE,
                                payed_amount: emptyTozero(record.PAYED_AMOUNT),

                                delivery_type: record.DELIVERY_TYPE,
                                remark: record.REMARK,
                                products: [],

                                invoice_date: record.INVOICE_DATE,
                                check_out_warehouse: emptyTozero(record.CHECK_OUT_WAREHOUSE_ID),
                                pick_up_warehouse_id: emptyTozero(record.PICK_UP_WAREHOUSE_ID),
                                pick_up_site: record.PICK_UP_SITE,
                                expected_delivery_date: record.EXPECTED_DELIVERY_DATE,
                                invoice_charges: [],
                                department_list: [],

                                delivery_total: emptyTozero(record.DELIVERY_TOTAL),
                                refund_total: emptyTozero(record.REFUND_TOTAL),
                                refund: emptyTozero(record.REFUND_TOTAL),
                                grand_total: emptyTozero(record.ITEM_TOTAL),

                                customer_name: 'Visitor',
                                customer_info: {
                                    account_no: 'visitor',
                                    user_id: 0,
                                    customer_name: '',
                                    customer_first_name: '',
                                    customer_last_name: '',
                                    customer_email: '',
                                    customer_country_code: '',
                                    customer_mobile: '',
                                    vip_level: ''
                                }
                            }
                        };
                        up_ret.list.push(ret.data);

                        offline.db.executeSql(
                            'SELECT *,INVOICE_ITEM.ID as ITEM_ID,INVOICE_ITEM.ADDONS as ITEM_ADDONS from INVOICE_ITEM left join (select PRODUCT.ID, PRODUCT.NAME_EN_US, PRODUCT.NAME_ZH_HK, PRODUCT.NAME_ZH_CN, PRODUCT.HIGHLIGHT_EN_US,PRODUCT.HIGHLIGHT_ZH_HK, PRODUCT.HIGHLIGHT_ZH_CN, PRODUCT.CODE, PRODUCT.WEIGHT, PRODUCT.VOLUME, PRODUCT.REMARK, PRODUCT.SPEC_FOR_OFFLINE, PRODUCT.DEPARTMENT_ID, PM.ID as PHOTO_ID from PRODUCT LEFT JOIN (SELECT MIN(ID) as ID,PRODUCT_ID FROM PRODUCT_MEDIA group by PRODUCT_ID) as PM on PRODUCT.ID = PM.PRODUCT_ID) as FP on INVOICE_ITEM.PRODUCT_ID = FP.ID where INVOICE_ID=?', [ids[iic++]],
                            function (inv_item) {



                                for (var i = 0; i < inv_item.rows.length; i++) {

                                    //ret.data.item_count = inv_item.rows.length;
                                    //                     var product;
                                    //                     var invoice_item = inv_item.rows.item(0);
                                    //                     offline.db.executeSql(
                                    //                     'SELECT * from PRODUCT where ID=?', [inv_item.rows.item(i).PRODUCT_ID], function(inv_item_pro) {
                                    //
                                    //                         product = inv_item_pro.rows.item(0);
                                    //
                                    //
                                    //                     }, function(err) {
                                    //                       defer.reject(err);
                                    //                     });
                                    var invoice_item = inv_item.rows.item(i);
                                    checkUndefined(invoice_item);
                                    var photo_url = $helper.getRootPath() + secondPath + invoice_item.PHOTO_ID;

                                    var tid = ids.indexOf(invoice_item.INVOICE_ID);


                                    var product_info = {
                                        id: invoice_item.PRODUCT_ID,
                                        item_id: invoice_item.ITEM_ID,
                                        name: invoice_item['NAME_' + params.locale],
                                        highlight: invoice_item['HIGHLIGHT_' + params.locale],
                                        photo: photo_url,
                                        qty: emptyTozero(invoice_item.QTY),
                                        buy_qty: emptyTozero(invoice_item.QTY),
                                        sku_no: invoice_item.SKU_NO,
                                        code: invoice_item.CODE,
                                        options: [],
                                        currency: invoice_item.CURRENCY,
                                        o_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        actual_unit_price: emptyTozero(invoice_item.UNIT_PRICE),
                                        sub_total: emptyTozero(invoice_item.UNIT_PRICE * invoice_item.QTY),
                                        //buy_sub_total
                                        //actual sub total
                                        weight: invoice_item.WEIGHT,
                                        volumn: invoice_item.VOLUMN,
                                        remark: invoice_item.REMARK,
                                        served: invoice_item.SERVED,
                                        specifications: JSON.parse(invoice_item.SPEC_FOR_OFFLINE),
                                        department_id: invoice_item.DEPARTMENT_ID,
                                        addons: invoice_item.ITEM_ADDONS,
                                        department_name: '',
                                        department_name_EN_US: '',
                                        department_name_ZH_HK: '',
                                        department_name_ZH_CN: '',

                                    };
                                    up_ret.list[tid].item_count = 0;
                                    up_ret.list[tid].products.push(product_info);

                                }

                                offline.db.executeSql(
                                    'SELECT * from INVOICE_CHARGES where INVOICE_ID=?', [ids[icc++]],
                                    function (inv_charge) {

                                        var tid_o = 0;

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);

                                            var charge_info = {
                                                title_EN_US: invoice_charge.NAME_EN_US,
                                                title_ZH_HK: invoice_charge.NAME_ZH_HK,
                                                title_ZH_CN: invoice_charge.NAME_ZH_CN,
                                                sign: invoice_charge.SIGN,
                                                type: invoice_charge.TYPE,
                                                value_type: invoice_charge.VALUE_TYPE,
                                                value: invoice_charge.VALUE,

                                            };

                                            var tid = ids.indexOf(invoice_charge.INVOICE_ID);
                                            tid_o = tid;
                                            up_ret.list[tid].invoice_charges.push(charge_info);

                                        }

                                        var baseDiscount = [];
                                        var baseCharge = [];
                                        var baseTotal = [];

                                        baseDiscount[tid_o] = 0;
                                        baseCharge[tid_o] = 0;
                                        baseTotal[tid_o] = up_ret.list[tid_o].item_total;

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);
                                            if (invoice_charge.VALUE_TYPE == 'percent') {
                                                if (invoice_charge.SIGN == '+') {
                                                    var totalFlag = baseTotal[tid_o];
                                                    baseTotal[tid_o] *= (1 + Number(invoice_charge.VALUE) / 100);
                                                    baseCharge[tid_o] += baseTotal[tid_o] - totalFlag;
                                                } else {
                                                    var totalFlag = baseTotal[tid_o];
                                                    baseTotal[tid_o] *= (1 - Number(invoice_charge.VALUE) / 100);
                                                    baseDiscount[tid_o] += totalFlag - baseTotal[tid_o];
                                                }
                                            }

                                        }

                                        for (var i = 0; i < inv_charge.rows.length; i++) {
                                            var invoice_charge = inv_charge.rows.item(i);
                                            checkUndefined(invoice_charge);
                                            if (invoice_charge.VALUE_TYPE != 'percent') {
                                                if (invoice_charge.SIGN == '+') {
                                                    var totalFlag = baseTotal[tid_o];
                                                    baseTotal[tid_o] += Number(invoice_charge.VALUE);
                                                    baseCharge[tid_o] += baseTotal[tid_o] - totalFlag;
                                                } else {
                                                    var totalFlag = baseTotal[tid_o];
                                                    baseTotal[tid_o] -= Number(invoice_charge.VALUE);
                                                    baseDiscount[tid_o] += totalFlag - baseTotal[tid_o];
                                                }
                                            }

                                        }

                                        up_ret.list[tid_o].grand_total = baseTotal[tid_o];
                                        up_ret.list[tid_o].discount_total = 0;
                                        up_ret.list[tid_o].service_total = 0;

                                        list_c++;
                                        if (list_c == res.rows.length) {

                                            offline.db.executeSql('UPDATE "INVOICE" set "CARRY_UP" = 1 where INVOICE.STATUS = ? and INVOICE.CARRY_UP = ?', [5, 0], function (update_res) {

                                                defer.resolve(up_ret);

                                            }, function (err) {

                                                console.log(err);
                                                defer.reject(err);
                                            });


                                        }
                                    },
                                    function (err) {
                                        defer.reject(err);
                                    });

                                //defer.resolve(ret);
                            },
                            function (err) {
                                defer.reject(err);
                            });
                    }
                    if (res.rows.length == 0) {
                        defer.resolve(up_ret);
                    }
                },
                function (err) {
                    defer.reject(err);
                });

            return defer.promise;

        };


        /**************************************************
        // offline/invoice-pdf
        **************************************************/
        offline.getInvoiceTemplateCss = function (type) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + type + '.css.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateBody = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.' + params.type + '.body.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateFoodBody = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.' + params.type + '.body-food.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateFood2Body = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.' + params.type + '.body-food2.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateDrinkBody = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.' + params.type + '.body-drink.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateProductItem = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(params.type.startsWith("order") ? cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.product-item.order.html' : cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.product-item.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };


        offline.getInvoiceTemplateChargeItem = function (type) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + type + '.charge-item.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplateOptionItem = function (params) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + params.size + '.invoice-option.order.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.getInvoiceTemplate = function (type) {

            var defer = $q.defer();

            window.resolveLocalFileSystemURL(cordova.file.applicationDirectory + 'www/templates/tpl.invoice-pdf.' + type + '.html', function (fileEntry) {
                fileEntry.file(function (file) {
                    var reader = new FileReader();
                    reader.onloadend = function (e) {
                        defer.resolve(this.result);
                    }
                    reader.readAsText(file);
                });
            }, function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };

        offline.invoicePdf = function (params) {

            var defer = $q.defer();
            console.log(params);
            console.log('1');
            offline.getInvoiceTemplateCss(params.size).then(function (css) {
                offline.getInvoiceTemplateProductItem(params).then(function (productItem) {
                    offline.getInvoiceTemplateOptionItem(params).then(function (optionItem) {
                        offline.getInvoiceTemplateChargeItem(params.size).then(function (chargeItem) {
                            offline.getInvoiceTemplate(params.size).then(function (template) {
                                if (params.type.startsWith("order") && params.data.DEPARTMENT_LIST.length > 0) {
                                    var pages = [];
                                    var cur = 0;
                                    var max_length = params.data.DEPARTMENT_LIST.length;

                                    var asyncLoop = function (o) {
                                        var i = -1;

                                        var loop = function () {
                                            i++;
                                            if (i == o.length) { o.callback(); return; }
                                            o.functionToLoop(loop, i);
                                        }
                                        loop();
                                    }

                                    asyncLoop({
                                        length: max_length,
                                        functionToLoop: function (loop, i) {

                                            offline.getInvoiceTemplateBody(params).then(function (body_for_food) {
                                                console.log('1');
                                                var imagesReg = /{{ '(\w+)' \| IMAGE }}/g;
                                                var translateReg = /{{ '(\w+)' \| TRANSLATE }}/g;
                                                var dataReg = /{{ '(\w+)' \| DATA }}/g;
                                                var productItemReg = /{{ '(\w+)' \| PRODUCT_ITEM }}/g;
                                                var optionItemReg = /{{ '(\w+)' \| INVOICE_OPTION }}/g;
                                                var chargeItemReg = /{{ '(\w+)' \| CHARGE_ITEM }}/g;

                                                var page2 = body_for_food.replace(imagesReg, function (match, param, offset, string) {
                                                    return params.images[param];
                                                });

                                                var page2 = page2.replace(translateReg, function (match, param, offset, string) {
                                                    return $translate.instant(param);
                                                });

                                                var page2 = page2.replace(translateReg, function (match, param, offset, string) {
                                                    return $translate.instant(param);
                                                });

                                                var page2 = page2.replace(dataReg, function (match, param, offset, string) {
                                                    return params.data[param];
                                                });

                                                var products = '';
                                                console.log('3');
                                                console.log(params.data.DEPARTMENT_LIST[cur]);
                                                for (var i = 0; i < params.data.DEPARTMENT_LIST[cur].products.length; i++) {
                                                    products += productItem.replace(productItemReg, function (match, param, offset, string) {
                                                        return params.data.DEPARTMENT_LIST[cur].products[i][param];
                                                    });
                                                    console.log('product:');
                                                    console.log(products);
                                                }

                                                var options = '';
                                                for (var i = 0; i < params.data.DEPARTMENT_LIST[cur].products.length; i++) {
                                                    options = optionItem.replace(optionItemReg, function (match, param, offset, string) {
                                                        return params.data.DEPARTMENT_LIST[cur].products[i][param];
                                                    });
                                                }

                                                var charges = '';
                                                console.log('4');
                                                console.log(params.data.CHARGE_ITEM);
                                                for (var i = 0; i < params.data.CHARGE_ITEM.length; i++) {
                                                    charges += chargeItem.replace(chargeItemReg, function (match, param, offset, string) {
                                                        return params.data.CHARGE_ITEM[i][param];
                                                    });
                                                    console.log('product:');
                                                    console.log(charges);
                                                }

                                                var page2 = page2.replace(/{{ 'PRODUCT_ITEM' \| TEMPLATE }}/g, products);
                                                var page2 = page2.replace(/{{ 'CHARGE_ITEM' \| TEMPLATE }}/g, charges);
                                                var page2 = page2.replace(/{{ 'INVOICE_OPTION' \| TEMPLATE }}/g, options);

                                                var page2 = page2.replace(/{{ 'CURRENT_PAGE' \| PAGE }}/g, '1');
                                                var page2 = page2.replace(/{{ 'TOTAL_PAGE' \| PAGE }}/g, '2');

                                                pages.push(page2);
                                                cur++;

                                                if (cur == max_length)
                                                    defer.resolve({
                                                        css: css,
                                                        bodys: pages
                                                    });
                                                loop();
                                            }).catch(function (err) {
                                                defer.reject(err);
                                            });
                                        },
                                        callback: function () {

                                        }
                                    });
                                } else {
                                    offline.getInvoiceTemplateBody(params).then(function (body) {
                                        var pdf = template.replace(/{{ 'CSS' \| TEMPLATE }}/g, css);
                                        console.log('2');
                                        var imagesReg = /{{ '(\w+)' \| IMAGE }}/g;
                                        var translateReg = /{{ '(\w+)' \| TRANSLATE }}/g;
                                        var dataReg = /{{ '(\w+)' \| DATA }}/g;
                                        var productItemReg = /{{ '(\w+)' \| PRODUCT_ITEM }}/g;
                                        var chargeItemReg = /{{ '(\w+)' \| CHARGE_ITEM }}/g;

                                        var page1 = body.replace(imagesReg, function (match, param, offset, string) {
                                            return params.images[param];
                                        });

                                        var page1 = page1.replace(translateReg, function (match, param, offset, string) {
                                            return $translate.instant(param);
                                        });

                                        var page1 = page1.replace(translateReg, function (match, param, offset, string) {
                                            return $translate.instant(param);
                                        });

                                        var page1 = page1.replace(dataReg, function (match, param, offset, string) {
                                            return params.data[param];
                                        });

                                        // 2017-6-10 jaydon: show or hide octopus
                                        if (params.judge == null) params.judge = { OCTOPUS_DISPLAY: 'none' };
                                        console.log(params.judge.OCTOPUS_DISPLAY);
                                        var page1 = page1.replace(/isOctoput/g, params.judge.OCTOPUS_DISPLAY);

                                        var products = '';
                                        console.log('3');
                                        console.log(params.data.PRODUCT_ITEM);
                                        for (var i = 0; i < params.data.PRODUCT_ITEM.length; i++) {
                                            products += productItem.replace(productItemReg, function (match, param, offset, string) {
                                                return params.data.PRODUCT_ITEM[i][param];
                                            });
                                            console.log('product:');
                                            console.log(products);
                                        }

                                        var charges = '';
                                        console.log('4');
                                        console.log(params.data.CHARGE_ITEM);
                                        for (var i = 0; i < params.data.CHARGE_ITEM.length; i++) {
                                            charges += chargeItem.replace(chargeItemReg, function (match, param, offset, string) {
                                                return params.data.CHARGE_ITEM[i][param];
                                            });
                                            console.log('product:');
                                            console.log(charges);
                                        }

                                        var page1 = page1.replace(/{{ 'PRODUCT_ITEM' \| TEMPLATE }}/g, products);
                                        var page1 = page1.replace(/{{ 'CHARGE_ITEM' \| TEMPLATE }}/g, charges);

                                        var page1 = page1.replace(/{{ 'CURRENT_PAGE' \| PAGE }}/g, '1');
                                        var page1 = page1.replace(/{{ 'TOTAL_PAGE' \| PAGE }}/g, '2');

                                        pdf = pdf.replace(/{{ 'BODY' \| TEMPLATE }}/g, page1);
                                        defer.resolve({
                                            css: css,
                                            body: page1,
                                            pdf: pdf
                                        });
                                    }).catch(function (err) {
                                        defer.reject(err);
                                    });
                                }

                            }).catch(function (err) {
                                defer.reject(err);
                            });

                        }).catch(function (err) {
                            defer.reject(err);
                        });

                    }).catch(function (err) {
                        defer.reject(err);
                    });
                }).catch(function (err) {
                    defer.reject(err);
                });
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;

        };
        /**************************************************
        // start socket server for data synchronization
        **************************************************/
        offline.startServer = function (port) {

            if ($rootScope.localServer == undefined) {
                if ($rootScope.localClient != undefined) {
                    offline.disconnectFromServer();
                }

                $rootScope.localServer = {
                    socket: cordova.plugins.wsserver,
                    port: port,
                    clients: []
                };

                $rootScope.localServer.socket.start(port, {
                    // WebSocket Server handlers
                    'onFailure': function (addr, port, reason) {
                        console.log('Stopped listening on %s:%d. Reason: %s', addr, port, reason);
                        delete $rootScope.localServer;
                        // TODO: make more presentable alert message
                        alert('Local server stopped. Reason: ' + reason);
                    },
                    // WebSocket Connection handlers
                    'onOpen': function (conn) {
                        /* conn: {
                        'uuid' : '8e176b14-a1af-70a7-3e3d-8b341977a16e',
                        'remoteAddr' : '192.168.1.10',
                        'acceptedProtocol' : 'my-protocol-v1',
                        'httpFields' : {...},
                        'resource' : '/?param1=value1&param2=value2'
                        } */
                        console.log('A user connected from %s', conn.remoteAddr);
                        $rootScope.localServer.clients.push(conn);
                        // TODO: translation with remote address
                        $helper.toast($translate.instant('CLIENT_CONNECTED'), 'long', 'bottom');
                    },
                    'onMessage': function (conn, msg) {
                        console.log(conn, msg);
                        alert(msg);
                        //                 $rootScope.stopSyncCommand = true;
                        //                 var command = angular.fromJson(msg);
                        //                 console.log(command);
                        //                 // TODO: trigger method with params
                        //                 $eval(
                        //                   command.method + '(' + command.params + ').then(function(res) { \
                        //                     $rootScope.stopSyncCommand = false; \
                        //                   }).catch(function(err) { \
                        //                     $rootScope.stopSyncCommand = false; \
                        //                   }); \
                        //                 ');
                        // TODO: broadcast to other clients
                        for (var i = 0; i < $rootScope.localServer.clients.length; i++) {
                            if ($rootScope.localServer.clients[i].uuid != conn.uuid) {
                                $rootScope.localServer.socket.send($rootScope.localServer.clients[i], msg);
                            }
                        }
                    },
                    'onClose': function (conn, code, reason) {
                        console.log('A user disconnected from %s', conn.remoteAddr);
                        var clients = [];
                        for (var i = 0; i < $rootScope.localServer.clients.length; i++) {
                            if ($rootScope.localServer.clients[i].uuid != conn.uuid) {
                                clients.push($rootScope.localServer.clients[i]);
                            }
                        }
                        $rootScope.localServer.clients = clients;
                    },
                    // Other options
                    //         'origins': [ 'file://' ], // validates the 'Origin' HTTP Header.
                    //         'protocols': [ 'ws', 'http' ], // validates the 'Sec-WebSocket-Protocol' HTTP Header.
                    'tcpNoDelay': true // disables Nagle's algorithm.
                }, function onStart(addr, port) {
                    console.log('Listening on %s:%d', addr, port);
                    offline.showServerQrCode();
                }, function onDidNotStart(reason) {
                    console.log('Did not start. Reason: %s', reason);
                    delete $rootScope.localServer;
                    // TODO: make more presentable alert message
                    alert('Could not start local server. Reason: ' + reason);
                });
            }


        };

        /**************************************************
        // show server QR code for client connection
        **************************************************/
        offline.showServerQrCode = function () {

            networkinterface.getWiFiIPAddress(function (ip) {
                var data = {
                    ipAddr: ip,
                    port: $rootScope.localServer.port
                };
                // TODO: show server QR code in modal for client connect
            });

        };

        /**************************************************
        // stop socket server for some reason
        **************************************************/
        offline.stopServer = function () {

            if ($rootScope.localServer != undefined) {
                $rootScope.localServer.socket.stop(function onStop(addr, port) {
                    console.log('Stopped listening on %s:%d', addr, port);
                    delete $rootScope.localServer;
                });
            }

        };

        /**************************************************
        // scan server QR code to connect to socket server
        **************************************************/
        offline.scanServerQrCode = function () {
            // TODO: scan QR code to retrieve server IP and port
            // TODO: connect to server
        };

        /**************************************************
        // connect to socket server for data synchronization
        **************************************************/
        offline.connectToServer = function (ipAddr, port) {

            if ($rootScope.localClient == undefined) {
                if ($rootScope.localServer != undefined) {
                    offline.stopServer();
                }

                $rootScope.localClient = {
                    socket: $websocket('ws://' + ipAddr + ':' + port.toString()),
                    ipAddr: ipAddr,
                    port: port
                };

                $rootScope.localClient.socket.onOpen(function () {
                    console.log('Connected to %s:%d', $rootScope.localClient.ipAddr, $rootScope.localClient.port);
                });

                $rootScope.localClient.socket.onMessage(function (msg) {
                    alert(msg.data);
                    //               $rootScope.stopSyncCommand = true;
                    //               var command = angular.fromJson(msg.data);
                    //               console.log(command);
                    //               // TODO: trigger method with params
                    //               $eval(
                    //                 command.method + '(' + command.params + ').then(function(res) { \
                    //                   $rootScope.stopSyncCommand = false; \
                    //                 }).catch(function(err) { \
                    //                   $rootScope.stopSyncCommand = false; \
                    //                 }); \
                    //               ');
                });

                $rootScope.localClient.socket.onError(function (e) {
                    console.log('Client socket error', e);
                });

                $rootScope.localClient.socket.onClose(function (msg) {
                    delete $rootScope.localClient;
                });
            }

        };

        /**************************************************
        // disconnect from server for some reason
        **************************************************/
        offline.disconnectFromServer = function () {

            if ($rootScope.localClient != undefined) {
                $rootScope.localClient.socket.close(false);
            }

        };

        /**************************************************
        // synchronize command depends on server / client
        **************************************************/
        offline.syncCommand = function (command) {

            if ($rootScope.localServer != undefined) {
                for (var i = 0; i < $rootScope.localServer.clients.length; i++) {
                    $rootScope.localServer.socket.send($rootScope.localServer.clients[i], command);
                }
            }
            if ($rootScope.localClient != undefined) {
                $rootScope.socket.send(command);
            }

        };


        /**************************************************
        // Return service
        **************************************************/
        return offline;

    });
