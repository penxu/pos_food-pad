angular.module('controllers.splash', [
    'ionic',
    'pascalprecht.translate',
    'services.localstorage',
    'services.helper',
    'services.api',
    'directives.common',
    'directives.ubeing',
    'services.offline',
    'services.printer',
    'ion-datetime-picker'
])

    .controller('splashCtrl', function ($scope, $rootScope, $localStorage, $translate, $helper, $ionicLoading, $api, $ionicHistory, $ionicModal, $filter, $ionicPopup, $offline, $timeout, $window, $ionicPopover, SERVER, $sce, ionicDatePicker, $cordovaDatePicker, $printer) {

        /**************************************************
        // initialize view
        **************************************************/
        $rootScope.choosePayment = function (paymentInfo) {
            $rootScope.currentPayment = paymentInfo;
            $rootScope.popOverOperation.popoverBack();
        };

        $rootScope.chooseOrderType = function (orderType) {
            $rootScope.currentOrderType = orderType;
            $rootScope.popOverOperation.popoverBack();
        };


        $rootScope.generatePaymentList = function(paymentList){

            // if(!paymentList || paymentList.length<1){
            //     $rootScope.choosePaymentList = [{
            //          id: 1, name: 'cash', code: $translate.instant('CASH'), translateKey: 'CASH', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false 
            //     }];
            //     return;
            // }
            // $rootScope.choosePaymentList = [
            //     { id: 1, name: 'cash', code: $translate.instant('CASH'), translateKey: 'CASH', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 2, name: 'member coin', code: $translate.instant('COIN'), translateKey: 'COIN', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 8, name: 'Octopussmalllogo', code: $translate.instant('OPMLG'), translateKey: 'OPMLG', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 3, name: 'aliPay', code: $translate.instant('ALIPAY'), translateKey: 'ALIPAY', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 4, name: 'amex', code: $translate.instant('AMEX'), translateKey: 'AMEX', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 5, name: 'android', code: $translate.instant('ANDROID'), translateKey: 'ANDROID', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 6, name: 'apple', code: $translate.instant('APPLE'), translateKey: 'APPLE', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 7, name: 'mastercard', code: $translate.instant('MASTER'), translateKey: 'MASTER', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     //{ id: 8, name: 'Octopussmalllogo', code: $translate.instant('OPMLG'), translateKey: 'OPMLG', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 9, name: 'paypal', code: $translate.instant('PAYPAL'), translateKey: 'PAYPAL', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 10, name: 'visa', code: $translate.instant('VISA'), translateKey: 'VISA', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            //     { id: 11, name: 'wechat', code: $translate.instant('WECHAT'), translateKey: 'WECHAT', image: 'img/2.0icon/icon-pay/cash.png', price: 0, check: false, showEdit: false },
            // ];
            // var tempList = [];
            // for(var i=0;i<paymentList.length;i++){
            //     paymentList[i].title_en_us = $filter('lowercase')(paymentList[i].title_en_us);
            //     for(var j=0;j<$rootScope.choosePaymentList.length;j++){
            //         if(paymentList[i].title_en_us == $rootScope.choosePaymentList[j].name){
            //             tempList.push($rootScope.choosePaymentList[j]);
            //             break;
            //         }
            //     }
            // }
            // $rootScope.choosePaymentList = tempList;
        };

        // 2017-6-6 - TK: test code
        $rootScope.testScanner = function (type, e) {
            console.log('会员' + $rootScope.memberValue)
            // console.log(type, e);
            console.log(e, 11111);
            // if ($rootScope.canScan) {
            if (e.keyCode == 13 && $rootScope.memberValue != '') {
                //$rootScope.canScan = false;
                $rootScope.loadCustomerList('refresh', $rootScope.memberValue, true);
                // $timeout(function () {
                //     document.getElementById('setfocus').blur();
                //     document.getElementById('setfocus').value = '';
                // }, 0);
                console.log('load member');
            }
        };
        /* 2017 0616 parker: login change mode */
            $rootScope.initSandboxMode = function (sandbox) {
                $rootScope.sandboxCheckbox = true;
                var activate = $localStorage.get('activate');
                if (sandbox) {
                    activate.prefix = 'sandbox.';
                } else {
                    activate.prefix = '';
                }
                $localStorage.set('activate', activate);
                $api.auth().then(function (res) {
                    $helper.hideLoading();
                    if (res.status == 'Y') {
                        var settings = $localStorage.get('settings');
                        settings.token = res.token;
                        $rootScope.currentInvoiceId = null;
                        $localStorage.set('settings', settings);
                        $rootScope.hideMenu();
                        $helper.navForth('login', null, 'slide-left-right');
                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.hideLoading();
                    $helper.toast(err, 'long', 'bottom');
                });

                console.log($rootScope.sandboxCheckbox,'sandbox');
            };
            $rootScope.initProductionMode = function (sandbox) {
                $rootScope.sandboxCheckbox = false;
                if (sandbox) {
                    activate.prefix = 'sandbox.';
                } else {
                    activate.prefix = '';
                }
                $localStorage.set('activate', activate);
                $api.auth().then(function (res) {
                    $helper.hideLoading();
                    if (res.status == 'Y') {
                        var settings = $localStorage.get('settings');
                        settings.token = res.token;
                        $rootScope.currentInvoiceId = null;
                        $localStorage.set('settings', settings);
                        $rootScope.hideMenu();
                        $helper.navForth('login', null, 'slide-left-right');
                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.hideLoading();
                    $helper.toast(err, 'long', 'bottom');
                });
                console.log($rootScope.sandboxCheckbox,'sandbox');
            }

            /** end **/

        $scope.init = function () {

            // get app status
            if (ionic.Platform.isIOS()) {
                $rootScope.platform = 'ios';
            } else if (ionic.Platform.isAndroid()) {
                $rootScope.platform = 'android';
            } else if (ionic.Platform.isWindowsPhone()) {
                $rootScope.platform = 'window';
            } else {
                $rootScope.platform = 'web';
            }
            console.log($rootScope.platform);
            // table 初始化
            $rootScope.currentTable = { id: 0 };
            $rootScope.tableData = $localStorage.get('table_data');
            //searchbar init
            $rootScope.showSearchBar = false;
            // detect network info
            console.log('1');
            $helper.watchNetwork();
            if ($localStorage.get('offline') == undefined) {
                $localStorage.set('offline', 'online');
            }
            $rootScope.isOffline = $localStorage.get('offline') == 'offline' ? true : false;
            $rootScope.networkResult = !$rootScope.isOffline && $rootScope.networkStatus;
            // TODO: check network availibility
            $rootScope.online = true;

            $rootScope.can_open_history = true;
            // disable back to splash page
            $ionicHistory.nextViewOptions({ disableBack: true });
            console.log('2');
            // initialize app status from server
            if ($localStorage.get('settings') === null) {
                $localStorage.set('settings', {
                    token: null,
                    locale: 'EN_US',
                    print_locale: 'EN_US',
                    device_type: '',
                    device_token: '',
                    warehouse_lock: false,
                    warehouse_id: '',
                    cloud_lock: false,
                    cloud_address: '',
                    epson_ip_address: '192.168.1.23',
                    epson_port: '8008',
                    epson_device_id: 'local_printer',
                    printer_type: 'EPSON thermal printer'
                });
            }
            $translate.use($localStorage.get('settings').locale);

            // 2017-06-01-city : add decimal point setting and device prefix setting
            if ($localStorage.get('other_settings') === null) {
                $localStorage.set('other_settings', {
                    decimal_point: 1,
                    device_prefix: '',
                    is_by_department: true
                });
            }
            console.log('3');
            // initialize user
            if ($localStorage.get('user') === null) {
                $localStorage.set('user', {
                    id: null,
                    name: null,
                    shop_id: null,
                    level: null,
                    login: '',
                    password: '',
                    warehouse_id: '',
                    isLogin: false,
                    invoice_prefix: ''
                });
            }

            console.log('4');
            if ($localStorage.get('activate') === null) {
                $localStorage.set('activate', {
                    status: false,
                    path: 'www',
                    prefix: '',
                    passcode: '',
                    shop_icon: ''
                });
            }

            /**************************************************
            // switch sandbox
            **************************************************/
            if ($localStorage.get('activate').prefix == '') {
                $rootScope.sandboxCheckbox = false;
            } else {
                $rootScope.sandboxCheckbox = true;
            }
            console.log('check sandbox production :' + $rootScope.sandboxCheckbox);

            console.log('5');
            //get dial code
            if ($localStorage.get('dial') === null) {
                console.log('ready to get dial!!');
                $api.getDialCode({}).then(function (res) {
                    console.log(res);
                    if (res.status == 'Y') {
                        var dial_list = res.data;
                        $localStorage.set('dial', {
                            dial_list: dial_list,
                        });

                    } else { }
                }).catch(function (err) {
                    console.log(err);
                    $helper.toast(err, 'long', 'bottom');
                });
            }
            console.log('6');
            // display loading indicator
            $ionicLoading.show({
                template: '<ion-spinner icon="lines"></ion-spinner>',
                noBackdrop: true
            });
            console.log('7');
            var activate = $localStorage.get('activate');
            console.log($rootScope.networkResult);
            if ($rootScope.networkResult) {
                if (activate.status) {
                    // initial data & api token & auto login
                    console.log('before auth');
                    var api_auth = function () {
                        $api.auth().then(function (res) {
                            if (res.status == 'Y') {
                                console.log('into auth');
                                var settings = $localStorage.get('settings');
                                settings.token = res.token;
                                $localStorage.set('settings', settings);

                                // auto login if user saved password
                                var user = $localStorage.get('user');
                                if (user.id != null) {
                                    console.log('before login');
                                    $api.login({
                                        token: settings.token,
                                        locale: settings.locale,
                                        login: user.login,
                                        password: user.password,
                                        device_type: settings.device_type,
                                        device_token: settings.device_token
                                    }).then(function (res) {
                                        if (res.status == 'Y') {
                                            // save returned user info
                                            console.log('login success');
                                            console.log(res);
                                            user.level = res.user_level;
                                            user.shop_id = res.shop_id;
                                            user.isLogin = true;
                                            user.name = res.name;
                                            user.invoice_prefix = res.invoice_prefix;
                                            $localStorage.set('user', user);
                                            console.log('set user' + $localStorage.get('user'));

                                            // save returned warehouse list
                                            var warehouse = [];
                                            console.log('generate warehouse');
                                            for (var i = 0; i < res.options.length; i++) {
                                                warehouse.push({
                                                    id: res.options[i].warehouse_id,
                                                    code: res.options[i].warehouse_code,
                                                    name: res.options[i].warehouse_name,
                                                    warehouse_address_EN_US: res.options[i].warehouse_address_EN_US,
                                                    warehouse_address_ZH_HK: res.options[i].warehouse_address_ZH_HK,
                                                    warehouse_address_ZH_CN: res.options[i].warehouse_address_ZH_CN,
                                                    warehouse_tel: res.options[i].warehouse_tel
                                                });
                                            }
                                            console.log('set warehouse');
                                            $localStorage.set('warehouse', warehouse);
                                            var inventory_warehouse = [];
                                            for (var i = 0; i < res.warehouses.length; i++) {
                                                inventory_warehouse.push({
                                                    id: res.warehouses[i].warehouse_id,
                                                    code: res.warehouses[i].warehouse_code,
                                                    name: res.warehouses[i].warehouse_name
                                                });
                                            }
                                            $localStorage.set('inventory_warehouse', inventory_warehouse);

                                            $rootScope.generatePaymentList(res.payment_method);

                                            $ionicLoading.hide();
                                            $rootScope.firstInit();
                                            $helper.judgeUpdate(res.last_update);
                                            $helper.navForth('home', null, 'slide-left-right');
                                        } else {
                                            // auto login failed, mark the user as un-logged
                                            user.isLogin = false;
                                            $localStorage.set('user', user);
                                            $ionicLoading.hide();
                                            $helper.navForth('login', null, 'slide-left-right');
                                        }
                                    }).catch(function (err) {
                                        user.isLogin = false;
                                        $localStorage.set('user', user);
                                        $ionicLoading.hide();
                                        $helper.navForth('login', null, 'slide-left-right');
                                        console.log('catch login err');
                                        $helper.toast(err, 'long', 'bottom');
                                    });
                                } else {
                                    if (user != null) {
                                        // auto login failed, mark the user as un-logged
                                        user.isLogin = false;
                                        $localStorage.set('user', user);
                                    }

                                    $ionicLoading.hide();
                                    $helper.navForth('login', null, 'slide-left-right');
                                }
                            } else {
                                $helper.toast(res.msg, 'short', 'bottom');
                                $ionicLoading.hide();
                                $ionicPopup.alert({
                                    template: $filter('translate')('NETWORK_UNSTEADY_RETRY')
                                }).then(function (res) {
                                    $ionicLoading.show({
                                        template: '<ion-spinner icon="lines"></ion-spinner>',
                                        noBackdrop: true
                                    });
                                    api_auth();
                                });
                            }
                        }).catch(function (err) {
                            $helper.toast(err, 'long', 'bottom');
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                template: $filter('translate')('NETWORK_UNSTEADY_RETRY')
                            }).then(function (res) {
                                $ionicLoading.show({
                                    template: '<ion-spinner icon="lines"></ion-spinner>',
                                    noBackdrop: true
                                });
                                api_auth();
                            });
                        });
                    };
                    api_auth();
                } else {
                    $ionicLoading.hide();
                    $helper.navForth('activate', null, 'slide-left-right');
                }
            } else {
                if (activate.status) {
                    var user = $localStorage.get('user');
                    console.log('0');
                    if (user.id != null) {
                        console.log('1');
                        $ionicLoading.hide();
                        $rootScope.offlineFirstInit();
                        $helper.navForth('home', null, 'slide-left-right');
                    } else {
                        console.log('2');
                        if (user != null) {
                            console.log('3');
                            user.isLogin = false;
                            $localStorage.set('user', user);
                        }
                        $ionicLoading.hide();
                        $helper.navForth('login', null, 'slide-left-right');
                        $helper.toast($filter('translate')('NETWORK_CONTINUE'));
                    }
                } else {
                    console.log('3');
                    $ionicLoading.hide();
                    $helper.navForth('activate', null, 'slide-left-right');
                    $helper.toast($filter('translate')('NETWORK_CONTINUE'));
                }
            }

            console.log($rootScope.networkResult);

            // 2017-5-22 - TK: added for speed up printing
            $printer.epsonInit();

            $rootScope.reportRootPath = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fmanage-day-end-report-fab&warehouse_id=' + $localStorage.get('user').warehouse_id + '&locale=' + $localStorage.get('settings').locale;

            var dateObj = new Date();
            month = '' + (dateObj.getMonth() + 1),
                day = '' + dateObj.getDate(),
                year = dateObj.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            console.log([year, month, day].join('-'));
            $rootScope.reportStartDate = [year, month, day].join('-');
            $rootScope.reportEndDate = [year, month, day].join('-');
        };

        /**************************************************
        // event handlers
        **************************************************/


        // load invoice detail
        $rootScope.popUpSetting = function () {          
            console.log('check sandboxCheckbox :' + $rootScope.sandboxCheckbox);
            if ($rootScope.tableData)
                $rootScope.tableNum = $rootScope.tableData.length;
            $rootScope.hideMenu();
            $rootScope.settingConfig = {
                title: 'SETTINGS',
                back: function () {
                    $printer.epsonInit();
                    $rootScope.settingModal.hide();
                    $rootScope.settingModal.remove();
                }
            };
            $rootScope.langs = SERVER.languages;
            $ionicModal.fromTemplateUrl('templates/modal.setting.html', {
                scope: $rootScope,
                animation: 'none'
            }).then(function (modal) {
                $rootScope.settingModal = modal;
                modal.show();
            });

        };



        /**************************************************
        // finally
        **************************************************/
        $rootScope.firstInit = function () {
            $rootScope.shop_icon = $localStorage.get('activate').shop_icon;
            $rootScope.user_name = $localStorage.get('user').name;
            var settings = $localStorage.get('settings');
            $rootScope.currentLang = settings.locale;
            $rootScope.currentPrintLang = settings.print_locale;
            $rootScope.printerType = settings.printer_type;
            $rootScope.epsonIpAddress = settings.epson_ip_address;
            $rootScope.epsonPort = settings.epson_port;
            $rootScope.epsonDeviceId = settings.epson_device_id;
            console.log($rootScope.epsonIpAddress);
            angular.forEach($localStorage.get('warehouse'), function (val) {
                if ($localStorage.get('user').warehouse_id == val.id) {
                    $rootScope.warehouse_name = val.name;
                    $rootScope.warehouse = val;
                }
            });

            // switch language
            $rootScope.switchLanguage = function (lang) {
                console.log('firstInit switchLanguage');
                var settings = $localStorage.get('settings');
                if (settings.locale == lang) return;
                settings.locale = lang;
                $localStorage.set('settings', settings);
                $translate.use(lang);
                $rootScope.currentLang = lang;
            };
            // switch print language
            $rootScope.switchPrintLanguage = function (lang) {
                var settings = $localStorage.get('settings');
                settings.print_locale = lang;
                $localStorage.set('settings', settings);
                $rootScope.currentPrintLang = lang;

            };

            $rootScope.warehouseCheckbox = $localStorage.get('settings').warehouse_lock;
            $rootScope.warehouseLock = function (value) {
                var settings = $localStorage.get('settings');
                settings.warehouse_lock = value;
                $localStorage.set('settings', settings);
                $rootScope.warehouseCheckbox = value;
                console.log($rootScope.warehouseCheckbox);
            };

            $rootScope.selectWarehouse = function (war_name) {
                if (!$localStorage.get('settings').warehouse_lock) {
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        if (war_name == val.name) {
                            var user = $localStorage.get('user');
                            user.warehouse_id = val.id;
                            $localStorage.set('user', user);
                            var setting = $localStorage.get('settings');
                            setting.warehouse_id = val.id;
                            $localStorage.set('settings', setting);
                            $api.settings({
                                token: $localStorage.get('settings').token,
                                locale: $localStorage.get('settings').locale,
                                warehouse_id: val.id,
                            }).then(function (res) {
                                if (res.status == 'Y') { } else {
                                    $helper.toast(res.msg, 'short', 'bottom');
                                }
                            }).catch(function (err) {
                                $helper.toast(err, 'long', 'bottom');
                            });

                        }
                    });
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        if ($localStorage.get('user').warehouse_id == val.id) {
                            $rootScope.warehouse_name = val.name;
                            $rootScope.warehouse = val;
                        }
                    });
                    $rootScope.showWarehouseModal.hide();
                    $rootScope.showWarehouseModal.remove();
                    $rootScope.currentInvoiceId = null;
                    $helper.navForth('home', null, 'slide-left-right');
                    $rootScope.initCart();
                    //移除 modal setting
                    $rootScope.settingConfig.back();
                }

            }

            $rootScope.showWarehouse = function () {
                if (!$localStorage.get('settings').warehouse_lock) {
                    $rootScope.warehouseList = [];
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        $rootScope.warehouseList.push(val.name);
                    });
                    $rootScope.warehouseConfig = {
                        title: 'SELECT_WAREHOUSE',
                        back: function () {
                            $rootScope.showWarehouseModal.hide();
                            $rootScope.showWarehouseModal.remove();
                        }
                    };
                    $ionicModal.fromTemplateUrl('templates/modal.select-warehouse.html', {
                        scope: $rootScope,
                        animation: 'slide-in-up'
                    }).then(function (modal) {
                        $rootScope.showWarehouseModal = modal;
                        $rootScope.showWarehouseModal.show();
                    });
                }

            }

            $rootScope.showPrinter = function () {

                $rootScope.printerList = [];
                $rootScope.printerList.push('Other');
                $rootScope.printerList.push('EPSON thermal printer');

                $rootScope.printerConfig = {
                    title: 'SELECT_PRINTER',
                    back: function () {
                        $rootScope.showWarehouseModal.hide();
                        $rootScope.showWarehouseModal.remove();
                    }
                };
                $ionicModal.fromTemplateUrl('templates/modal.select-printer.html', {
                    scope: $rootScope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $rootScope.showWarehouseModal = modal;
                    $rootScope.showWarehouseModal.show();
                });

            }

            $rootScope.changeEpsonIpAddr = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_ip_address = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonIpAddress = addr;
                console.log($rootScope.epsonIpAddress);
            };


            $rootScope.changeEpsonPort = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_port = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonPort = addr;
                console.log($rootScope.epsonPort);
            };


            $rootScope.changeEpsonDeviceId = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_device_id = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonDeviceId = addr;
                console.log($rootScope.epsonDeviceId);
            };

            $rootScope.selectPrinter = function (printer) {

                var settings = $localStorage.get('settings');
                settings.printer_type = printer;
                $localStorage.set('settings', settings);
                $rootScope.printerType = printer;
                console.log($rootScope.printerType);

                $rootScope.showWarehouseModal.hide();
                $rootScope.showWarehouseModal.remove();

            }
            
            //2017-06-02-city: set decimal point function
            $rootScope.decimalPoint = $localStorage.get('other_settings').decimal_point;

            $rootScope.setDecimalPoint = function (value, type) {
                if (type) {
                    if (type == 'add' && value < 2) {
                        value++;
                    } else if (type == 'minus' && value > 0) {
                        value--;
                    }
                }
                var settings = $localStorage.get('other_settings');
                settings.decimal_point = value;
                $localStorage.set('other_settings', settings);
                $rootScope.decimalPoint = value;
                console.log("decimalPoint :" + $rootScope.decimalPoint);
            };
            console.log($rootScope.decimalPoint);

            //2017-06-01-city: device prefix function
            $rootScope.devicePrefix = $localStorage.get('other_settings').device_prefix == ' ' ? '' : $localStorage.get('other_settings').device_prefix;

            $rootScope.changeDevicePrefix = function (prefix) {
                var settings = $localStorage.get('other_settings');
                settings.device_prefix = prefix;
                $localStorage.set('other_settings', settings);
                $rootScope.devicePrefix = prefix;
                console.log("devicePrefix :" + $rootScope.devicePrefix);
            };

            //cloud print
            $rootScope.cloudPrint = $localStorage.get('settings').cloud_lock;

            $rootScope.switchCloudPrint = function (value) {
                var settings = $localStorage.get('settings');
                settings.cloud_lock = value;
                $localStorage.set('settings', settings);
                $rootScope.cloudPrint = value;
                console.log($rootScope.cloudPrint);
            };

            //print by department
            $rootScope.isByDepartment = $localStorage.get('other_settings').is_by_department;

            $rootScope.switchOrderPrintOption = function (value) {
                var settings = $localStorage.get('other_settings');
                settings.is_by_department = value;
                $localStorage.set('other_settings', settings);
                $rootScope.isByDepartment = value;
                console.log($rootScope.isByDepartment);
            };
            //2017 0617 parker: print or not
            $rootScope.switchIsPrint = function (isPrint) {
                console.log(isPrint,'打印前');
                $rootScope.isPrint = isPrint;
                console.log($rootScope.isPrint,'打印');
            };
            //end 
            $rootScope.cloudPrintAddress = $localStorage.get('settings').cloud_address == ' ' ? '' : $localStorage.get('settings').cloud_address;

            $rootScope.changePrintAddr = function (addr) {
                var settings = $localStorage.get('settings');
                settings.cloud_address = addr;
                $localStorage.set('settings', settings);
                $rootScope.cloudPrintAddress = addr;
                console.log($rootScope.cloudPrintAddress);
            };

            $rootScope.logout = function () {
                console.log('log out');
                var user = $localStorage.get('user');
                user.id = null;
                user.isLogin = false;
                $localStorage.set('user', {});
                $helper.showLoading();
                $api.auth().then(function (res) {
                    $helper.hideLoading();
                    if (res.status == 'Y') {
                        var settings = $localStorage.get('settings');
                        settings.token = res.token;
                        // $rootScope.currentInvoiceId = null;
                        $rootScope.initCart();
                        $localStorage.set('settings', settings);
                        $rootScope.hideMenu();
                        $helper.navForth('login', null, 'slide-left-right');
                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.hideLoading();
                    $helper.toast(err, 'long', 'bottom');
                });
            };

            $rootScope.switchSandbox = function (sandbox) {
                // var content = sandbox?'CHANGE_SANDBOX':'CHANGE_PRODUCTION';
                // $helper.popConfirm($filter('translate')('REMIND'),$filter('translate')(content),function(res){
                //     if(res){
                if(sandbox && $rootScope.sandboxCheckbox || !sandbox && !$rootScope.sandboxCheckbox ) {
                    return;
                }else {
                    console.log('into meme~~');
                    var activate = $localStorage.get('activate');
                    if (sandbox) {
                        activate.prefix = 'sandbox.';
                    } else {
                        activate.prefix = '';
                    };
                    $localStorage.set('activate', activate);
                    $rootScope.sandboxCheckbox = sandbox;
                    $rootScope.logout();
                    $rootScope.settingModal.hide();
                    $rootScope.settingModal.remove();
                    $rootScope.initCart();
                    // $rootScope.newCart();

                    // }else{
                    //     $scope.sandboxCheckbox = !$scope.sandboxCheckbox;
                    // }
                    // });
                };
            };



            /**************************************************
            // switch offline
            **************************************************/


            /**
             * switch offline mode
             */
            $rootScope.switchOffline = function (isOnline) {
                if ((!$rootScope.isOffline && isOnline) || ($rootScope.isOffline && !isOnline)) return;
                var str = isOnline ? 'SWITCH_ONLINE' : 'SWITCH_OFFLINE';
                $ionicPopup.confirm({
                    title: $translate.instant('REMIND'),
                    template: $filter('translate')(str)
                }).then(function (res) {
                    if (res) {
                        var offlineStatus = $localStorage.get('offline');
                        $localStorage.set('offline', isOnline ? 'online' : 'offline');
                        $rootScope.isOffline = !isOnline;
                        $rootScope.networkResult = !$rootScope.isOffline && $rootScope.networkStatus;
                        $helper.judgeUpdate($rootScope.sandboxCheckbox ? $localStorage.get('sandbox_update_time') : $localStorage.get('production_update_time'));
                        console.log('network result : ' + $rootScope.networkResult);
                        $helper.navForth('home', null, 'slide-left-right');
                        // $rootScope.currentInvoiceId = null;
                        $rootScope.initCart();
                        //移除 modal setting
                        $rootScope.settingConfig.back();

                    }
                });
            };



        };

        $rootScope.uploadData = function () {
            $api.getCanUploadInvoice({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale
            }).then(function (res) {
                if (res.status == 'Y') {
                    $helper.toast($translate.instant('UPLOAD_SUCCESS'), 'long', 'bottom');
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
            });
        };

        $rootScope.offlineFirstInit = function () {
            $rootScope.shop_icon = $localStorage.get('activate').shop_icon;
            $rootScope.user_name = $localStorage.get('user').name;
            $rootScope.currentLang = $localStorage.get('settings').locale;
            $rootScope.currentPrintLang = $localStorage.get('settings').print_locale;
            $rootScope.printerType = $localStorage.get('settings').printer_type;
            $rootScope.epsonIpAddress = $localStorage.get('settings').epson_ip_address;
            $rootScope.epsonPort = $localStorage.get('settings').epson_port;
            $rootScope.epsonDeviceId = $localStorage.get('settings').epson_device_id;
            console.log($rootScope.epsonIpAddress);
            angular.forEach($localStorage.get('warehouse'), function (val) {
                if ($localStorage.get('user').warehouse_id == val.id) {
                    $rootScope.warehouse_name = val.name;
                    $rootScope.warehouse = val;
                }
            });
            // switch language
            $rootScope.switchLanguage = function (lang) {
                console.log('offlineFirstInit switchLanguage');
                var settings = $localStorage.get('settings');
                settings.locale = lang;
                $localStorage.set('settings', settings);
                $translate.use(lang);
                $rootScope.currentLang = lang;

            };
            // switch print language
            $rootScope.switchPrintLanguage = function (lang) {
                var settings = $localStorage.get('settings');
                settings.print_locale = lang;
                $localStorage.set('settings', settings);
                $rootScope.currentPrintLang = lang;

            };

            $rootScope.warehouseCheckbox = $localStorage.get('settings').warehouse_lock;
            $rootScope.warehouseLock = function (value) {
                var settings = $localStorage.get('settings');
                settings.warehouse_lock = value;
                $localStorage.set('settings', settings);
                $rootScope.warehouseCheckbox = value;
                console.log($rootScope.warehouseCheckbox);
            };

            $rootScope.selectWarehouse = function (war_name) {
                if (!$localStorage.get('settings').warehouse_lock) {
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        if (war_name == val.name) {
                            var user = $localStorage.get('user');
                            user.warehouse_id = val.id;
                            $localStorage.set('user', user);
                            var setting = $localStorage.get('settings');
                            setting.warehouse_id = val.id;
                            $localStorage.set('settings', setting);
                            $api.settings({
                                token: $localStorage.get('settings').token,
                                locale: $localStorage.get('settings').locale,
                                warehouse_id: val.id,
                            }).then(function (res) {
                                if (res.status == 'Y') { } else {
                                    $helper.toast(res.msg, 'short', 'bottom');
                                }
                            }).catch(function (err) {
                                $helper.toast(err, 'long', 'bottom');
                            });

                        }
                    });
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        if ($localStorage.get('user').warehouse_id == val.id) {
                            $rootScope.warehouse_name = val.name;
                            $rootScope.warehouse = val;
                        }
                    });
                    $rootScope.showWarehouseModal.hide();
                    $rootScope.showWarehouseModal.remove();
                    $rootScope.currentInvoiceId = null;
                    $helper.navForth('home', null, 'slide-left-right');
                    $rootScope.initCart();
                    //移除 modal setting
                    $rootScope.settingConfig.back();
                }

            }

            $rootScope.showWarehouse = function () {
                if (!$localStorage.get('settings').warehouse_lock) {
                    $rootScope.warehouseList = [];
                    angular.forEach($localStorage.get('warehouse'), function (val) {
                        $rootScope.warehouseList.push(val.name);
                    });
                    $rootScope.warehouseConfig = {
                        title: 'SELECT_WAREHOUSE',
                        back: function () {
                            $rootScope.showWarehouseModal.hide();
                            $rootScope.showWarehouseModal.remove();
                        }
                    };
                    $ionicModal.fromTemplateUrl('templates/modal.select-warehouse.html', {
                        scope: $rootScope,
                        animation: 'slide-in-up'
                    }).then(function (modal) {
                        $rootScope.showWarehouseModal = modal;
                        $rootScope.showWarehouseModal.show();
                    });
                }

            }

            $rootScope.showPrinter = function () {

                $rootScope.printerList = [];
                $rootScope.printerList.push('Other');
                $rootScope.printerList.push('EPSON thermal printer');

                $rootScope.printerConfig = {
                    title: 'SELECT_PRINTER',
                    back: function () {
                        $rootScope.showWarehouseModal.hide();
                        $rootScope.showWarehouseModal.remove();
                    }
                };
                $ionicModal.fromTemplateUrl('templates/modal.select-printer.html', {
                    scope: $rootScope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $rootScope.showWarehouseModal = modal;
                    $rootScope.showWarehouseModal.show();
                });

            }

            $rootScope.changeEpsonIpAddr = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_ip_address = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonIpAddress = addr;
                console.log($rootScope.epsonIpAddress);
            };


            $rootScope.changeEpsonPort = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_port = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonPort = addr;
                console.log($rootScope.epsonPort);
            };


            $rootScope.changeEpsonDeviceId = function (addr) {
                var settings = $localStorage.get('settings');
                settings.epson_device_id = addr;
                $localStorage.set('settings', settings);
                $rootScope.epsonDeviceId = addr;
                console.log($rootScope.epsonDeviceId);
            };

            $rootScope.selectPrinter = function (printer) {

                var settings = $localStorage.get('settings');
                settings.printer_type = printer;
                $localStorage.set('settings', settings);
                $rootScope.printerType = printer;
                console.log($rootScope.printerType);

                $rootScope.showWarehouseModal.hide();
                $rootScope.showWarehouseModal.remove();

            }

            //2017-06-02-city: set decimal point function
            $rootScope.decimalPoint = $localStorage.get('other_settings').decimal_point;

            $rootScope.setDecimalPoint = function (value, type) {
                var settings = $localStorage.get('other_settings');
                settings.decimal_point = value;
                $localStorage.set('other_settings', settings);
                $rootScope.decimalPoint = value;
                console.log("decimalPoint :" + $rootScope.decimalPoint);
            };
            console.log($rootScope.decimalPoint);

            //2017-06-01-city: device prefix function
            $rootScope.devicePrefix = $localStorage.get('other_settings').device_prefix == ' ' ? '' : $localStorage.get('other_settings').device_prefix;

            $rootScope.changeDevicePrefix = function (prefix) {
                var settings = $localStorage.get('other_settings');
                settings.device_prefix = prefix;
                $localStorage.set('other_settings', settings);
                $rootScope.devicePrefix = prefix;
                console.log("devicePrefix :" + $rootScope.devicePrefix);
            };

            //print by department
            $rootScope.isByDepartment = $localStorage.get('other_settings').is_by_department;

            $rootScope.switchOrderPrintOption = function (value) {
                var settings = $localStorage.get('other_settings');
                settings.is_by_department = value;
                $localStorage.set('other_settings', settings);
                $rootScope.isByDepartment = value;
                console.log($rootScope.isByDepartment);
            };

            //cloud print
            $rootScope.cloudPrint = $localStorage.get('settings').cloud_lock;

            $rootScope.switchCloudPrint = function (value) {
                var settings = $localStorage.get('settings');
                settings.cloud_lock = value;
                $localStorage.set('settings', settings);
                $rootScope.cloudPrint = value;
                console.log($rootScope.cloudPrint);
            };

            $rootScope.cloudPrintAddress = $localStorage.get('settings').cloud_address == ' ' ? '' : $localStorage.get('settings').cloud_address;

            $rootScope.changePrintAddr = function (addr) {
                var settings = $localStorage.get('settings');
                settings.cloud_address = addr;
                $localStorage.set('settings', settings);
                $rootScope.cloudPrintAddress = addr;
                console.log($rootScope.cloudPrintAddress);
            };

            $rootScope.logout = function (hideModalFunc) {
                console.log('log out');
                var user = $localStorage.get('user');
                user.id = null;
                user.isLogin = false;
                $localStorage.set('user', {});
                $helper.showLoading();
                $api.auth().then(function (res) {
                    $helper.hideLoading();
                    if (res.status == 'Y') {
                        var settings = $localStorage.get('settings');
                        settings.token = res.token;
                        // $rootScope.currentInvoiceId = null;
                        $rootScope.initCart();
                        $localStorage.set('settings', settings);
                        $rootScope.hideMenu();
                        $helper.navForth('login', null, 'slide-left-right');
                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.hideLoading();
                    $helper.toast(err, 'long', 'bottom');
                });

            };


            /**************************************************
            // switch sandbox
            **************************************************/
            if ($localStorage.get('activate').prefix == '') {
                $rootScope.sandboxCheckbox = false;
            } else {
                $rootScope.sandboxCheckbox = true;
            };
            $rootScope.switchSandbox = function (sandbox) {
                console.log('into meme~~');
                var activate = $localStorage.get('activate');
                if (sandbox) {
                    activate.prefix = 'sandbox.';
                } else {
                    activate.prefix = '';
                }
                $localStorage.set('activate', activate);
                $rootScope.sandboxCheckbox = sandbox;
                $rootScope.logout();
                $rootScope.settingModal.hide();
                $rootScope.settingModal.remove();
                $rootScope.initCart();
                // $rootScope.newCart();
            };



            /**************************************************
            // switch offline
            **************************************************/


            /**
             * switch offline mode
             */
            $rootScope.switchOffline = function () {
                var str = $rootScope.isOffline ? 'SWITCH_ONLINE' : 'SWITCH_OFFLINE';
                $ionicPopup.confirm({
                    title: $translate.instant('REMIND'),
                    template: $filter('translate')(str)
                }).then(function (res) {
                    if (res) {
                        var offlineStatus = $localStorage.get('offline');
                        $localStorage.set('offline', offlineStatus == 'online' ? 'offline' : 'online');
                        $rootScope.isOffline = !$rootScope.isOffline;
                        $rootScope.networkResult = !$rootScope.isOffline && $rootScope.networkStatus;
                        $helper.judgeUpdate($rootScope.sandboxCheckbox ? $localStorage.get('sandbox_update_time') : $localStorage.get('production_update_time'));
                        console.log('network result : ' + $rootScope.networkResult);
                        $helper.navForth('home', null, 'slide-left-right');
                        $rootScope.initCart();
                        //移除 modal setting
                        $rootScope.settingConfig.back();

                    }
                });
            };


        };


        $rootScope.downloadData = function (remindStr) {
            // $rootScope.showDownload = true;
            $ionicPopup.confirm({
                title: $translate.instant('REMIND'),
                template: $translate.instant(remindStr),
                buttons: [{
                    text: $translate.instant('CANCEL'),
                    onTap: function (e) {
                        return false;
                    }
                }, {
                    type: 'button-positive',
                    text: $translate.instant('CONFIRM'),
                    onTap: function (e) {
                        return true;
                    }
                },

                ]
            }).then(function (res) {
                if (res) {
                    // data
                    if($rootScope.settingConfig){
                        $rootScope.settingConfig.back();
                    }                    
                    $rootScope.showDownload = true;
                    $offline.getOfflineData({
                        token: $localStorage.get('settings').token,
                        locale: $localStorage.get('settings').locale,
                        category: 'POS',
                        type: 'SQLite'
                    }).then(function (res) {
                        console.log('deploy data success!!!');
                        $offline.getOfflinePhoto({
                            token: $localStorage.get('settings').token,
                            locale: $localStorage.get('settings').locale,
                        }).then(function (res) {
                            $rootScope.showDownload = false;
                            console.log('deploy photos success!!!');
                            $ionicPopup.alert({
                                title: '',
                                template: $translate.instant('DOWNLOAD_SUCCESS')
                            }).then(function (res) {

                            });
                            if ($rootScope.sandboxCheckbox) {
                                $localStorage.set('sandbox_update_time', $rootScope.sandboxUpdateTime);
                            } else {
                                $localStorage.set('production_update_time', $rootScope.productionUpdateTime);
                            }
                        }, function (err) {
                            console.log('photo error');
                            $rootScope.showDownload = false;
                        }).catch(function (err) {
                            $rootScope.showDownload = false;
                            console.log('catch getOfflinePhoto error!!!');
                            $helper.toast(err, 'short', 'bottom');
                        });
                    }).catch(function (err) {
                        $rootScope.showDownload = false;
                        console.log('catch get OfflineData error!!!');
                        console.log(err);
                        $helper.toast(err, 'short', 'bottom');
                    });
                }
            });
        };

        $rootScope.popoverMenu = function ($event) {
            $ionicPopover.fromTemplateUrl('templates/popover.setting.html', {
                scope: $rootScope
            }).then(function (popover) {
                $rootScope.menuPopover = popover;
                popover.show($event);
            });
        };

        $rootScope.hideMenu = function () {
            if ($rootScope.menuPopover) {
                $rootScope.menuPopover.hide();
                $rootScope.menuPopover.remove();
            }
        };

        $rootScope.modalCustomerSearchBar = {
            searchHints: $translate.instant('SEARCH_CUSTOMER_HINTS'),
            searchKeyword: '',
            searchFor: function (keyword) {
                console.log('search for');
                $rootScope.loadCustomerList('refresh', keyword);
            },
            back: function () {
                $rootScope.memberModal.hide();
                $rootScope.memberModal.remove();
            },
            scanQR: function () {
                document.addEventListener("deviceready", function () {

                    $helper.scan(function (scanData) {
                        if (scanData.cancelled == 0) {
                            $rootScope.loadCustomerList('refresh', scanData.text);
                        }
                    });
                }, false);
            }
        };

        $rootScope.popOverOperation = {
            popoverMemberList: function ($event) {
                if ($rootScope.isMemberDetail) {
                    $rootScope.loadCustomerDetail('init');

                } else {
                    $rootScope.loadMenuCustomerList('init');
                };
                $rootScope.menuCustomerSearchBar = {
                    searchHints: $translate.instant('SEARCH_CUSTOMER_HINTS'),
                    searchKeyword: '',
                    searchFor: function (keyword) {
                        $rootScope.isMemberDetail = false;
                        $rootScope.currentMemberId = 0;
                        $rootScope.currentUserId = 0;
                        $localStorage.set('memberProfile', {
                            member_id: ''
                        });
                        $rootScope.loadMenuCustomerList('refresh', keyword);
                    },
                    back: function () {
                        $rootScope.memberModal.hide();
                        $rootScope.memberModal.remove();
                    },
                    clear: function () {
                        $rootScope.menuCustomerSearchBar.searchKeyword = '';
                        $rootScope.loadMenuCustomerList('init');
                    },
                    scanQR: function () {
                        document.addEventListener("deviceready", function () {
                            $helper.scan(function (scanData) {
                                console.log(scanData, 11111111)
                                if (scanData.cancelled == 0) {
                                    $scope.loadMenuCustomerList('refresh', scanData.text);

                                }
                            });
                        }, false);
                    }
                };
                $ionicPopover.fromTemplateUrl('templates/popover.member-list.html', {
                    scope: $rootScope
                }).then(function (popover) {
                    $rootScope.memberPopover = popover;
                    popover.show($event);
                });
            },

            processMemberDetail: function (member) {
                console.log(member, 'member~~~');
                $rootScope.isMemberDetail = true;
                $rootScope.currentMemberId = member.member_id;
                $rootScope.memberID = member.member_id;
                $rootScope.currentUserId = member.user_id;
                $localStorage.set('memberProfile', {
                    member_id: member.member_id
                });
                $rootScope.currentMember = member;
                console.log($rootScope.currentMember.member_coin);
                $rootScope.memberPoint = $rootScope.currentMember.member_coin;
                console.log($rootScope.memberPoint, 4444444);
                $rootScope.memberDiscountPercent = Number(member.discount) / 100;
                $rootScope.loadCustomerDetail('init');
                $rootScope.confirmChangeMember($rootScope.currentUserId);
                console.log("change member id:" + member.member_id);
                //$rootScope.confirmChangeMember(member.member_id);

            },
            memberBack: function () {
                console.log($rootScope.currentUserId);
                $rootScope.isMemberDetail = false;
                $rootScope.currentMemberId = 0;
                $rootScope.currentUserId = 0;
                $localStorage.set('memberProfile', {
                    member_id: ''
                });
                $rootScope.currentMember = null;
                $rootScope.memberDiscountPercent = 0;
                //modify by parer 20170613
                //$rootScope.initCart();

            },
            popoverBack: function () {
                $rootScope.memberPopover.hide();
                $rootScope.memberPopover.remove();
            }
        };

        $rootScope.scanMember = function () {
            document.addEventListener("deviceready", function () {
                $helper.scan(function (scanData) {
                    console.log(scanData, 5555555555)
                    if (scanData.cancelled == 0) {
                        $rootScope.loadCustomerList('refresh', scanData.text);
                    }
                });
            }, false)
        }
        //made by parker 20170613
        //clear search key and then load costomer list 
        $rootScope.customerListOperation = {
            popUpCustomerList: function () {
                $rootScope.hideMenu();
                $rootScope.loadCustomerList('init');
                $rootScope.searchButtonBar = {
                    searchHints: $filter('translate')('SEARCH_CUSTOMER_HINTS'),
                    searchKeyword: '',
                    searchFor: function (keyword) {
                        $rootScope.loadCustomerList('refresh', keyword);
                    },
                    back: function () {
                        $rootScope.memberModal.hide();
                        $rootScope.memberModal.remove();
                    },
                    scanQR: function () {
                        $rootScope.scanMember();
                    }
                };
                $rootScope.proccessMemberDetail = function (memberID) {
                    $localStorage.set('memberProfile', {
                        member_id: memberID
                    });
                    $rootScope.loadModalCustomerDetail('init');
                    console.log("change member id:" + memberID);
                    $ionicModal.fromTemplateUrl('templates/modal.member-detail.html', {
                        scope: $rootScope
                    }).then(function (modal) {
                        $rootScope.memberDetailModal = modal;
                        $rootScope.memberDetailModalBack = function () {
                            $rootScope.memberDetailModal.hide();
                            $rootScope.memberDetailModal.remove();
                        };
                        console.log('ohohohohohoho');
                        modal.show();
                    });
                };
                $ionicModal.fromTemplateUrl('templates/modal.member-list.html', {
                    scope: $rootScope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $rootScope.memberModal = modal;
                    modal.show();
                });
            },
            back: function () {
                $rootScope.memberModal.hide();
                $rootScope.memberModal.remove();
            },
            // clearCostomerKey : function() {
            // console.log('清楚。。。')
            // $rootScope.menuCustomerSearchBar.searchKeyword = '';
            // $rootScope.loadMenuCustomerList('init')
            // console.log(88888);

            //}

        };

        $rootScope.popUpDate = function (type) {

            var isEn = $localStorage.get('settings').locale == 'EN_US';
            var ipObj1 = {

                callback: function (val) { //Mandatory
                    var dateObj = new Date(val);
                    console.log('Return value from the datepicker popup is : ' + val, dateObj);
                    month = '' + (dateObj.getMonth() + 1);
                    day = '' + dateObj.getDate();
                    year = dateObj.getFullYear();
                    if (month.length < 2) month = '0' + month;
                    if (day.length < 2) day = '0' + day;
                    console.log(type + ", select date:" + [year, month, day].join('-'));
                    console.log(val);
                    console.log(Number($rootScope.reportStartDate.replace('-', '').replace('-', '')));
                    console.log(Date.parse($rootScope.reportStartDate + "T16:00:00.000Z"));
                    console.log(Date.parse($rootScope.reportEndDate + "T16:00:00.000Z"));
                    if (type == 'start') {
                        if (Number(year + month + day) <= Number($rootScope.reportStartDate.replace('-', '').replace('-', ''))) {
                            if ($rootScope.reportStartDate != [year, month, day].join('-')) {
                                $rootScope.changeDateAndRefresh(type, [year, month, day].join('-'))
                            }
                        }
                    } else {
                        if (Number(year + month + day) >= Number($rootScope.reportStartDate.replace('-', '').replace('-', ''))) {
                            if ($rootScope.reportEndDate != [year, month, day].join('-')) {
                                $rootScope.changeDateAndRefresh(type, [year, month, day].join('-'))
                            }
                        }
                    }
                },

                currentYear: new Date().getFullYear(),
                inputDate: type == 'start' ? new Date($rootScope.reportStartDate) : new Date($rootScope.reportEndDate),
                mondayFirst: true,
                closeOnSelect: false,
                templateType: 'popup',
                weeksList: isEn ? ["S", "M", "T", "W", "T", "F", "S"] : ["日", "一", "二", "三", "四", "五", "六"],
                monthsList: isEn ? ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"] : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                setLabel: $filter('translate')('CONFIRM'),
                todayLabel: $filter('translate')('TODAY'),
                closeLabel: $filter('translate')('CANCEL'),
                from: new Date().setMonth(new Date().getMonth() - 12),
                to: new Date()
            };
            ionicDatePicker.openDatePicker(ipObj1);
        };

        // 2017-5-23 - City: add start date and end date
        $rootScope.changeDateAndRefresh = function (type, date) {
            if (type == 'start') {
                $rootScope.reportStartDate = date;
                console.log($rootScope.reportStartDate);
            } else {
                $rootScope.reportEndDate = date;
                console.log($rootScope.reportEndDate);
            }
            $rootScope.iframeUrl = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fmanage-day-end-report-fab&warehouse_id=' + $localStorage.get('user').warehouse_id + '&locale=' + $localStorage.get('settings').locale + '&start_date=' + $rootScope.reportStartDate + '&end_date=' + $rootScope.reportEndDate;
            console.log($rootScope.iframeUrl);
            $rootScope.iframeUrl = $sce.trustAsResourceUrl($rootScope.iframeUrl);
        };

        $rootScope.printStaticRang = function (type) {
            if (type == 'week') {
                $rootScope.reportStartDate = new Date($rootScope.reportEndDate).setDate(new Date($rootScope.reportEndDate).getDate() - 7);
            } else if (type == 'month') {
                $rootScope.reportStartDate = new Date($rootScope.reportEndDate).setMonth(new Date($rootScope.reportEndDate).getMonth() - 1);
            } else if (type == '3month') {
                $rootScope.reportStartDate = new Date($rootScope.reportEndDate).setMonth(new Date($rootScope.reportEndDate).getMonth() - 3);
            }
            var date = new Date($rootScope.reportStartDate);
            month = '' + (date.getMonth() + 1);
            day = '' + date.getDate();
            year = date.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            console.log([year, month, day].join('-'));
            $rootScope.reportStartDate = [year, month, day].join('-');

            $rootScope.iframeUrl = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fmanage-day-end-report-fab&warehouse_id=' + $localStorage.get('user').warehouse_id + '&locale=' + $localStorage.get('settings').locale + '&start_date=' + $rootScope.reportStartDate + '&end_date=' + $rootScope.reportEndDate;
            console.log($rootScope.iframeUrl);
            $rootScope.iframeUrl = $sce.trustAsResourceUrl($rootScope.iframeUrl);
        };

        $rootScope.showReport = function () {

            $rootScope.iframeUrl = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fmanage-day-end-report-fab&warehouse_id=' + $localStorage.get('user').warehouse_id + '&locale=' + $localStorage.get('settings').locale + '&start_date=' + $rootScope.reportStartDate + '&end_date=' + $rootScope.reportEndDate;
            console.log($rootScope.iframeUrl);
            console.log($rootScope.platform);
            // if ($rootScope.platform == 'web') {
            $rootScope.iframeUrl = $sce.trustAsResourceUrl($rootScope.iframeUrl);
            // }
            $rootScope.viewInvoiceC = {
                iframe: null,
                innerDoc: null,
                back: function () {
                    $rootScope.iframeModal.hide();
                    $rootScope.iframeModal.remove();
                }
            };
            // var htmlTemplate = $rootScope.platform == 'web' ? 'templates/modal.iframe.full.html' : 'templates/common.webview.report.html';
            var htmlTemplate = 'templates/modal.iframe.full.html';
            $ionicModal.fromTemplateUrl(htmlTemplate, {
                scope: $rootScope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $rootScope.iframeModal = modal;
                modal.show().then(function () {

                });
            });
        };

        $rootScope.orderHistoryOperation = {
            popUpOrderHistory: function () {
                $rootScope.hideMenu();
                $rootScope.loadOrderHistory('refresh');
                $rootScope.searchButtonBar = {
                    searchHints: $filter('translate')('SEARCH_INVOICE_HINTS'),
                    searchKeyword: '',
                    searchFor: function (keyword) {
                        $rootScope.loadOrderHistory('refresh', keyword);
                    },
                    back: function () {
                        $rootScope.can_open_history = true;
                        $rootScope.historyModal.hide();
                        $rootScope.historyModal.remove();
                    },
                    scanQR: function () {
                        document.addEventListener("deviceready", function () {
                            $helper.scan(function (scanData) {
                                if (scanData.cancelled == 0) {
                                    $scope.loadOrderHistory('refresh', scanData.text);
                                }
                            });
                        }, false);
                    },
                    //modify by parker 20170616
                    close: function () {
                        console.log('开始清空');
                        $rootScope.searchButtonBar.searchKeyword = '';
                        $rootScope.loadOrderHistory('refresh');
                    }
                    //end
                };
                $ionicModal.fromTemplateUrl('templates/modal.order-history.html', {
                    scope: $rootScope,
                    animation: 'none'
                }).then(function (modal) {
                    $rootScope.historyModal = modal;
                    modal.show();
                });
            },
            back: function () {
                $rootScope.historyModal.hide();
                $rootScope.historyModal.remove();
            },
            voidOrderHistory: function (invoiceId) {

                $ionicPopup.confirm({
                    title: $translate.instant('REMIND'),
                    template: $translate.instant('CONFIRM_VOID_ORDER'),
                    buttons: [{
                        text: $translate.instant('CANCEL'),
                        onTap: function (e) {
                            return false;
                        }
                    }, {
                        type: 'button-positive',
                        text: $translate.instant('CONFIRM'),
                        onTap: function (e) {
                            return true;
                        }
                    }]
                }).then(function (res) {
                    if (res) {
                        $api.setCart({
                            token: $localStorage.get('settings').token,
                            locale: $localStorage.get('settings').locale,
                            warehouse_id: $localStorage.get('user').warehouse_id,
                            action: 'delete',
                            invoice_id: invoiceId,
                            calling_type: 'f_b'
                        }).then(function (res) {
                            if (res.status == 'Y') {
                                $rootScope.loadOrderHistory('refresh');
                            } else {
                                $helper.toast(res.msg, 'short', 'bottom');
                            }
                        }).catch(function (err) {
                            $helper.toast(err, 'long', 'bottom');

                        });
                    }
                });
            }
        };

        $rootScope.loadMenuCustomerList = function (mode, keyword) {
            console.log('load customer list!!!' + mode + ',' + keyword);
            if (mode != 'more' || $rootScope.menuCustomerList == undefined) {
                $scope.menuCustomerLimitFrom = 0;
                $scope.menuCustomerLimit = 20;
                $rootScope.menuCustomerCount = 0;
            } else {
                $scope.menuCustomerLimitFrom += 20;
            }

            $api.getMemberList({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                keyword: keyword != undefined ? keyword : null,
                limit_from: $scope.menuCustomerLimitFrom,
                limit: $scope.menuCustomerLimit
            }).then(function (res) {
                if (mode != 'more' || $rootScope.menuCustomerList == undefined) {
                    $rootScope.menuCustomerList = [];
                }
                if (res.status == 'Y') {
                    $rootScope.menuCustomerCount = res.member.count;
                    for (var i = 0; i < res.member.list.length; i++) {
                        res.member.list[i].prefix = $localStorage.get('user').invoice_prefix;
                        $rootScope.menuCustomerList.push(res.member.list[i]);
                    }
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
                if (mode == 'refresh') {
                    $rootScope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $rootScope.$broadcast('scroll.infiniteScrollComplete');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
                if (mode == 'refresh') {
                    $rootScope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $rootScope.$broadcast('scroll.infiniteScrollComplete');
                }
            });

        };

        $rootScope.loadcartList = function (mode) {
            $scope.cartLimit = 20;
            $rootScope.cartCount = 0;
            $rootScope.cartList = [];
            if (mode != 'more' || $rootScope.cartList == undefined) {
                $scope.cartLimitFrom = 0;
            } else {
                $scope.cartLimitFrom += 20;
            }
            $scope.customerID = 1;
            $scope.customerID = $localStorage.get('memberProfile');
            $scope.status = [5];
            $api.getInvoiceList({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                user_id: $scope.customerID,
                limit_from: $scope.cartLimitFrom,
                limit: $scope.cartLimit,
                status: $scope.status
            }).then(function (res) {
                if (res.status == 'Y') {
                    $rootScope.cartCount = res.data.count;
                    for (var i = 0; i < res.data.list.length; i++) {
                        $rootScope.cartList.push(res.data.list[i]);
                    }
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
                if (mode == 'refresh') {
                    $scope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
                if (mode == 'refresh') {
                    $scope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $scope.$broadcast('scroll.infiniteScrollComplete');
                }
            });
        };

        $rootScope.loadCustomerDetail = function (mode) {

            if (mode != 'more' || $rootScope.cartList == undefined) {
                $scope.cartLimitFrom = 0;
                $scope.cartLimit = 20;
                $rootScope.cartCount = 0;
                $rootScope.cartList = [];
            } else {
                $scope.cartLimitFrom += 20;
            }
            $api.getMemberProfile({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                member_id: $rootScope.currentMemberId
            }).then(function (res) {
                if (res.status == 'Y') {
                    $rootScope.customerDetail = res.data;
                    $rootScope.customerDetail.prefix = $localStorage.get('user').invoice_prefix;
                    $rootScope.currentUserId = res.data.user_id;
                    $rootScope.currentMember = res.data;
                    $scope.status = [2, 3, 4, 5];
                    $api.getInvoiceList({
                        token: $localStorage.get('settings').token,
                        locale: $localStorage.get('settings').locale,
                        user_id: $rootScope.currentUserId,
                        limit_from: $scope.cartLimitFrom,
                        limit: $scope.cartLimit,
                        status: $scope.status
                    }).then(function (res) {
                        if (res.status == 'Y') {
                            $rootScope.cartCount = res.data.count;
                            for (var i = 0; i < res.data.list.length; i++) {
                                $rootScope.cartList.push(res.data.list[i]);
                            }
                        } else {
                            $helper.toast(res.msg, 'short', 'bottom');
                        }
                        if (mode == 'refresh') {
                            $scope.$broadcast('scroll.refreshComplete');
                        }
                        if (mode == 'more') {
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        }
                    }).catch(function (err) {
                        $helper.toast(err, 'long', 'bottom');
                        if (mode == 'refresh') {
                            $scope.$broadcast('scroll.refreshComplete');
                        }
                        if (mode == 'more') {
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        }
                    });
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
            });
        };

        $rootScope.loadModalCustomerDetail = function (mode) {

            if (mode != 'more' || $rootScope.cartList == undefined) {
                $scope.cartLimitFrom = 0;
                $scope.cartLimit = 20;
                $rootScope.cartCount = 0;
                $rootScope.cartList = [];
            } else {
                $scope.cartLimitFrom += 20;
            }
            $api.getMemberProfile({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                member_id: $localStorage.get('memberProfile')
            }).then(function (res) {
                if (res.status == 'Y') {
                    $helper.checkUndefined(res.data);
                    $rootScope.customerDetail = res.data;
                    $scope.customerID = res.data.user_id;
                    $scope.status = [2, 3, 4, 5];
                    $api.getInvoiceList({
                        token: $localStorage.get('settings').token,
                        locale: $localStorage.get('settings').locale,
                        user_id: $scope.customerID,
                        limit_from: $scope.cartLimitFrom,
                        limit: $scope.cartLimit,
                        status: $scope.status
                    }).then(function (res) {
                        if (res.status == 'Y') {
                            $rootScope.cartCount = res.data.count;
                            for (var i = 0; i < res.data.list.length; i++) {
                                $rootScope.cartList.push(res.data.list[i]);
                            }
                        } else {
                            $helper.toast(res.msg, 'short', 'bottom');
                        }
                        if (mode == 'refresh') {
                            $scope.$broadcast('scroll.refreshComplete');
                        }
                        if (mode == 'more') {
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        }
                    }).catch(function (err) {
                        $helper.toast(err, 'long', 'bottom');
                        if (mode == 'refresh') {
                            $scope.$broadcast('scroll.refreshComplete');
                        }
                        if (mode == 'more') {
                            $scope.$broadcast('scroll.infiniteScrollComplete');
                        }
                    });
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
            });
        };

        $rootScope.loadOrderHistory = function (mode, keyword) {
            console.log('mode: ' + mode + ' ,keyword : ' + keyword);
            if ($rootScope.searchButtonBar && !keyword)
                $rootScope.searchButtonBar.searchKeyword = '';
            if (mode != 'more' || $rootScope.orderHistoryList == undefined) {
                $scope.invoiceLimitFrom = 0;
                $scope.invoiceLimit = 20;
                $rootScope.invoiceCount = 0;
            } else {
                $scope.invoiceLimitFrom += 20;
            }
            $scope.status = [5];
            $api.getInvoiceList({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                keyword: keyword != undefined ? keyword : null,
                limit_from: $scope.invoiceLimitFrom,
                limit: $scope.invoiceLimit,

                // warehouse_id: $localStorage.get('user').warehouse_id,
                status: $scope.status
            }).then(function (res) {
                if (res.status == 'Y') {
                    console.log('order history res');
                    console.log(res);
                    if (mode != 'more' || $rootScope.orderHistoryList == undefined) {
                        $rootScope.orderHistoryList = [];
                    }
                    $rootScope.invoiceCount = res.data.count;
                    for (var i = 0; i < res.data.list.length; i++) {
                        $rootScope.orderHistoryList.push(res.data.list[i]);
                    }
                } else {
                    $helper.toast(res.msg, 'short', 'bottom');
                }
                if (mode == 'refresh') {
                    $rootScope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $rootScope.$broadcast('scroll.infiniteScrollComplete');
                }
            }).catch(function (err) {
                $helper.toast(err, 'long', 'bottom');
                if (mode == 'refresh') {
                    $rootScope.$broadcast('scroll.refreshComplete');
                }
                if (mode == 'more') {
                    $rootScope.$broadcast('scroll.infiniteScrollComplete');
                }
            });

        };

        // load invoice detail
        $rootScope.loadInvoiceDetail = function (ionvice) {
            if (!$rootScope.can_open_history)
                return;
            $rootScope.can_open_history = false;
            // $rootScope.showReceiptPage = true;
            $rootScope.showPrintPage = true;
            $api.getInvoiceDetail({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                invoice_id: ionvice.id,
                calling_type: 'f_b'
            }).then(function (res) {
                if (res.status == 'Y') {
                    var logo_img = $localStorage.get('local_icon');
                    // if ($rootScope.currentOrderType && $rootScope.currentOrderType.id == 2)
                    //     $scope.confirmPayment();
                    console.log('order history detail : ');
                    console.log(res);
                    $translate.use($rootScope.currentPrintLang);

                    var isOctopus = res.data.octopus_info && (res.data.octopus_info != '');

                    $scope.getImageToBase64AndPrint(logo_img, res.data, isOctopus);
                    // $scope.invoiceDetail = res.data;
                    // $scope.productDetail = [];
                    // for (var i = 0; i < res.data.products.length; i++) {
                    //     $scope.productDetail.push(res.data.products[i]);
                    //     $scope.productDetail[i].actual_pickup_qty = $scope.productDetail[i].can_pick_qty;
                    // }
                    console.log(res.data);
                } else {
                    $rootScope.can_open_history = true;
                    $helper.toast(res.msg, 'short', 'bottom');
                }

            }).catch(function (err) {
                $rootScope.can_open_history = true;
                $helper.toast(err, 'long', 'bottom');
            });

            // $rootScope.printInvoiceId = ionvice.id;
            // $rootScope.pdfUrl = ionvice.pdf;
            // if ($rootScope.platform == 'web') {
            //     $rootScope.pdfUrl = $sce.trustAsResourceUrl(ionvice.pdf);
            // }
            // console.log($rootScope.pdfUrl);
            // $rootScope.viewInvoiceC = {
            //     mode: 'orderHistory',
            //     back: function() {
            //         $scope.processCheckoutModal.hide();
            //         $scope.processCheckoutModal.remove();
            //     }
            // };
            // $ionicModal.fromTemplateUrl('templates/modal.view-invoice.html', {
            //     scope: $rootScope,
            //     animation: 'slide-in-up'
            // }).then(function(modal) {
            //     $scope.processCheckoutModal = modal;
            //     modal.show();
            // });

        };

        $scope.getImageToBase64AndPrint = function (imgUrl, r, isOctopus) {
            var canvas = document.createElement('CANVAS');
            console.log('2');
            var ctx = canvas.getContext('2d');
            console.log('2.2');
            var img = new Image;
            img.crossOrigin = 'Anonymous';
            img.src = imgUrl;
            console.log('2.3');
            console.log(imgUrl);
            console.log(angular.toJson(img));
            img.onload = function () {
                canvas.height = img.height;
                canvas.width = img.width;
                console.log('~1');
                ctx.drawImage(img, 0, 0);
                console.log('~2');
                var dataURL = canvas.toDataURL('image/png');
                console.log('~3');
                console.log(r);
                // Clean up
                // var baseVal = dataURL.replace('data:image/png;base64,', '');
                console.log(dataURL);

                var products = [];
                var i = 1;

                for (var j = 0; j < r.products.length; j++) {
                    var p = r.products[j];
                    var options = '<br/>';

                    angular.forEach(p.options, function (opt) { });
                    for (var i = p.options.length - 1; i >= 0; i--) {
                        if (p.options[i].options[0] != '不需要') {
                            if (i < p.options.length - 1)
                                options = options + ',';
                            options = options + p.options[i].options[0];
                        }
                    }

                    for (var i = p.addons.length - 1; i >= 0; i--) {
                        if (p.addons[i].qty > 0) {
                            // if (i < p.addons.length - 1)
                            if (i > 0)
                                options = options + ' ';
                            // options = options + p.options[i].title + ':' + p.options[i].options[0];
                            if ($scope.currentPrintLang == 'EN_US') {
                                options = options + p.addons[i].name_EN_US + '*' + p.addons[i].qty;
                            } else if ($scope.currentPrintLang == 'ZH_CN') {
                                options = options + p.addons[i].name_ZH_CN + '*' + p.addons[i].qty;
                            } else {
                                options = options + p.addons[i].name_ZH_HK + '*' + p.addons[i].qty;
                            }
                        }
                    }

                    if (options.length == 5) {
                        options = '';
                    }
                    var dis = '';
                    console.log('!!!!!~~~~~~~');
                    console.log(p.o_price);
                    console.log(p.unit_price);
                    if (Number(p.o_price) == Number(p.unit_price) || Number(p.unit_price) == 0) {
                        dis = '-';
                    } else {
                        dis = (p.o_price - p.unit_price) / p.o_price * 100;
                        dis = dis.toFixed(1) + '%';
                    }

                    products.push({
                        ITEM_NO: i,
                        PRODUCT_CODE: p.code,
                        PRODUCT_NAME: p.qty == 0 ? p.name + '(' + $translate.instant('CANCELED') + ')' + options : p.name + options,
                        QTY: p.qty,
                        UNIT_PRICE: p.unit_price,
                        DISCOUNT: dis,
                        SUB_TOTAL: '$' + p.sub_total.toFixed(1)
                    });
                    i++;
                }

                var charges = [];
                var baseTotal = r.item_total;

                for (var i = 0; i < r.invoice_charges.length; i++) {
                    var charge = r.invoice_charges[i];
                    if (charge.value_type == 'percent') {
                        if (charge.sign == '+') {
                            //service charge
                            var totalFlag = baseTotal;
                            var charge_value = charge.value + '%';
                            baseTotal *= 1 + Number(charge.value) / 100;
                            var baseCharge = baseTotal - totalFlag;
                            if ($rootScope.currentPrintLang == 'EN_US') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_EN_US,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else if ($rootScope.currentPrintLang == 'ZH_ZN') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.ZH_ZN,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_ZH_HK,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });

                            }
                        } else if (charge.sign == '-') {
                            //discount
                            var totalFlag = baseTotal;
                            var charge_value = '(' + charge.value + '%)';
                            baseTotal *= 1 - Number(charge.value) / 100;
                            var baseDiscount = totalFlag - baseTotal;
                            if ($rootScope.currentPrintLang == 'EN_US') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_EN_US,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else if ($rootScope.currentPrintLang == 'ZH_ZN') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.ZH_ZN,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_ZH_HK,
                                        CHARGE_VALUE: charge_value,
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });
                            }
                        }
                    }
                }

                for (var i = 0; i < r.invoice_charges.length; i++) {
                    var charge = r.invoice_charges[i];
                    if (charge.value_type != 'percent') {

                        if (charge.sign == '+') {
                            //service charge
                            var totalFlag = baseTotal;
                            var charge_value = '$' + charge.value;
                            baseTotal += Number(charge.value);
                            var baseCharge = baseTotal - totalFlag;
                            if ($rootScope.currentPrintLang == 'EN_US') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_EN_US,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else if ($rootScope.currentPrintLang == 'ZH_ZN') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.ZH_ZN,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_ZH_HK,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '$' + Number(baseCharge).toFixed(1),
                                        CHARGE_SIGN: charge.sign
                                    });
                            }

                        } else if (charge.sign == '-') {
                            //discount
                            var totalFlag = baseTotal;
                            var charge_value = '($' + charge.value + ')';
                            baseTotal -= Number(charge.value);
                            var baseDiscount = totalFlag - baseTotal;
                            if ($rootScope.currentPrintLang == 'EN_US') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_EN_US,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else if ($rootScope.currentPrintLang == 'ZH_ZN') {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.ZH_ZN,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });

                            } else {
                                if (Number(charge.value) != 0)
                                    charges.push({
                                        CHARGE_NAME: charge.title_ZH_HK,
                                        CHARGE_VALUE: '',
                                        CHARGE_VALUE_TYPE: charge.value_type,
                                        CHARGE_AMOUNT: '($' + Number(baseDiscount).toFixed(1) + ')',
                                        CHARGE_SIGN: charge.sign
                                    });

                            }
                        }
                    }
                }

                var qr = new QRious({ value: r.invoice_no });
                var invoice_no_qr = qr.toDataURL();
                var currentTime = new Date();

                if (!r.customer_info || r.customer_info.member_id == 0) {
                    var member_class = $translate.instant('Visitor');
                    var customer_detail = '-';
                } else {
                    var member_class = r.customer_info.vip_level;
                    var customer_detail = r.customer_info.customer_name + '<br /> \
                                    Tel: ' + r.customer_info.customer_country_code + ' ' + r.customer_info.customer_mobile + '<br /> \
                                    Email: ' + r.customer_info.customer_email + '<br />';
                }

                console.log(invoice_no_qr);
                console.log(r.ticket_num);
                console.log(products);
                console.log(charges);
                console.log(r.customer_info);
                console.log(r.pay_method);
                var pay_method = '';
                for (var i = 0; i < $rootScope.choosePaymentList.length; i++) {
                    if ($rootScope.choosePaymentList[i].name == r.pay_method) {
                        pay_method = $translate.instant($rootScope.choosePaymentList[i].translateKey);
                    }
                }
                var print_type = '';
                if (r.table_num == 0) {
                    print_type = 'receipt';
                } else {
                    print_type = 'receipt-dinein';
                }

                var address = '';
                if ($scope.currentPrintLang == 'ZH_HK' && $rootScope.warehouse.warehouse_address_ZH_HK) {
                    // address = '上水新祥街21号地下';
                    address = $rootScope.warehouse.warehouse_address_ZH_HK;
                } else if ($scope.currentPrintLang == 'ZH_CN' && $rootScope.warehouse.warehouse_address_ZH_CN) {
                    // address = '上水新祥街21號地下';
                    address = $rootScope.warehouse.warehouse_address_ZH_CN;
                } else if ($scope.currentPrintLang == 'EN_US' && $rootScope.warehouse.warehouse_address_EN_US) {
                    address = $rootScope.warehouse.warehouse_address_EN_US;
                } else {
                    address = $rootScope.warehouse.warehouse_name;
                }

                var splitRecivePrice = 0;
                var splitPayMethod = '';
                if (r.payments && r.payments.length > 0) {
                    for (var i = 0; i < r.payments.length; i++) {
                        splitRecivePrice += r.payments[i].amount;
                        //if want to show pay amount
                        // splitPayMethod += $translate.instant(r.payments[i].pay_method) + ':  $' + r.payments[i].amount + '<br />';
                        if (i != r.payments.length - 1) {
                            for (var j = 0; j < $rootScope.choosePaymentList.length; j++) {
                                if ($rootScope.choosePaymentList[j].name == r.payments[i].pay_method) {
                                    splitPayMethod += $translate.instant($rootScope.choosePaymentList[j].translateKey) + ', ';
                                }
                            }
                        }
                        else {
                            for (var j = 0; j < $rootScope.choosePaymentList.length; j++) {
                                if ($rootScope.choosePaymentList[j].name == r.payments[i].pay_method) {
                                    splitPayMethod += $translate.instant($rootScope.choosePaymentList[j].translateKey);
                                }
                            }
                        }
                        console.log($translate.instant(r.payments[i].pay_method), r.payments[i].amount);
                    };
                    console.log(splitRecivePrice, splitPayMethod);
                }
                if (splitPayMethod == '') {
                    if (r.payments && r.payments.length > 0) {
                        for (var i = 0; i < r.payments.length; i++) {
                            //if want to show pay amount
                            // splitPayMethod += $translate.instant(r.payments[i].pay_method) + ':  $' + r.payments[i].amount + '<br />';
                            if (i != r.payments.length - 1)
                                splitPayMethod += $translate.instant(r.payments[i].pay_method) + ', ';
                            else
                                splitPayMethod += $translate.instant(r.payments[i].pay_method);
                        }
                    }
                }

                // 八达通信息修整      
                var octopusDateStr = '0.0'; 
                var octopusDateTitle = '';
                var octopusLastaddmethod = '';
                var octopusDateEnd = '';
                if(isOctopus && r.octopus_info){
                    console.log('value value value value');              
                    console.log(r.octopus_info);                    
                    if ($rootScope.currentPrintLang == 'EN_US') {
                        octopusDateTitle = 'Last add value by';                    
                    } else if ($rootScope.currentPrintLang == 'ZH_HK') {
                        octopusDateTitle = '上一次於';
                        switch (r.octopus_info.lastaddmethod) {
                            case 'Cash': octopusLastaddmethod = '現金';break;
                            case 'AAVS': octopusLastaddmethod = '自動';break;
                            case 'Online': octopusLastaddmethod = '網上';break;
                        }
                        octopusDateEnd = '增值';
                    } else {
                        octopusDateTitle = '上一次于';
                        switch (r.octopus_info.lastaddmethod) {
                            case 'Cash': octopusLastaddmethod = '现金';break;
                            case 'AAVS': octopusLastaddmethod = '自动';break;
                            case 'Online': octopusLastaddmethod = '网上';break;
                        }                    
                        octopusDateEnd = '充值';
                    } 
                }
   

                if(isOctopus && r.octopus_info && r.octopus_info.lastaddvaluedate && r.octopus_info.lastaddmethod){
                    if($localStorage.get('settings').print_locale == 'EN_US'){
                        octopusDateStr = octopusDateTitle + ' ' + r.octopus_info.lastaddmethod + ' ' + 'on' + ' ' + r.octopus_info.lastaddvaluedate;
                    }else{
                        octopusDateStr = octopusDateTitle + ' ' + r.octopus_info.lastaddvaluedate + ' ' + octopusLastaddmethod + octopusDateEnd;
                    }
                }
                console.log(octopusDateStr,121212133);                         

                // if take away then pay&print            
                $offline.invoicePdf({
                    size: '80mm', // '80mm',
                    type: print_type, // 'receipt'/'receipt-dinein' or 'order',
                    images: {
                        // COMPANY_LOGO: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARMAAAA9CAYAAACOeI1KAAARr0lEQVR4nO2df2yd11nHP1xdWZblGc+YYEKwqlLsKEQhZF4oWcQytytpyVKart1WuvXHSrs2FFZK6VBAqCoVjDFlY/MoW7tCF7ZulCxAFEqpvBKiyGQhhDQExzKWMcEKJrKCZV1ZV1dX/PE9J/fc1++997z32im0z0ey4tz7nnPe95znec7zPOe8x2AYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhsH3vNk3YKweoyODyz4b3nv+TbgT4+2AGZO3IClGJOf+LfsPzKgYK40Zk7cQCSMyANwEvBvoAgrAPwGHgXEwg2KsLPk3+waMlSEwJN3AJ4BfAK5NXHYX8HHgYeD1q3VvxtsDMyZvIslwZAU8hbXAfuCDVEKbJOuBJ4ETyFtpGsvJGCEW5lxl0hQwjSxK6ersAb4C7IkoMgO8d3jv+enoRpa358kBHe73RTCD8nbFPJOrSKCEfcAG4DpgjftsHjgNnAKWMtaZB54Afi6yWJ4mxz54hn5gJ/A+4BqU3B0D9o+ODF4wg/L2w4zJ1SUP3As8igxJR+L7y8AB4Ndxs3wk24EHqR3aJJl3bTVDO/Ax4JdRyBS2uQ1oc9+Vlxc13sq0OjvVpd7s1KiOZsrGzIar0W6GsjuAz6LVlTS6gQeAbwOjdRur0AY8hMKcWM4gg9LMM90JfB4ZlTQ2ue9aysckiZU5aD3MytJWTHsx9bVSR62yzZRphcw5k+AG1wKb0Qz7g8gw/Q9adhwDLsDym048YLurpw/NcJeBKZwg+rI1OqXNlc+hsOBKaBDRuf3uvrvRDLoAzALTwFJa+aDsGmArUpp3Av8FHEcJzVKdtnPACFppqUcJuG147/nDDa7z97QF+Bugt9H1Qf33IQ/Isw55Gf2oP6eQwbkE1f05OjL4PHB/nfr/ErhjeO/5YuI+SdbViJRx70JGsxvoRJ5dCcnLJfdT5XHFtlejrV7300Vl4i24NuaQQb7ynHXkJo/6tsvdr5fXApK9Ulr5lHtqR8+ed2Uuu3rq6UqnK9Pu7nWBoI9W0qg0a0y2A19AcX9b4pISMiifBr4OlIf3nk8+5FpgF3ArsBEJSA659qdc3a9Q7Sq3o70TW4F3oWXPbipG6ATwJ8BEnUHtBR5Ds+s6KgLiB+YY8JvAeEKBcM95J3LhNyWeex54FngGKNRovwf4W6T89ZgFbhjee368wXX+vn4DeLrRtQFngfcjZdgC3IP2o/RT8TYKyJjsBw4SGMnRkcFPus9r8STwe41uItIDzCMjtw34Kff7WqQg7e77MlKSRTSBnQL+Gi19L2RoCzSpbQduQBPlOmQA2qiEc6HxmgC+Axxxv6cZhDZgHwpvO4P7XXL3N43k4iVgPsUotANDwM1I9tdRMQxTwJ+7smFY3I284J9BstoXlLnk+uhraBIsr5RBaSbMyQF7UWfXqnMjcoXngSOJPRB3Ibd8I8tj/A6U1NuKhPI55AnsBO5wn68hnRvddR8ZHRmcSDEGPchIfbjGPfehJdUC2otRCsp2oDzGr7A8z+Hr/lXgX5ABrcLV4ROujTiJhCSGTmQYYikDL6B+fwYJeF/KdR3A9cAfuf9/K/juIOqfjSnllpCx6nL1rkcTwA8jhfw34BAwmXZzCSOy3bUzjAxIPdpdm2uRjNyLwsSngbHRkcF6XgNIQe8GPoLGqZ5etKH+6XXPtxvlwEaAZ0dHBpOTST/KZ6X1M0iPdgM/ifRq0d1XDvhpV/cw0p0kA0ju3w08jvp/F5owt5Ieil6DjNNutN/oUJ1nzUSzxiTtwZL0oM4ZRQ+5Dfgt1DGN2u1Bwj6ILOzmyHvdgryH30757kH3XSMG0CCEg/qLwK+x3AsLaUOzx0ukJx8/QO1cSch3gWJkaHAtEv5YTiHP55tIWRvRDXwUCZx352eQZ/KHLO+PNuSRFpCC9qZc8yFk8CdTDD7IIDyBkrxZ8kAh7cAtaCzvQx5nFUH4sQdNFLUmxxiuBX4H+H4k48Xgux7SJ6CQHJLNr42ODL6G+u1xJLON+iCPws5/Bb4P+CXi5KwPGapXWaH8Vmz2P6SEBCqGjcgSPoAE+CbiDdgaNNsPZSgD8KPhf5zQXId2hMY87xzVwrADCXc9Q+JZdo1rfwB5PTHcjDyg7TghTMsZuc82E69w3sX9NHGGxNPB8n57GeXFkuTQmG9FRiGtz4bQWFwheL7NwDeAT9K8IQm5DnknVXW59jqBp4Dnac2QeNrQpDOcGK8izsttQDtaZt+AwvVPEd8HefQsnyLOkHh8yLgiNLs0/O+R1/Ug4b2RxtZ5pSimfLab5VvLa/EPVDyDHhTvxiY33yDwSoJcy2MZ2t/mfhZQ3P888OroyGBVos3xLuInhBzyCmOMYshplu978UnIZvFue1jHJhSCxSh2GeUofIK0HttQyHYEroxJF/IkHqSxDpSQR7eOxuFWJ3Abmu2bWRq/H006A02UzWJEPHNk24JQl2aNyTsjr+tEinw1eSPx/y6U6I3hEkr8eu5GcWsM80iIgKrZdo+rJytdqO9uRDmLfShECRViKEN9zYz1HPIoCdoFeTbbmqjP41dILrs616IcW6yHcBCFAfuQQahHG/KWjgT5r6fQqlqMIT6APNN9yGNqxIBrcyl4ttjZv4/auZXV4HWCEKeVLRfQXJjTTnryrRExrl6SBbJZ+IvAaCIjPkTjFRTPceCc+/1alPOJVcKDwOnEytVWlPtpxZXsQAnFzyTq2Uz2cSgQPw4llLA+mXimfhQ6JD2CC8DRyPoLVJQtjzy3HZH3NY3yEjPEj807gvzXJ4BHiJP9CTR+l4jLE/o2csGz3Up2b7BEds/mIm71KpIZ0ieKfpS/eYCMHlImYxJY2ixJP9Br70+RHoKkUUYrOXcQn58BzeDjUBVifJQ4ZS4Df4Vc+hxK3MV25jRKSoaJ0/VIGZPhzRQyUi+RLfG1C9gYzK4Pk821fQ0902zEtWXgReAPgHJi1eMLLM+5jAG3o+XGGJkaxW2aQ4b+3ogy/r6+ggx+F/HG9L/dv8Mo2Rqj3GW0mjWJwt1NkW2FObc9pK8e1uMMko8scn8ELQMfzFDmEG4LRDC+d6Jl9T9F/fwCGTylZjwTn1yL5RTqnEniZ5KjaNAnMpSZRfmFUtA5O4l78Q0kBMfd7xvQakIMXujOBZ8NuM+2prTxGPAlpNhPED+b+PdpcmhmjX0ukPJ+HHkPMTPsMdT/4b1dh1ZwkmHrUbRXZRI9UyOZehnlK0rIIDxJfE5qnMpmuw3EGfslNDb9rt3Ytk4HbW2MbAvgH9Gz3YK8ySwG/yJaYWm4YTFgHMnUGeJ1ZRHtTyknwnG/d8zXs4FVNCY5ZAFjb7qAErAzwHsi21tEW84vIVe+1r6SJK9RrdBDSHhiB3MKeRh5NKD9keUmqN5bsgVZ9GSuZR4NuheUJbTRLXY2Oevauh/F77Gu8zjaaDeDlmUb9UcR7ZmYCz7bgWarXYlrj6GVmQm0knF9g7oPo4llFvXz48Tn1MpolcPP2DcRZxinkEx9nvgck2/L98HNxHm382hCuh9NbLEy5Nvcj4zzRuKUuITGyiejY72nCSRPnltQ/yR1bYz4PU/xSTlnwfqJT0iCZsTDSICTs3S9Mv69lPcSrzR/jzrXb3x7muzhWLcre1eGMkepxO+7kAFbn7hmHoV5OSSkfmYYJ/4N4TPIGD1CvIFcRPmFs0jQYryZy2hWBgnXva7dpHCfQJsPJ5BiP0r9ycJPEnNIMR9BS+CxMniBygarHrRvJ4Y8UpRY+QNNKt7o9yFli6GEPLod1H53qRZjwFfd7++LLH8WeXqg8Y3ZFAmSu3kq+1s+g0LYkEm0Gzw6D5M1w78T7RuJoYDc4gKarWNd0hdcmW6yrRi8HwnpDWgwsyY9h9A7Lv0Zy25CG4V+AilrUtHnkID9CPLSvJDcjbyvWM/rY2RP5B1ECtiOQqqkwKTRjTyZ/0TJwyGWG4kTyCM5h+ThGRqHD+2u3htcnVmXqY9RmSV3Ej8LN7PMehwZFJDnFDsp+d3aWVlCIYZP9O6ILPdNFBqBJt7Y7Re9SLduR15U0sO7gCaHk5H1AdmMSSdKiMaGRkepHA04RJxLegp43SWF+og3XCALG7PDtRZ+CTEr11PbvZ9GCrSAXNhwtukkm9HKakjmkTEvolk5GaLUa+eROt8fR/kX/+7QHuLChzw6byX2zJUk59HMvw71adb+yMIMCjvWI6Va7aM6juH2wSDDFWO8Qk/Nv8ISyzCS2TQP9xLyRF+BbC8CZsmZXEP8DRepeBg59JJWI8rI9fcbmfzLXFebMhkOJ6rDCeDn0Vu0tQZuNRnDLVWjFaWVaP915JF4Q5JD74VcDW5FeZm0xHYMS7i3oCP4IApXX6C5CQbil+CLaOXEhxNbiBurQ1ReavVv38eSr9GGXyA4CNnfKM5iTPxGoxhOo4QoyCgkcwhpTCPF8yzQ+jsDsUvRIS+iZelW2nwRJTv96lCsEMdwkcbuZwkZZm8Ui7R2WFEZLbvfh0tyB4LWyqx9Gfgy7riKBmxBoUBs/iJkCnkz9xC3ND6AtqY3SiinUUDyc7bRhY5TVHQF4MciylxZjXH/z9O6pzaFthscoMk3ibMYkwJxyllCmWz/OnWexh6G3z8QvvzlXydvhhmUI4jdV+E5jJYq/4xsG4A8E2i14mFczO2e5wgpL5tlZAkZ29sJNhvV4DTVy4unyJCVTzCLVo8eovqZQOP23SbrnUD99Kj7mWiynnrMoWX4DyCjdcS1uRptgfr9IZS4jNWtvyA4eoC4CfsYbjOhY5Hmx7eEZOVDNOmReLIYk3EqM209vo42ZHkWaGylD6Bl0pACyjNkMQZLqENuB37f3ctnaWwEy67cXiSAY65c7HsLs8DnkNA+R+JwJ/f9Q2jQsnpLBbS6dR8Km44jZahVz2Xkos8F7U8jg5BlI9Qc8rBuBX7X1ZsmaAeQksZ6PrNIwW9DclJELvttwBeJ81LqUUL98zngZ5GhCrcMeIP8HJXkZSssIOV+DI3/AZSvOlevkOM01cc7APxzgzIX0epLKJslFP5l0ZUiCsUfRhs7T0JrhyVlcVEX0L6ARZRt9ic+gZR4Bu1F+CKwENxUCQl3N8GbsEj4ZpHC7wcupzyI37W5DyX50rLVZRRGjKEdmK9Q3dFfovKyXdrKyTTyip6lsiuziBToDHKNh9BypD/ZzR+mNIl2DB5CBrMMNQfkHDIGu1Eie8jdT9oYFFB/HkNHOB6j2lMaBf4YrfCEXt8MWlkJw0XPy+5Z70HjsA71p2+/6J5/Avg7ZCDOUOMUMP/Z6MjgLBqjDyPD40+wC2UjrPcVEn3lth2cQ6HICJIv/watf7ellhtfRGMxgzyw7yCDe8Vwphx1cBYp0QhKRr4HhTa9ri2/ORB3nyX3s4jGYQ55Am8ghTxL9fgUkXdScPWHRzH4fj6OjMJUom9fBH4cJcxDL2UJjcfTpB/p+RoyCo+inFIPlUOd6h0gdSUEb/WQpOiT1hInPw1QOemshKzlOMGsknJilM+drKFy1OIklSW4esfWdaOYeRPwQ66uItom7Y8YnCTl6MbgnYytaEbya/ELyEV/lcDtTbnvNrRc3E9FKBaQsE4RCFGGE73aUUJ7PerHH3D1LqEl2XGkXBdJ+ZOewZb6HcgofS/wH0jIzqTdS6L9XqSkve5eykghLyJFqcpVZXiuDrQvoweNkVfASySOdmhwWBFIoXtcff5oz24qBrCAlHIWyd0FMhzZmNJWN+qPbiqnq+HuuUDFkPhc3rJ8Xors5Km8bdyF5NAfEXrB1Z1m7DrQhs0Nrg8KSL5PknKUZqLNdiSr11A5rnGJyvjOksjhrdRJa5mObWz2YNzVPMS5ifaX/d3dyHKZ2qxF1uepVX+zhwWvxjOtdL3N9FHWNla7rWb7o5XDp1f6IOys/L/6I1yt/gW5/6t/ga7ZA5dbLWsYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYhmEYK8z/AjwfJb3qkHt9AAAAAElFTkSuQmCC',
                        COMPANY_LOGO: dataURL,
                        INVOICE_QR: invoice_no_qr,
                        SHOP_QR: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAYAAACtWK6eAAALxklEQVR4Xu2d23bkNgwE1///0ZNjZ+OMx7oANQBFSZVXExTZ6CJA2kk+Ho/H44//qIAKLCrwISA6QwXWFRAQ3aECGwoIiPZQAQHRAyrAFLCCMN2MuokCAnKTRLtNpoCAMN2MuokCAnKTRLtNpoCAMN2MuokCAnKTRLtNpoCAMN2MuokCaUA+Pj5OL83Wn5+N3h/9U7itddI5txI7WpcOkxFdBOQlE6ONQJL2uWQBySNEtBYQAQk5bfTBEVpUcpCABAWzxQoK9TRMQIKaXV2o0fsjp5otVtCsL8OI1rZYtlght40+OEKLSg4SkKBgtlhBoWyx/pRWEEJoPlWxiNGvPB1PpB16Ul1oXCxbdaOq1ykgdblZnYm2JwKST46ABDWrFir42cVhAvKOernY6rxbQXL6o9ECgmRDQQISlK1aqOBnrSDvCFUQW513K0hBUvamsILsKVT3cwEJalktVPCzVpB3hCqIrc77sApCT9EtzejvM+jrEBW/I47qQj04eg90f3Sda98TkIRjqPgdcdRAie3+GDp6D3R/dJ0C8qSAFSSPCTXe2TsHK0jCKx0moW0iPWET27WCfP57N9n/eHWHSWjSqLmsIHnF75p3AUl4pcMkFHIryLICNEfeQbyDJI6C30Op8byDBI13dqH23EX3RysIjaOVR0D2HPD353cVak8eAcm3PHuarv2cHg7k7ukdhGbpJU5ABORLAStIrRE6TsMrwDq6FfSSHrwr0YJyBVPOBKuAUCcGjU4rHV2WgNRWVgGhThSQLwXoaT/aePTgGL1OW6wgWJTbDiPQtVB4RsfNtD8BEZC2RxYKloA8KdBxwtLEkPfwvWR27G/vm2s/79CF3uk6dOnYnxXECmIFeTzSZ46/KExLNu61hi6t44S1ggSzcVeh9uTpaCX2vmmL9VsB6k9bLFssW6yZWyx6GtK46pPkcx10Thq3tXc65+g4mj8aR/d3eAWhG6Zx1UIJyPr/m7LjVXCWvA+7pNMN0zgByT8mdFzuaf5oXHXeBSSRCSo+jbPFSiTn79BqrQUkkQMqPo0TkERyBCQn1hVMKSC5nL9zT/SSXvDMS6GjcQJyMUDy2zkmouMySiG4QtwxWcx/lby2ld5B8ks+JkJAlnWnuhyTxfxXBSSoGTWCcUGBJx0mIMHEaHQrSNAqtf9t3uhHjx4nIAIS9aB3kBelrnBpHn0ARM129DhbrGAGRhvo6t8Lyn74sCGAHL7L5gXQf6+DiL/3i62trXZA1yztKadPt1in3GVi0QKSEOsGQwUkcQehJ/pWXAeQ9B51A7+ntyggApI2zZ0CBERA7uT39F4FREDSprlTgIAIyJ38nt5rGpDRl0p6Me64qDrncjboE3farYGA6hwJSED0/4ZUi7/3exBqvLOsMyF9eGj13gUkLD3/z/7QZ14BSSTn71ABedJs9G+Tq8W3guQB2IuozpEVZE/xp59Xiy8gCfGDQ6tzJCBB4e9u5mrjJWRPDa1ep4Ak5K8W/+7QJaQPD63OURqQ0RfOsDIHD6xOzB48dLsd9zb69E/3sBVHHzbW5hSQoiwJSJGQb04jIG8K2BUuIF3K5uYVkJxew0YLyDCpNz8kIHPk4dcqBGSOxAjIHHkQkCcFvKQHTdlxigY/ffiwjr13GM9XrJxV0q9YHUnLLfn/0TMlm5Z2qmfH3ukTPn12HX2okBwJyEt2Owx7deMJyAn7TXo6Cciy3Tt0oTmiB44VJAgybU/oSUkTSr832nhnWaeACMiXAgKyjKyACIiAfNT+76q9pHtJDz0iegcJycQH0bJP42jfT8owV2V85Ex6UuioaiS36QpCF0cTQ+MEJP8aRQz0zp1HQJrvBKMTSg+HmeJmOnAEREBmYuOQC3zHkzoVlRyotlgvahMRacKOiLOC5FQXEAH5VoAeDhQ6WyxbrNxxNWA0NXPHo4eAHAjIaPG3DERP5g5eOnQ5y/7IOi/bYnUYgRqWJIZ+ay+uQ5ez7I+sU0D2HFXwc5KYgs8uTiEgOWUFJKcXGi0gSDYUVH3HEhCUhlyQgOT0eme0gDypN9Mvobykv2PrulgBEZC33OQdJCdfusWiAtPTviMuJ1FsND25OuJiK/49qqMVpPvr+L0L0UVAiGoLMdQIHXF0SwLyWzkBoW56ieswOq2edEsCIiDUO7txArIsEdXFFutJASoijdt1OxhA19IRB5b/FWIFsYJQ7+zGdRjdFmtcVVpLsHeQXevHBgjIODNTrWOZ/DlqGCBbi5vppBwp/l7C6FpGx9H7wt7+q39OWkgBKXqNqk7m53yjjU6/JyDBCzU1iRWktj2hRqdxAiIgba881Fz0UOmIo3ugByqNs8UKKjfaJMFl/RpGT/TRcQJiBbGCPB6Ic/q3e+hjO0FWkKCqVpDaO48VJFhBCKFneq2hp2EHkHQtW2buyB/93ixtYukzb4fAo81FvzeTEYKF9NewjvzNpAvZn4C8ZFBA8njRaka17ohb27WACMi3AuSE3WuRrSBPCnQI3HFadPS3MxkhXwP+jejI30y6kP1ZQawgVpANigVEQARkdkBoS0Avh7Tsz/TeT1tPqnVHXMceSBu1mddHckbav3cILCDLqnbo0pE/AelQ9WnODiMkz4vv1XSshVa60WuhaRYQqlwwrsMIAhIUv2CYgBSIOLrvF5DmpD1NLyDNWltBvIO8KkAPuDWrTvHMSzkSEAGZDhBq5rPEdbzS0Tk74ujFn+aP7oG21odXECrUWeJmSihdC62s1eb6zDndg4BMSsxMCaVrEZA6c6XvIHWfnnMmasqOE4+uRUDqvCUgL1pSUwpI/sGAtnQdOSp7xapjc86ZOsSnc3bEeUnP+c4KYgX5VoCe6B3Vc/ScZRWE9rc5bntHz2SEjipBf0M9Oq6jmlE9BeRJAQFZtoOA/NYl3WJZQWovo/TEu0KcFaS3U8KzW0GsIFHzWEGiSu2Mu8KJbotli/WlgBXEChI9F60gUaWsIKsK0MpzuztIx8lM/UtbHvq9jnd7ugf6kNKRP7qHjjyQOUsrSIfAZFOfMTMlhq6lI67j1O44HGjeq+MEpFrRhfk6jD5TWyMgTwrQZA/w4Y9PzLROupaOOCtIzolWkJxeaHSH0a0gKBXpIAFJS5YPEJD8s3Je5Z4IAenRtaTd6wDLFiuX8GGA0KdHmtCZzNWxlo4WK2ed3tEd+yOvrALykmcKMk3o6LheW9fNTnWhB+panIAISJ2rC2cSkAIxqYg0jp5Otlj5ZI/OkRXkSYHR4guIgHwp0GG8mU7tmdYyWuu8xd+L6Nifl/SnnHSc2gLynukz0QKSUWtlLBXxLHEFEh0+xWit6SHmHaTgDjJTsg93fnABM2lmi9XcYs2U7KA/Dx82k2YCIiCHA/G6AAEpaF1oVkeLP/p7VJeZ4mbSzApiBZmJjbee/ilYXtILKhYVf3TcdG4HC5pJMytIQQXZ8gD9Q0Z6qtHf5QAff4XQ71FdKDx0fwIiINQ7ArKinH/Nm7AUPSmtIMsKWEGCJ3rCoz+GUoFJqd1rQc6yB9pCUq07DocOrdfmtIIk1LaCLItFdemAjgIpIMHXL3rCJjg7tArS/XWYuWNOAQkanb7IUAMJSF4BAclrVhZBAaFxgpVPnYDkNSuLoEancQKST52A5DUri6BGp3ECkk+dgOQ1K4ugRqdxApJPnYDkNSuLoEancQKST52A5DUri6BGp3ECkk+dgOQ1K4ugRqdxApJP3e0AyUt0TARNzFn+RIXCOpMuHVqTOUv/1OQYu+e/OpMR8qv/N4Ik+zOOVkga1wHryDkF5EXt0UYQkGUFRh8Aa3kQEAH5VmCmyiog9OgsiJvJCHQ7ow00urKO3p8V5EkBAcm3NQISPMro3/4Hpx8yTEAEJGq09B0kOrHjVOAKCgjIFbLoHtoUEJA2aZ34CgoIyBWy6B7aFBCQNmmd+AoKCMgVsuge2hQQkDZpnfgKCgjIFbLoHtoUEJA2aZ34CgoIyBWy6B7aFBCQNmmd+AoKCMgVsuge2hQQkDZpnfgKCvwDxO9kqG0xFmUAAAAASUVORK5CYII='
                    },
                    data: {
                        INVOICE_NO: r.invoice_no,
                        INVOICE_TITLE: $translate.instant('PRINT_RECEIPT'),
                        MEMBER_CLASS: member_class,
                        INVOICE_DATE: r.invoice_date,
                        INVOICE_LOG: $translate.instant('DUPLICATE'),
                        TICKET_NO: r.ticket_num,
                        TABLE_NUM: r.table_num == 0 ? '-' : r.table_num,
                        CUSTOMER_DETAIL: customer_detail,
                        DELIVERY_ADDRESS: '',
                        SALESMAN: $localStorage.get('user').name,
                        PRODUCT_ITEM: products,
                        CHARGE_ITEM: charges,
                        // PAYMENT_METHOD: pay_method,
                        PAYMENT_METHOD: r.payments && r.payments.length > 0 ? splitPayMethod : pay_method,
                        REMARKS: '',
                        INVOICE_TYPE: r.delivery_type == 'pick up' ? $translate.instant('PICK_UP') : (r.table_num == 0 ? $translate.instant('TAKE_AWAY') : $translate.instant('DINE_IN')),
                        ITEM_TOTAL: '$' + Number(r.item_total).toFixed(1),
                        GRAND_TOTAL: '$' + r.grand_total.toFixed(1),
                        CASH_RECEIVED: '$' + (Number(r.payed_amount) > 0 ? Number(r.payed_amount).toFixed(1) : '0.0'),
                        CHANGES: '$' + ((Number(r.payed_amount) - r.grand_total) > 0 ? (Number(r.payed_amount) - r.grand_total).toFixed(1) : '0.0'),
                        SHOP_NAME: $localStorage.get('user').name,
                        SHOP_TEL: $rootScope.warehouse.warehouse_tel,
                        SHOP_ADDRESS: address,
                        SHOP_EMAIL: '',
                        DEVICE_NO: isOctopus ? r.octopus_info.device_no : '',
                        OCTOPUS_NO: isOctopus ? r.octopus_info.cardid : '',
                        AMOUNT_DEDUCTED: '$-' + (isOctopus ? Number(r.octopus_info.price).toFixed(1) : ''),
                        REMAINING_VALUE: '$' + (isOctopus ? Number(r.octopus_info.remaining).toFixed(1) : ''),
                        DEDUCT_DATE: isOctopus ? octopusDateStr : '',
                        OCTOPUS_DATE: isOctopus ? $rootScope.parseOctopusDate(r.octopus_info.transactiondate) : ''
                    },
                    judge: {
                        OCTOPUS_DISPLAY: (isOctopus == null || !isOctopus) ? 'none' : ''
                    }

                }).then(function (res) {
                    console.log(res);
                    $rootScope.isShowPrintFooter = true;
                    $rootScope.historyIframeUrl = 'templates/tpl.invoice-pdf.80mm.print.html';
                    $scope.css = res.css;
                    $scope.body = res.body;

                    $rootScope.viewInvoiceC = {
                        iframe: null,
                        innerDoc: null,
                        css: res.css,
                        body: res.body,
                        back: function () {
                            $rootScope.historyIframeModal.hide();
                            $rootScope.historyIframeModal.remove();
                            $translate.use($rootScope.currentLang);
                            $rootScope.can_open_history = true;
                            $rootScope.showPrintPage = false;
                        },
                        print: function () {
                            // if ($rootScope.printBoxModal) {
                            //     $rootScope.printBoxModal.hide();
                            //     $rootScope.printBoxModal.remove();
                            // }

                            // 2017-5-22 - TK: for speed up printing
                            $rootScope.isShowPrintFooter = false;

                            $rootScope.t0 = performance.now();
                            $printer.epsonPrintCanvas();
                            // $rootScope.viewInvoiceC.iframe.contentWindow.print($localStorage.get('settings').epson_ip_address, $localStorage.get('settings').epson_port, $localStorage.get('settings').epson_device_id);                        
                        }
                    };

                    // 2017-5-22 - TK: for speed up printing
                    $printer.epsonPrepareCanvas(res.css, res.body);

                    if ($rootScope.historyIframeModal) {
                        $rootScope.historyIframeModal.remove();
                    }
                    $ionicModal.fromTemplateUrl('templates/modal.iframe.order.html', {
                        scope: $rootScope,
                        animation: 'none',
                        hardwareBackButtonClose: false
                    }).then(function (modal) {
                        $rootScope.historyIframeModal = modal;
                        if (!modal.isShown()) {
                            $helper.showLoading(1000);
                            modal.show().then(function () {
                                $timeout(function () {
                                    $rootScope.viewInvoiceC.iframe = document.getElementById('history-iframe-printer');
                                    $rootScope.viewInvoiceC.innerDoc = $rootScope.viewInvoiceC.iframe.contentDocument || $rootScope.viewInvoiceC.iframe.contentWindow.document;
                                    $rootScope.viewInvoiceC.innerDoc.getElementById('css-wrapper').innerHTML = $rootScope.viewInvoiceC.css;
                                    $rootScope.viewInvoiceC.innerDoc.getElementById('html-wrapper').innerHTML = $rootScope.viewInvoiceC.body;
                                }, 500);
                            });
                        }
                    });
                }).catch(function (err) {
                    $rootScope.can_open_history = true;
                    $helper.hideLoading();
                    alert('fail :' + err);
                });
            };

        };

        $rootScope.yyyymmdd = function (dateIn) {
            var yyyy = dateIn.getFullYear();
            var mm = '00' + (dateIn.getMonth() + 1).toString(); // getMonth() is zero-based
            mm = mm.slice(-2);
            var dd = '00' + dateIn.getDate().toString();
            dd = dd.slice(-2);
            var hh = '00' + dateIn.getHours().toString();
            hh = hh.slice(-2);
            var min = '00' + dateIn.getMinutes().toString();
            min = min.slice(-2);
            var ss = '00' + dateIn.getSeconds().toString();
            ss = ss.slice(-2);
            return String(yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + ss); // Leading zeros for mm and dd
        };

        // print invoice
        $rootScope.printHistory = function () {

            if ($rootScope.platform == 'web') {
                window.open($scope.pdfUrl, '_system', 'location=yes');
            } else {
                $api.epsonPrint($rootScope.printInvoiceId);
            }
        };


        $rootScope.reportOperation = {
            dayEndReport: function () {

                var dateObj = new Date();
                month = '' + (dateObj.getMonth() + 1),
                    day = '' + dateObj.getDate(),
                    year = dateObj.getFullYear();
                if (month.length < 2) month = '0' + month;
                if (day.length < 2) day = '0' + day;
                console.log([year, month, day].join('-'));
                $rootScope.reportDate = [year, month, day].join('-');

                $rootScope.iframeUrl = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fdaily-sales-report-pos&warehouse_id=' + $localStorage.get('user').warehouse_id + '&date=' + $rootScope.reportDate;
                console.log($rootScope.iframeUrl);
                // if ($rootScope.platform == 'web') {
                $rootScope.iframeUrl = $sce.trustAsResourceUrl($rootScope.iframeUrl);
                // }
                $rootScope.viewInvoiceC = {
                    iframe: null,
                    innerDoc: null,
                    back: function () {
                        $rootScope.iframeModal.hide();
                        $rootScope.iframeModal.remove();
                    }
                };
                // var htmlTemplate = $rootScope.platform == 'web' ? 'templates/modal.iframe.full.html' : 'templates/common.webview.report.html';
                var htmlTemplate = 'templates/modal.iframe.full.html';
                $ionicModal.fromTemplateUrl(htmlTemplate, {
                    scope: $rootScope,
                    animation: 'none'
                }).then(function (modal) {
                    $rootScope.iframeModal = modal;
                    modal.show().then(function () {

                    });
                });
            },
            popUpDate: function () {
                var isEn = $localStorage.get('settings').locale == 'EN_US';
                var ipObj1 = {

                    callback: function (val) { //Mandatory
                        var dateObj = new Date(val);
                        console.log('Return value from the datepicker popup is : ' + val, dateObj);
                        month = '' + (dateObj.getMonth() + 1),
                            day = '' + dateObj.getDate(),
                            year = dateObj.getFullYear();
                        if (month.length < 2) month = '0' + month;
                        if (day.length < 2) day = '0' + day;
                        console.log([year, month, day].join('-'));
                        if ($rootScope.reportDate != [year, month, day].join('-')) {
                            $rootScope.reportDate = [year, month, day].join('-');
                            $rootScope.iframeUrl = SERVER.http + 'cms.' + $localStorage.get('activate').prefix + $localStorage.get('activate').path + SERVER.subdomain + SERVER.apiPath + 'report%2Fdaily-sales-report-pos&warehouse_id=' + $localStorage.get('user').warehouse_id + '&date=' + $rootScope.reportDate;
                            console.log($rootScope.iframeUrl);
                            $rootScope.iframeUrl = $sce.trustAsResourceUrl($rootScope.iframeUrl);
                        }
                    },

                    currentYear: new Date().getFullYear(),
                    inputDate: new Date(),
                    mondayFirst: true,
                    closeOnSelect: false,
                    templateType: 'popup',
                    weeksList: isEn ? ["S", "M", "T", "W", "T", "F", "S"] : ["日", "一", "二", "三", "四", "五", "六"],
                    monthsList: isEn ? ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"] : ["一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"],
                    setLabel: $filter('translate')('CONFIRM'),
                    todayLabel: $filter('translate')('TODAY'),
                    closeLabel: $filter('translate')('CANCEL'),
                    from: new Date().setMonth(new Date().getMonth() - 12),
                    to: new Date()
                };
                ionicDatePicker.openDatePicker(ipObj1);
            },
            printReport: function () {
                if ($rootScope.platform == 'web') {
                    window.open($rootScope.pdfUrl, '_system', 'location=yes');
                } else {
                    $api.printDailyReport($localStorage.get('user').warehouse_id, $rootScope.reportDate);
                }
            }
        };







        $rootScope.tableOperation = {
            getTableInfo: function(){
                $api.getTableInfo({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                warehouse_id: $localStorage.get('user').warehouse_id,
                table_total: $rootScope.tableNum                  
            }).then(function(res){
                console.log('table info~~~');
                console.log(angular.toJson(res));
                    if(res.status == 'Y'){
                        $rootScope.tableOperation.showOrHide(true);
                    }else{
                        $helper.toast(err,'short','top');
                    }
                }).catch(function(err){
                    $helper.toast(err,'short','top');
                });
            },
            showOrHide: function (isShow) {
                $rootScope.showTable = isShow;
            },
            generateTableData: function (num) {
                $rootScope.tableData = $localStorage.get('table_data');
                console.log(num, $rootScope.tableData);
                console.log(!$rootScope.tableData);
                console.log($rootScope.tableData==null);
                console.log($rootScope.tableData==undefined);
                
                if (!$rootScope.tableData) {
                    $rootScope.tableData = [];
                    for (var i = 1; i < Number(num) + 1; i++) {
                        console.log(i);
                        $rootScope.tableData.push({
                            id: i,
                            name: "Table " + i,
                            invoice: [],
                            share: [{
                                id: -1,
                                name: i,
                                invoice_no: ''
                            }, {
                                id: 0,
                                name: i + 'A',
                                invoice_no: ''
                            }, {
                                id: 1,
                                name: i + 'B',
                                invoice_no: ''
                            }, {
                                id: 2,
                                name: i + 'C',
                                invoice_no: ''
                            }, {
                                id: 3,
                                name: i + 'D',
                                invoice_no: ''
                            }, {
                                id: 4,
                                name: i + 'E',
                                invoice_no: ''
                            }, {
                                id: 5,
                                name: i + 'F',
                                invoice_no: ''
                            }, {
                                id: 6,
                                name: i + 'G',
                                invoice_no: ''
                            }, {
                                id: 7,
                                name: i + 'H',
                                invoice_no: ''
                            }, {
                                id: 8,
                                name: i + 'I',
                                invoice_no: ''
                            }, {
                                id: 9,
                                name: i + 'J',
                                invoice_no: ''
                            }, {
                                id: 10,
                                name: i + 'K',
                                invoice_no: ''
                            }, {
                                id: 11,
                                name: i + 'L',
                                invoice_no: ''
                            }, {
                                id: 12,
                                name: i + 'M',
                                invoice_no: ''
                            }, {
                                id: 13,
                                name: i + 'N',
                                invoice_no: ''
                            }, {
                                id: 14,
                                name: i + 'O',
                                invoice_no: ''
                            }, {
                                id: 15,
                                name: i + 'P',
                                invoice_no: ''
                            }, {
                                id: 16,
                                name: i + 'Q',
                                invoice_no: ''
                            }, {
                                id: 17,
                                name: i + 'R',
                                invoice_no: ''
                            }, {
                                id: 18,
                                name: i + 'S',
                                invoice_no: ''
                            }, {
                                id: 19,
                                name: i + 'T',
                                invoice_no: ''
                            }, {
                                id: 20,
                                name: i + 'U',
                                invoice_no: ''
                            }, {
                                id: 21,
                                name: i + 'V',
                                invoice_no: ''
                            }, {
                                id: 22,
                                name: i + 'W',
                                invoice_no: ''
                            }, {
                                id: 23,
                                name: i + 'X',
                                invoice_no: ''
                            }, {
                                id: 24,
                                name: i + 'Y',
                                invoice_no: ''
                            }, {
                                id: 25,
                                name: i + 'Z',
                                invoice_no: ''
                            }]
                        });
                    }
                    $localStorage.set('table_data', $rootScope.tableData);
                    $rootScope.tableNum = $rootScope.tableData.length;
                }

            },
            chooseTable: function (num) {
                try {
                    if (!$helper.isNumber(num)) {
                        num = num.substring(0, num.length - 1);
                    }
                } catch (err) {
                    console.log('filter table num err :' + err);
                }

                console.log('set table index:' + num);
                if ($rootScope.currentInvoiceId == null) {
                    // $helper.toast($translate.instant('THIS_CART_IS_EMPTY'), 'short', 'bottom');
                    $rootScope.newCart(num);
                } else if ($rootScope.tableData && $rootScope.tableData.length > num) {
                    $rootScope.tableData = $localStorage.get('table_data');
                    for (var i = 0; i < $rootScope.tableData[num].invoice; i++) {
                        if ($rootScope.tableData[num].invoice[i] == $rootScope.currentInvoiceId) {
                            $rootScope.showTable = false;
                            return;
                        }
                    }
                    if ($rootScope.currentTable && $rootScope.currentTable.id != 0) {
                        $rootScope.tableOperation.removeInvoice($rootScope.currentTable.id, $rootScope.currentInvoiceId);
                    }
                    $rootScope.tableData[num].invoice.push($rootScope.currentInvoiceId);
                    for (var i = 0; i < $rootScope.tableData[num].share.length; i++) {
                        if ($rootScope.tableData[num].share[i].invoice_no == '') {
                            $rootScope.tableData[num].share[i].invoice_no = $rootScope.currentInvoiceId;
                            break;
                        }
                    }

                    $localStorage.set('table_data', $rootScope.tableData);
                    $rootScope.currentTable = $rootScope.tableData[num];
                    $rootScope.currentOrderType = $rootScope.chooseOrderTypeList[0];
                    console.log($rootScope.currentTable);
                    $rootScope.setTableNum($rootScope.currentTable.id);
                }
                $timeout(function() {
                    $rootScope.showTable = false;
                },300);
            },
            removeInvoice: function (tableId, invoiceId) {

                try {
                    if (!$helper.isNumber(tableId)) {
                        tableId = tableId.substring(0, tableId.length - 1);
                    }
                } catch (err) {
                    console.log('filter table num err :' + err);
                }

                var tempTableList = $localStorage.get('table_data');
                for (var i = 0; i < tempTableList.length; i++) {
                    if (tempTableList[i].id == tableId) {
                        for (var k = 0; k < tempTableList[i].share.length; k++) {
                            if (tempTableList[i].share[k].invoice_no == invoiceId) {
                                tempTableList[i].share[k].invoice_no = '';
                            }
                        }
                        for (var j = 0; j < tempTableList[i].invoice.length; j++) {
                            if (tempTableList[i].invoice[j] == invoiceId) {
                                tempTableList[i].invoice.splice(j, 1);
                                $localStorage.set('table_data', tempTableList);
                                $rootScope.currentTable = { id: 0 };
                                $rootScope.tableData = $localStorage.get('table_data');
                                break;
                            }
                        }
                        break;
                    }
                }
            },
            getTableById: function (tableId) {
                if ($localStorage.get('table_data') == null) {
                    $rootScope.tableOperation.generateTableData(50);
                    return { id: 0 };
                }
                $rootScope.tableData = $localStorage.get('table_data');
                for (var i = 0; i < $rootScope.tableData.length; i++) {
                    if ($rootScope.tableData[i].id == tableId) {
                        return $rootScope.tableData[i];
                    }
                }
            },
            changeTableNum: function (num) {
                console.log('change table num: ' + num);
                // $rootScope.tableData = [];
                // for (var i = 1; i < Number(num) + 1; i++) {
                //     console.log('~');
                //     $rootScope.tableData.push({ id: i, name: "Table " + i, invoice: [] });
                // }
                // console.log('length after push : ' + $rootScope.tableData.length);
                // $rootScope.tableNum = $rootScope.tableData.length;
                // $localStorage.set('table_data', $rootScope.tableData);
                $localStorage.clear('table_data');
                $rootScope.tableOperation.generateTableData(num);
                $rootScope.hideTableNumModal();
            },
            popSelectTable: function () {
                $ionicModal.fromTemplateUrl('templates/modal.select-table-num.html', {
                    scope: $rootScope,
                    animation: 'slide-in-up'
                }).then(function (modal) {
                    $rootScope.tempTableNum = $rootScope.tableData.length;
                    $rootScope.hideTableNumModal = function () {
                        $rootScope.tableModal.hide();
                        $rootScope.tableModal.remove();
                    };
                    $rootScope.changeTableTemp = function (num) {
                        console.log(num);
                        $rootScope.tempTableNum = num;
                    };
                    $rootScope.tableModal = modal;
                    modal.show();
                });
            }
        };
        $rootScope.tableOperation.generateTableData(50);

        $rootScope.dineIn = function () {
            console.log('....');
            $rootScope.isDineIn = !$rootScope.isDineIn;
        };

        $rootScope.showHideSearchBar = function () {
            $rootScope.showSearchBar = !$rootScope.showSearchBar;
        }

        //  ******************************  Cashier  ************************************************************
        $rootScope.moneyPaper = [
            { id: 1, value: 1000, amount: 0 },
            { id: 1, value: 500, amount: 0 },
            { id: 1, value: 100, amount: 0 },
            { id: 1, value: 50, amount: 0 },
            { id: 1, value: 20, amount: 0 },
            { id: 1, value: 10, amount: 0 },
            { id: 1, value: 5, amount: 0 },
            { id: 1, value: 2, amount: 0 },
            { id: 1, value: 1, amount: 0 },
            { id: 1, value: 0.5, amount: 0 },
            { id: 1, value: 0.2, amount: 0 },
            { id: 1, value: 0.1, amount: 0 }
        ];
        $rootScope.calculateList = [
            [{ name: '1', value: 1 }, { name: '2', value: 2 }, { name: '3', value: 3 }],
            [{ name: '4', value: 4 }, { name: '5', value: 5 }, { name: '6', value: 6 }],
            [{ name: '7', value: 7 }, { name: '8', value: 8 }, { name: '9', value: 9 }],
            [{ name: '0', value: 0 }, { name: '00', value: '00' }, { name: 'X', value: 'X' }]
        ];
        $rootScope.cashierOperation = {

            // Cashier 页面跳转
            getCashierList : function(){
                $api.getAllCashier({
                    token: $localStorage.get('settings').token,
                    locale: $localStorage.get('settings').locale,
                }).then(function (res) {
                    if (res.status == 'Y') {
                        console.log(res);
                        $rootScope.cashierList = res.data;
                        if(!res.data || res.data.length<1) return;
                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.toast(err, 'long', 'bottom');
                });                
            },
            pop: function () {
                $rootScope.currentMoneyIndex = 0;
                $rootScope.checkMoneyTotal = 0;
                $rootScope.currentLogin = $localStorage.get('user');
                console.log($rootScope.currentLogin);
                if ($rootScope.tempCalculateString == null) $rootScope.tempCalculateString = '0';
                $ionicModal.fromTemplateUrl('templates/modal.cashier-management.html', {
                    scope: $rootScope,
                    backdropClickToClose: false,
                    animation: 'none'
                }).then(function (modal) {
                    $rootScope.cashierModal = modal;
                    $rootScope.cashierState = 'list';
                    modal.show();
                });
                $rootScope.cashierOperation.getCashierList();
            },
            hide: function () {
                if ($rootScope.cashierState != 'list') {
                    $helper.popConfirm($translate.instant('REMIND'), $translate.instant('CLOSE_CASHIER_HINT'), function (res) {
                        if (res) {
                            $rootScope.cashierModal.hide();
                            $rootScope.cashierModal.remove();
                            $helper.clearUpCamera();
                        }
                    });
                } else {
                    $rootScope.cashierModal.hide();
                    $rootScope.cashierModal.remove();
                    $helper.clearUpCamera();
                }

            },
            toDetail: function (cashier) {
                $rootScope.cashierOperation.clearCalculate();
                $rootScope.currentCashier = cashier;
                if (cashier.remarks == null) $rootScope.currentCashier.remarks = '';
                $rootScope.cashierState = 'detail';
            },
            toCheck: function (cashier) {
                $rootScope.cashierOperation.clearCalculate();
                $rootScope.currentCashier = cashier;
                $rootScope.currentCashier.balance = Number($rootScope.currentCashier.balance);
                if (cashier.remarks == null) $rootScope.currentCashier.remarks = '';
                $rootScope.cashierState = cashier.status == 'CLOSE' ? 'check-in' : 'check-out';
            },
            processCheck: function () {
                $rootScope.isPositive = true;
                $rootScope.cashierOperation.setCashierHistory(Number($rootScope.checkMoneyTotal), $rootScope.cashierState == 'check-in' ? 'OPEN' : 'CLOSE');
                // $rootScope.cashierOperation.back();
                $rootScope.cashierState = 'detail';
            },
            back: function () {
                $rootScope.cashierOperation.clearCalculate();
                switch ($rootScope.cashierState) {
                    case 'check-in':
                    case 'check-out':
                        $rootScope.cashierState = 'list';
                        break;
                    case 'detail':
                        $rootScope.cashierState = 'list';
                        break;
                    default:
                        break;
                }
            },
            //  2017-6-19  Jaydon: choose Cashier
            popChooseCashier: function(){
                $ionicModal.fromTemplateUrl('templates/modal.choose-cashier.html', {
                    scope: $rootScope,
                    // backdropClickToClose: false,
                    animation: 'none'
                }).then(function (modal) {
                    $rootScope.chooseCashierModal = modal;
                    modal.show();
                });
                $rootScope.cashierOperation.getCashierList();                
            },   
            chooseCashier: function(cashierIndex,cashier){
                if($rootScope.currentChooseCashier == null){
                    $rootScope.currentChooseCashier = {};
                }                
                if(cashier.id == $rootScope.currentChooseCashier.id) return;
                $rootScope.currentChooseCashier = $rootScope.cashierList[cashierIndex];
                $localStorage.set('choose_current_cashier',$rootScope.currentChooseCashier);
                console.log($rootScope.currentChooseCashier);
            },         


            // Cashier API
            setCashierHistory: function (balance, status) {
                // OPEN, CLOSE, CASH_IN, CASH_OUT
                $api.setCashierHistory({
                    token: $localStorage.get('settings').token,
                    locale: $localStorage.get('settings').locale,
                    cashier_id: $rootScope.currentCashier.id,
                    warehouse_id: $localStorage.get('user').warehouse_id,
                    balance: $rootScope.isPositive ? balance : -balance,
                    status: status,
                    remarks: $rootScope.currentCashier.remarks == null ? '' : $rootScope.currentCashier.remarks
                }).then(function (res) {
                    if (res.status == 'Y') {
                        $rootScope.currentCashier = res.data;
                        if ($rootScope.currentCashier) {
                            console.log('into setcashier');
                            console.log($rootScope.currentCashier.id + ' , ' + angular.toJson($rootScope.cashierList));
                            for (var i = 0; i < $rootScope.cashierList.length; i++) {
                                if ($rootScope.cashierList[i].id == $rootScope.currentCashier.id) {
                                    console.log('oh yeah~~');
                                    $rootScope.cashierList[i] = res.data;
                                    break;
                                }
                            }
                        }

                        $rootScope.cashierOperation.upload_cashier_image(res.cashier_history_id);


                    } else {
                        $helper.toast(res.msg, 'short', 'bottom');
                    }
                }).catch(function (err) {
                    $helper.toast(err, 'long', 'bottom');
                });
            },

            // Cashier 功能方法
            popSignature: function (operation, isPositive) {
                $rootScope.cashierApiKey = operation;
                $rootScope.isPositive = isPositive;
                $rootScope.signatureOperation.drawCanvas();
            },
            clickCalculate: function (clickValue) {
                console.log(clickValue);
                switch (clickValue) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                    case 7:
                    case 8:
                    case 9:
                    case 0:
                        if ($rootScope.tempCalculateString == '0') $rootScope.tempCalculateString = '';
                        console.log($rootScope.tempCalculateString);
                        $rootScope.tempCalculateString = $rootScope.tempCalculateString.concat(clickValue.toString());
                        break;
                    case '00':
                        if ($rootScope.tempCalculateString == '0') {
                            $rootScope.tempCalculateString = '';
                            clickValue = 0;
                        }
                        $rootScope.tempCalculateString = $rootScope.tempCalculateString.concat(clickValue);
                        break;
                    case 'X':
                        $rootScope.tempCalculateString = $rootScope.tempCalculateString.substring(0, $rootScope.tempCalculateString.length - 1);
                        if ($rootScope.tempCalculateString == null || $rootScope.tempCalculateString == '') $rootScope.tempCalculateString = '0';
                        break;
                    default:
                        break;
                }
                if ($rootScope.tempCalculateString != null) {
                    $rootScope.moneyPaper[$rootScope.currentMoneyIndex].amount = Number($rootScope.tempCalculateString);
                    $rootScope.checkMoneyTotal = 0;
                    for (var i = 0; i < $rootScope.moneyPaper.length; i++) {
                        if ($rootScope.moneyPaper[i].amount != 0) {
                            $rootScope.checkMoneyTotal += $rootScope.moneyPaper[i].amount * $rootScope.moneyPaper[i].value;
                        }
                    }
                }
            },
            chooseMoneyPaper: function (index) {
                $rootScope.tempCalculateString = $rootScope.moneyPaper[index].amount.toString();
                $rootScope.currentMoneyIndex = index;
            },

            clearCalculate: function () {
                $rootScope.tempCalculateString = '0';
                $rootScope.checkMoneyTotal = 0;
                $rootScope.currentMoneyIndex = 0;
                $rootScope.cameraImgURL = '';
                if ($rootScope.currentCashier) {
                    $rootScope.currentCashier.remarks = '';
                }

                for (var i = 0; i < $rootScope.moneyPaper.length; i++) {
                    $rootScope.moneyPaper[i].amount = 0;
                }
            },

            camera: function () {
                navigator.camera.getPicture(function (imgData) {
                    console.log('success');
                    console.log(imgData);
                    // var image = document.getElementById('camera-image');
                    // image.src = imgData.slice(8);
                    // console.log(image.src);

                    console.log('------------------');


                    var canvas = document.createElement('CANVAS');
                    console.log('2');
                    var ctx = canvas.getContext('2d');
                    console.log('2.2');
                    var img = new Image;
                    img.crossOrigin = 'Anonymous';
                    img.src = imgData;
                    console.log('2.3');
                    console.log(imgData);
                    console.log(angular.toJson(img));
                    img.onload = function () {
                        canvas.height = img.height;
                        canvas.width = img.width;
                        console.log('~1');
                        ctx.drawImage(img, 0, 0);
                        console.log('~2');
                        $rootScope.cameraImgData = canvas.toDataURL('image/png');
                        console.log('~3');
                        console.log($rootScope.cameraImgData);
                        $rootScope.cameraImgURL = imgData;
                    }
                }, function (err) {
                    console.log('fail camera : ' + err);
                }, {
                        quality: 70,
                        targetWidth: 320,
                        targetHeight: 320,
                        saveToPhotoAlbum: false //true
                    });

                // $helper.camera(1).then(function(imgData){
                //     console.log('success');
                //     console.log(imgData);
                //     $rootScope.cameraImage = imgData;
                //     var image = document.getElementById('camera-image');
                //     image.src = imgData.substring(6,imgData.length); 
                //     console.log(image.src);                 
                // },function(err){
                //     console.log('fail camera : ' + err);
                // }).catch(function(err){
                //     console.log('catch err camera : ' + err);
                // });          
            },

            upload_cashier_image: function (historyId) {
                console.log('hisotryInfo:');
                console.log(historyId);
                if ($rootScope.signaturepng != null) {
                    $api.uploadCameraMedia($rootScope.signaturepng, {
                        token: $localStorage.get('settings').token,
                        locale: $localStorage.get('settings').locale,
                        cashier_history_id: historyId,
                        type: 'photo'
                    });
                }
                if ($rootScope.cameraImgData != null) {
                    $api.uploadCameraMedia($rootScope.cameraImgData, {
                        token: $localStorage.get('settings').token,
                        locale: $localStorage.get('settings').locale,
                        cashier_history_id: historyId,
                        type: 'photo'
                    });
                }
                $timeout(function () {
                    $rootScope.cashierOperation.clearCalculate();
                }, 500);

            },

            showCashierImage: {
                pop: function (photoUrl, remarks) {
                    console.log(photoUrl);
                    $rootScope.imageUrl = photoUrl;
                    $rootScope.currentRemark = remarks;
                    $rootScope.showCashierImage = true;
                },
                hide: function () {
                    $rootScope.showCashierImage = false;
                    $rootScope.imageUrl = '';
                    $rootScope.currentRemark = '';
                }

            },

            changeRemark: function (str) {
                $rootScope.currentCashier.remarks = str;
            }


        };



        $rootScope.signatureOperation = {
            canvasback: function () {
                console.log('back');
                $rootScope.drawingCanvasModal.hide();
                $rootScope.drawingCanvasModal.remove();
            },
            drawCanvas: function () {
                if ($rootScope.drawingCanvasModal != null) {
                    $rootScope.drawingCanvasModal.remove();
                }
                $ionicModal.fromTemplateUrl('templates/modal.drawing-canvas.html', {
                    scope: $rootScope,
                    animation: 'slide-in-up',
                    backdropClickToClose: false
                }).then(function (modal) {
                    $rootScope.drawingCanvasModal = modal;
                    modal.show();
                    var canvas = document.getElementById('signatureCanvas');
                    $rootScope.signaturePad = new SignaturePad(canvas);

                    $rootScope.clearCanvas = function () {
                        signaturePad.clear();
                    }

                    $rootScope.saveCanvas = function () {
                        var sigImg = signaturePad.toDataURL();
                        $rootScope.signature = sigImg;
                    }
                });

            },
            next: function () {

                $rootScope.signaturepng = $rootScope.signaturePad.toDataURL('image/png');

                console.log('next');
                //上传签名

                // HEAD
                //$rootScope.cashierOperation.setCashierHistory(Number($rootScope.tempCalculateString), $rootScope.cashierApiKey);

                // $api.uploadCameraMedia($rootScope.signaturepng, {
                //     token: $localStorage.get('settings').token,
                //     locale: $localStorage.get('settings').locale,
                //     cashier_history_id: 1,
                //     type: 'photo',
                // }).then(function(res) {
                //     $rootScope.cashierOperation.setCashierHistory(Number($rootScope.tempCalculateString),$rootScope.cashierApiKey);
                // }).catch(function(err) {
                //     $helper.toast(err, 'long', 'bottom');
                // });
                // //上传图片
                // $api.uploadMedia_Camera($rootScope.signaturepng, {
                //     token: $localStorage.get('settings').token,
                //     locale: $localStorage.get('settings').locale,
                //     cashier_history_id: 1,
                //     type: 'photo',
                // }).then(function(res) {

                // }).catch(function(err) {
                //     $helper.toast(err, 'long', 'bottom');
                // });            
                $rootScope.cashierOperation.setCashierHistory(Number($rootScope.checkMoneyTotal), $rootScope.cashierApiKey);

                $rootScope.drawingCanvasModal.hide();
                $rootScope.drawingCanvasModal.remove();

            }
        };


        /**
         *        2017-7-8  Jaydon: Socket connect (offline device connect)  
         **/
        $rootScope.startServer = function () {
            $offline.startServer(8888);
        };

        $rootScope.connectServer = function () {
            networkinterface.getWiFiIPAddress(function (ip) {
                alert(ip);
                // $offline.connectToServer(ip);
            });

        };



        //  ******************************    生命周期  ******************************

        $scope.$on('$ionicView.loaded', function () {
            $rootScope.isDineIn = true;
        });
        $scope.$on('$ionicView.enter', function () {
            // $timeout($scope.init,1000);
            $scope.init();

        });

        $rootScope.userOperation = {
            // user 页面跳转
            pop: function () {
                $rootScope.userSwitchStatus = 0;
                $rootScope.userSwitchPassword = '';
                $rootScope.loginAccounts = $localStorage.get('login_account');
                console.log(223311);
                $ionicModal.fromTemplateUrl('templates/modal.users-switch.html', {
                    scope: $rootScope,
                    animation: 'none'
                }).then(function (modal) {
                    $rootScope.userModal = modal;
                    modal.show();
                })
            }
        };

        $rootScope.changeSwitchStatus = function (status, account) {
            if (account == $rootScope.user_name) {
                if ($rootScope.userModal) {
                    $rootScope.userModal.hide();
                    $rootScope.userModal.remove();
                }
                return;
            }
            $rootScope.userSwitchStatus = status;
            if (status == 1) {
                $rootScope.currentSwitchAccount = account;
            } else if (status == 0) {
                $rootScope.userSwitchPassword = '';

            }
        };

        $rootScope.switchLogin = function (login, password) {
            console.log(login);
            console.log(password);
            if (!password || password.length < 5) {
                $helper.toast($translate.instant('INVALID_PASSWORD'), 'short', 'bottom');
                return;
            }
            $ionicLoading.show();
            var user = $localStorage.get('user');
            $api.login({
                token: $localStorage.get('settings').token,
                locale: $localStorage.get('settings').locale,
                login: login,
                password: password,
                device_type: $localStorage.get('settings').device_type,
                device_token: $localStorage.get('settings').device_token
            }).then(function (res) {
                if (res.status == 'Y') {
                    // save returned user info
                    console.log('login success');
                    console.log(res);
                    user.login = login;
                    user.password = password;
                    user.level = res.user_level;
                    user.shop_id = res.shop_id;
                    user.isLogin = true;
                    user.name = res.name;
                    user.invoice_prefix = res.invoice_prefix;
                    $localStorage.set('user', user);
                    console.log($localStorage.get('user'));
                    $rootScope.user_name = login;

                    // save returned warehouse list
                    var warehouse = [];
                    console.log('generate warehouse');
                    for (var i = 0; i < res.options.length; i++) {
                        warehouse.push({
                            id: res.options[i].warehouse_id,
                            code: res.options[i].warehouse_code,
                            name: res.options[i].warehouse_name,
                            warehouse_address_EN_US: res.options[i].warehouse_address_EN_US,
                            warehouse_address_ZH_HK: res.options[i].warehouse_address_ZH_HK,
                            warehouse_address_ZH_CN: res.options[i].warehouse_address_ZH_CN,
                            warehouse_tel: res.options[i].warehouse_tel
                        });
                    }
                    console.log('set warehouse');
                    $localStorage.set('warehouse', warehouse);
                    var inventory_warehouse = [];
                    for (var i = 0; i < res.warehouses.length; i++) {
                        inventory_warehouse.push({
                            id: res.warehouses[i].warehouse_id,
                            code: res.warehouses[i].warehouse_code,
                            name: res.warehouses[i].warehouse_name
                        });
                    }
                    $localStorage.set('inventory_warehouse', inventory_warehouse);

                    $ionicLoading.hide();

                    if ($rootScope.userModal) {
                        $rootScope.userModal.hide();
                        $rootScope.userModal.remove();
                    }
                } else {
                    $ionicLoading.hide();
                    $helper.toast(res.msg, 'short', 'bottom');
                }
            }).catch(function (err) {
                $ionicLoading.hide();
                $helper.toast(err, 'short', 'bottom');
            });
        };

        /******************     2017-06-16 Jaydon: Ocotpus    *****************/
        $rootScope.octopusOperation = {            
            reset: function () {
                $rootScope.showDownload = true;
                $api.octopusApi({
                    type: 'reset_device',
                }).then(function (res) {
                    $rootScope.showDownload = false;
                    console.log('reset octopus : ' + res);
                    if (res.return_code == 0) {
                        $helper.toast('reset success!', 'short', 'bottom');
                    } else {
                        $helper.alertMsg(res.return_message);
                    }
                }).catch(function (err) {
                    $rootScope.showDownload = false;
                    if (!err) {
                        err = $translate.instant('OCTOPUS_CONNECT_FAILED');
                    }
                    $helper.alertMsg(err);
                });
            },
            start: function () {
                $rootScope.showDownload = true;
                $api.octopusApi({
                    type: 'start'
                }).then(function (res) {
                    $rootScope.showDownload = false;
                    console.log('start octopus : ' + res);
                    if (res.return_code == 0) {
                        $helper.toast('start success!', 'short', 'bottom');
                    } else {
                        $helper.alertMsg(res.return_message);
                    }
                }).catch(function (err) {
                    $rootScope.showDownload = false;
                    if (!err) {
                        err = $translate.instant('OCTOPUS_CONNECT_FAILED');
                    }
                    $helper.alertMsg(err);
                });
            },
            generate: function () {
                $rootScope.showDownload = true;
                $api.octopusApi({
                    type: 'generate'
                }).then(function (res) {
                    $rootScope.showDownload = false;
                    console.log('generate octopus : ' + res);
                    if (res.return_code == 0) {
                        $helper.toast('generate success!', 'short', 'bottom');
                    } else {
                        $helper.alertMsg(res.return_message);
                    }
                }).catch(function (err) {
                    $rootScope.showDownload = false;
                    if (!err) {
                        err = $translate.instant('OCTOPUS_CONNECT_FAILED');
                    }
                    $helper.alertMsg(err);
                });
            }

        };

        $rootScope.octopusIP = $localStorage.get('octopus_ip') ? $localStorage.get('octopus_ip') : '';
        if ($rootScope.octopusIP == '') {
            $localStorage.set('octopus_ip', $rootScope.octopusIP);
        }
        $rootScope.changeOctopusIP = function (octopusIP) {
            $localStorage.set('octopus_ip', octopusIP);
            $rootScope.octopusIP = octopusIP;
            console.log("octopus ip :" + $rootScope.octopusIP);
        };   






        /*******************     2017-06-16 Jaydon: 一个打印机多个Department      ****************/


        

     
        $rootScope.departmentOperation = {
            getOriginDepartment: function(){
                var depart = [];
                for(var i=0;i<$scope.originDepartment.length;i++){
                    $scope.originDepartment[i].check = false;
                    depart.push({
                        department_id: $scope.originDepartment[i].department_id,
                        department_name: $scope.originDepartment[i].department_name,
                        department_name_EN_US: $scope.originDepartment[i].department_name_EN_US,
                        department_name_ZH_HK: $scope.originDepartment[i].department_name_ZH_HK,
                        department_name_ZH_CN: $scope.originDepartment[i].department_name_ZH_CN,
                        check: false
                    });
                }
                return depart;
            },
            popDepartmentList : function(printerIndex){                

                $ionicModal.fromTemplateUrl('templates/modal.department-list.html', {
                    scope: $rootScope,
                    animation: 'none'
                }).then(function (modal) {
                    $scope.departmentModal = modal;
                    $rootScope.currentPrinterIndex = printerIndex;
                    $rootScope.currentPrinter = $rootScope.printerList[printerIndex];
                    console.log($rootScope.currentPrinter);
                    modal.show();
                });            
            },
            back: function(){
                $scope.departmentModal.hide();
                // $scope.departmentModal.remove();
            },
            chooseDepartment: function(departmentIndex){
                console.log(departmentIndex);
                console.log($rootScope.currentPrinterIndex);
                ((($rootScope.printerList[$rootScope.currentPrinterIndex]).department)[departmentIndex]).check = !((($rootScope.printerList[$rootScope.currentPrinterIndex]).department)[departmentIndex]).check ;
                console.log(angular.toJson($rootScope.printerList));
            },
            addPrinter: function(){
                $rootScope.printerList.push({
                    id:$rootScope.printerList.length+1,
                    epsonIpAddress:'',
                    epsonPort: '',
                    epsonDeviceId: '',
                    department: $rootScope.departmentOperation.getOriginDepartment()
                });
            },
            deletePrinter: function(index){
                $rootScope.printerList.splice(index,1);
            }
        };

        $scope.originDepartment = [
            {
                department_id : 1,
                department_name: 'Jaydon',
                department_name_EN_US: 'Jaydon1',
                department_name_ZH_HK: 'Jaydon2',
                department_name_ZH_CN: 'Jaydon3'
            },
            {
                department_id: 55,
                department_name: "bar",
                department_name_EN_US: "bar",
                department_name_ZH_HK: "水吧",
                department_name_ZH_CN: "水吧"
            }            
        ];

          

        if(!$rootScope.printerList){
            $rootScope.printerList = [
                {
                    id:1,
                    epsonIpAddress:'',
                    epsonPort: '',
                    epsonDeviceId: '',
                    department: $rootScope.departmentOperation.getOriginDepartment()
                }
            ];
        }


            $rootScope.changeEpsonIpAddr2 = function (addr,printerIndex) {
                $rootScope.printerList[printerIndex].epsonIpAddress = addr;
            };


            $rootScope.changeEpsonPort2 = function (addr,printerIndex) {
                $rootScope.printerList[printerIndex].epsonPort = addr;
            };


            $rootScope.changeEpsonDeviceId2 = function (addr,printerIndex) {
                $rootScope.printerList[printerIndex].epsonDeviceId = addr;
            };
     

            $rootScope.parseOctopusDate = function(dateStr){
                var result = '';
                if(dateStr && dateStr!=''){
                    var dateArr = dateStr.split(' ');
                    if(dateArr.length == 2){
                        result = dateArr[0].split('/').reverse().join('-') + ' ' + dateArr[1];                        
                    }
                }
                return result;
                
            };
       


    });

